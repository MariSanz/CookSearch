package es.uniovi.cooksearch.negocio.impl;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import es.uniovi.cooksearch.excepciones.ElementoRepetidoException;
import es.uniovi.cooksearch.modelo.Usuario;
import es.uniovi.cooksearch.persistencia.UsuarioDao;
import es.uniovi.cooksearch.vista.RegistroForm;

public class UsuarioServiceImplTest {
	
	private UsuarioDao usuarioDao;
	private UsuarioServiceImpl usuarioServiceImpl;
	private RegistroForm registroForm;
	
	@Before
	public void setUp() {
		
		usuarioDao = mock(UsuarioDao.class);

		usuarioServiceImpl = new UsuarioServiceImpl();
		ReflectionTestUtils.setField(usuarioServiceImpl, "usuarioDao", usuarioDao);

		registroForm = new RegistroForm();
		registroForm.setEmail("test@prueba.es");
		registroForm.setPassword("test");
		registroForm.setUsuario("test");
	}

	@Test
	public void testRegistrarCorrecto() throws ElementoRepetidoException {
		
		usuarioServiceImpl.registrar(registroForm);
		
		verify(usuarioDao).findOneByEmail("test@prueba.es");
		verify(usuarioDao).insert(refEq(registroForm.toUsuario()));
	}

	@Test
	public void testRegistrarEmailRepetido() {
		
		when(usuarioDao.findOneByEmail("test@prueba.es")).thenReturn(new Usuario("", "", ""));
		
		try {
			usuarioServiceImpl.registrar(registroForm);
			fail();
		}
		catch (ElementoRepetidoException e) {}

		verify(usuarioDao, never()).insert(any(Usuario.class));
	}
	
	@Test
	public void testRegistrarUsuarioRepetido() {
		
		when(usuarioDao.findOneByUsuario("test")).thenReturn(new Usuario("", "", ""));
		
		try {
			usuarioServiceImpl.registrar(registroForm);
			fail();
		}
		catch (ElementoRepetidoException e) {}

		verify(usuarioDao, never()).insert(any(Usuario.class));
	}

}
