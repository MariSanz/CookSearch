package es.uniovi.cooksearch.negocio.impl;

import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.multipart.MultipartFile;

import es.uniovi.cooksearch.excepciones.DatosObligatoriosException;
import es.uniovi.cooksearch.modelo.Anotacion;
import es.uniovi.cooksearch.persistencia.AnotacionDao;
import es.uniovi.cooksearch.persistencia.UsuarioDao;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionNuevaForm;

public class AnotacionServiceImplTest {

	private AnotacionDao anotacionDao;
	@SuppressWarnings("unused")
	private UsuarioDao usuarioDao;
	private AnotacionServiceImpl anotacionServiceImpl;
	private AnotacionNuevaForm anotacionNuevaForm;

	private ApplicationContext applicationContext;

	@Before
	public void setUp() {

		anotacionDao = mock(AnotacionDao.class);
		usuarioDao = mock(UsuarioDao.class);
		applicationContext = mock(ApplicationContext.class);

		anotacionServiceImpl = new AnotacionServiceImpl();
		ReflectionTestUtils.setField(anotacionServiceImpl, "anotacionDao", anotacionDao);

		anotacionNuevaForm = new AnotacionNuevaForm();
		anotacionNuevaForm.setNombre("Test anotacion");
		anotacionNuevaForm.setDescripcion("Pruebas unitarias");
		anotacionNuevaForm.setPrivacidad(true);
		anotacionNuevaForm.setUsuarioId("572b1ff4e8ba66004177e7f8");

	}

	@Test
	public void testGuardarAnotacionCampoPredefinido() throws DatosObligatoriosException {

		anotacionNuevaForm.setTipoDeAnotacion("anotacion.tipoReceta");
		anotacionServiceImpl.guardar(anotacionNuevaForm);

		TextCriteria textCriteria = TextCriteria.forDefaultLanguage().matching("nombre");
		verify(anotacionDao).findBy(textCriteria);
		verify(anotacionDao).insert(refEq(anotacionNuevaForm.getAnotacion(applicationContext)));
	}

	@Test
	public void testGuardarAnotacionCampoFichero() throws DatosObligatoriosException {

		anotacionNuevaForm.setTipoDeAnotacion("anotacion.tipoLibro");

		MultipartFile file = (MultipartFile) new File("C:/Users/luism_000/Documents/ficheros/IndiceAuditoria.jpg");
		MultipartFile[] archivos = new MultipartFile[1];
		archivos[0] = file;

		String[] etiquetas = { "fichero" };
		String[] tipo = { "tipo-de-campo-file" };

		anotacionNuevaForm.setCampoFile(archivos);
		anotacionNuevaForm.setEtiquetaFile(etiquetas);
		anotacionNuevaForm.setTipoCampoFile(tipo);

		anotacionServiceImpl.guardar(anotacionNuevaForm);

		TextCriteria textCriteria = TextCriteria.forDefaultLanguage().matching("fichero");

		verify(anotacionDao).findBy(textCriteria);
		verify(anotacionDao).insert(refEq(anotacionNuevaForm.getAnotacion(applicationContext)));

	}

	@Test
	public void testGuardarAnotacionCampoURL() throws DatosObligatoriosException {

		anotacionNuevaForm.setTipoDeAnotacion("anotacion.tipoLibro");

		String[] urls = { "http://www.juntadeandalucia.es/servicios/madeja/contenido/recurso/381" };
		String[] etiquetas = { "url" };
		String[] tipo = { "tipo-de-campo-url" };

		anotacionNuevaForm.setCampoTexto(urls);
		anotacionNuevaForm.setEtiquetaFile(etiquetas);
		anotacionNuevaForm.setTipoCampoFile(tipo);

		anotacionServiceImpl.guardar(anotacionNuevaForm);

		TextCriteria textCriteria = TextCriteria.forDefaultLanguage().matching("url");

		verify(anotacionDao).findBy(textCriteria);
		verify(anotacionDao).insert(refEq(anotacionNuevaForm.getAnotacion(applicationContext)));

	}

	@Test
	public void testGuardarAnotacionCampoTexto() throws DatosObligatoriosException {

		anotacionNuevaForm.setTipoDeAnotacion("anotacion.tipoTecnica");

		String[] texto = { "Esto es una prueba para el proyecto, "
				+ "ruego a Dios todo salga bien, quien lea esto que sepa me siento un poco mal y desesperada" };
		String[] etiquetas = { "texto corto" };
		String[] tipo = { "tipo-de-campo-texto-corto" };

		anotacionNuevaForm.setCampoTexto(texto);
		anotacionNuevaForm.setEtiquetaFile(etiquetas);
		anotacionNuevaForm.setTipoCampoFile(tipo);

		anotacionServiceImpl.guardar(anotacionNuevaForm);

		TextCriteria textCriteria = TextCriteria.forDefaultLanguage().matching("es una prueba");

		verify(anotacionDao).findBy(textCriteria);
		verify(anotacionDao).insert(refEq(anotacionNuevaForm.getAnotacion(applicationContext)));

	}

	@Test
	public void testGuardarAnotacionTipoPersonalizado() throws DatosObligatoriosException {

		anotacionNuevaForm.setTipoDeContenidoOtro("Tipo de Prueba");

		anotacionServiceImpl.guardar(anotacionNuevaForm);

		TextCriteria textCriteria = TextCriteria.forDefaultLanguage().matching("Tipo de Prueba");

		verify(anotacionDao).findBy(textCriteria);
		verify(anotacionDao).insert(refEq(anotacionNuevaForm.getAnotacion(applicationContext)));

	}

	@Test
	public void testGuardarDatosIncompletos() {

		anotacionNuevaForm.setNombre("");

		List<Anotacion> aVacia = new ArrayList<>();
		Anotacion a= new Anotacion("", "", false, "", null);
		aVacia.add(a);
		
		TextCriteria textCriteria = TextCriteria.forDefaultLanguage().matching("nombre");
		when(anotacionDao.findBy(textCriteria))
				.thenReturn(aVacia);

		try {
			anotacionServiceImpl.guardar(anotacionNuevaForm);
			fail();
		} catch (DatosObligatoriosException e) {

		}

		verify(anotacionDao, never()).insert(any(Anotacion.class));

	}

}
