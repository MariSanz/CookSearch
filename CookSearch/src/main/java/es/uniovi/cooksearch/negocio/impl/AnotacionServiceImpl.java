package es.uniovi.cooksearch.negocio.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxRequestConfig;

import es.uniovi.cooksearch.excepciones.DatosObligatoriosException;
import es.uniovi.cooksearch.excepciones.ElementoNoEncontradoException;
import es.uniovi.cooksearch.excepciones.ElementoRepetidoException;
import es.uniovi.cooksearch.modelo.Anotacion;
import es.uniovi.cooksearch.modelo.Comentario;
import es.uniovi.cooksearch.modelo.Usuario;
import es.uniovi.cooksearch.modelo.campos.CampoFichero;
import es.uniovi.cooksearch.modelo.tipo.TipoDeAnotacion;
import es.uniovi.cooksearch.negocio.AnotacionService;
import es.uniovi.cooksearch.persistencia.AnotacionDao;
import es.uniovi.cooksearch.persistencia.ComentarioDao;
import es.uniovi.cooksearch.persistencia.TipoDeAnotacionDao;
import es.uniovi.cooksearch.persistencia.UsuarioDao;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionBuscarForm;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionEditarForm;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionListadoForm;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionNuevaForm;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionVerForm;
import es.uniovi.cooksearch.vista.anotaciones.FicheroForm;
import es.uniovi.cooksearch.vista.anotaciones.RespuestaVotacionForm;
import es.uniovi.cooksearch.vista.anotaciones.VotoForm;

/**
 * Clase que implementa los servicios de la interfaz, conectando con los objetos
 * de la persistencia para Anotación. Es un objeto service de Spring
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Service
public class AnotacionServiceImpl implements AnotacionService {

	@Autowired
	private AnotacionDao anotacionDao;

	@Autowired
	private TipoDeAnotacionDao tipoAnotacionDao;

	@Autowired
	private MongoTemplate MongoTemplate;

	@Autowired
	private UsuarioDao usuarioDao;

	@Autowired
	private ComentarioDao comentarioDao;

	@Autowired
	private ApplicationContext contexto;

	/**
	 * Ruta donde se almacena en el servidor los ficheros
	 */
	@Value("${path_ficheros}")
	private String pathFicheros;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void guardar(AnotacionNuevaForm anotacionForm) throws DatosObligatoriosException {

		if (anotacionForm.getNombre().equals("") || anotacionForm.getNombre() == null
				|| anotacionForm.getNombre().equals(" ")) {

			throw new DatosObligatoriosException();

		} else {
			Anotacion anotacion = anotacionForm.getAnotacion(contexto);
			anotacion.setUsuarioId(anotacionForm.getUsuarioId());

			if (anotacion.getTipoDeAnotacionId() == null) {

				TipoDeAnotacion tipoNuevo = anotacionForm.getNuevoTipoDeAnotacion();
				tipoAnotacionDao.save(tipoNuevo);

				anotacion.setTipoDeAnotacionId(tipoNuevo.getId());
			}

			anotacionDao.save(anotacion);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AnotacionListadoForm> buscar(AnotacionBuscarForm busqueda) {

		Criteria criteria = new Criteria()
				.andOperator(Criteria.where("puntuacion.puntuacion").gte(busqueda.getPuntuacion()));
		
		if(!busqueda.getTipoDeAnotacion().equals("")){//Si no es buscar por todos los tipos, filtro aqui
			criteria.and("tipoDeAnotacionId").is(busqueda.getTipoDeAnotacion());
		}
		if(busqueda.isPrivada()){ //si quiero buscar por privada
			criteria.and("privacidad").is(false);
		}
		if(busqueda.isPublica()){
			criteria.and("privacidad").is(true);
		}
		if (busqueda.getFechaDesde() != null) {
			criteria.and("fechaPublicacion").gte(busqueda.getFechaDesde());
		}
		if (busqueda.getFechaHasta() != null) {
			criteria.and("fechaPublicacion").lte(busqueda.getFechaDesde());
		}
		
		Query consulta = new Query();
		consulta.addCriteria(criteria);
		if(!busqueda.getTextoBuscar().equals("")){
			consulta.addCriteria(TextCriteria.forDefaultLanguage().matching(busqueda.getTextoBuscar()));
		}

		List<Anotacion> anotaciones = MongoTemplate.find(consulta, Anotacion.class);
		System.out.println(anotaciones);

		List<AnotacionListadoForm> buscadas = modelarListado(anotaciones);

		return buscadas;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AnotacionListadoForm> buscar(String textoBuscar, String usuarioNombre) {

		Usuario usuarioSesion = usuarioDao.findOneByUsuario(usuarioNombre);

		TextCriteria textCriteria = TextCriteria.forDefaultLanguage().matching(textoBuscar);
//TODO
		List<Anotacion> anotaciones = anotacionDao.findByAndUsuarioIdAndPrivacidadOrderByAjusteBusqueda(textCriteria,
				usuarioSesion.getId(), false);

		List<AnotacionListadoForm> buscadas = modelarListado(anotaciones);

		return buscadas;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean borrar(String id) {
		try {
			Anotacion borrada = anotacionDao.findOne(id);
			anotacionDao.delete(id);
			//borro los comentarios de la anotacion en la base de datos
			for (Comentario c : borrada.getComentarios()) {
				comentarioDao.delete(c);
			}

		} catch (Exception e) {
			return false;
		}

		if (anotacionDao.findOne(id) == null) {
			return true;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AnotacionVerForm ver(String id, String usuarioSession) {

		Anotacion anotacion = anotacionDao.findOne(id);

		AnotacionVerForm anotacionVerForm = new AnotacionVerForm();
		anotacionVerForm.setNombre(anotacion.getNombre());
		anotacionVerForm.setDescripcion(anotacion.getDescripcion());
		anotacionVerForm.getCampos().addAll(anotacion.getMasDatos());
		anotacionVerForm.getComentarios().addAll(comentarioDao.findByAnotacionIdAndAprobado(anotacion.getId(), true));
		anotacionVerForm.setTipoDeAnotacion(tipoAnotacionDao.findOne(anotacion.getTipoDeAnotacionId()));
		anotacionVerForm.setTipoAnotacionNombre(anotacionVerForm.getTipoDeAnotacion().getNombre());
		String usuarioAnotacion = usuarioDao.findOne(anotacion.getUsuarioId()).getUsername();
		anotacionVerForm.setUsuario(usuarioAnotacion);
		anotacionVerForm.setVotos(anotacion.getPuntuacion().getVotos());
		anotacionVerForm.setAnotacionId(anotacion.getId());
		anotacionVerForm.setPuntuacion(anotacion.getPuntuacion().getPuntuacion());
		anotacionVerForm.fechaPublicacion(anotacion.getFechaPublicacion());
		
		if(!anotacionVerForm.getUsuario().equals(usuarioSession)){//si no es el mismo usuario de sesión y de la anotación
			anotacion.setContadorVistas(anotacion.getContadorVistas() + 1);
			anotacionDao.save(anotacion);
		}

		return anotacionVerForm;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FicheroForm verFichero(String id) throws ElementoNoEncontradoException {

		Anotacion anotacion = anotacionDao.findByFichero(id);
		if (anotacion == null) {
			throw new ElementoNoEncontradoException();
		}

		CampoFichero campoFichero = (CampoFichero) anotacion.getMasDatos().get(0);
		ByteArrayOutputStream fichero = new ByteArrayOutputStream();

		try {
			
	        DbxRequestConfig config = new DbxRequestConfig(
	            "CookSearch", "es");
		        
			DbxClient client = new DbxClient(config, "2U7JlZqbXH4AAAAAAAAXZ4GJpMlS-RopER_sC-LOxVn84rycxRs11pX_jIRbXRgt");
			
		    client.getFile("/" + id, null, fichero);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ElementoNoEncontradoException();
		}

		return new FicheroForm(new ByteArrayInputStream(fichero.toByteArray()), campoFichero.getContentType(),
				campoFichero.getNombreRealFichero());

	}

	/**
	 * Método para relacionar los atributos de las anotaciones en la base de
	 * datos con los atributos del formulario de listado
	 * 
	 * @param anotaciones
	 *            {@link Anotacion}
	 * @return {@link List} {@link AnotacionListadoForm}
	 */
	private List<AnotacionListadoForm> modelarListado(List<Anotacion> anotaciones) {
		List<AnotacionListadoForm> buscadas = new ArrayList<>();
		AnotacionListadoForm aInsertar;
		for (Anotacion a : anotaciones) {

			aInsertar = new AnotacionListadoForm();
			aInsertar.setAnotacionId(a.getId());
			aInsertar.setDescripcion(a.getDescripcion());
			aInsertar.setNombre(a.getNombre());
			aInsertar.setTipoDeAnotacion(tipoAnotacionDao.findOne(a.getTipoDeAnotacionId()));
			aInsertar.setUsuarioNombre(usuarioDao.findOne(a.getUsuarioId()).getUsuario());
			aInsertar.setPuntuacion(a.getPuntuacion().getPuntuacion());
			aInsertar.setAjusteBusqueda(a.getAjusteBusqueda());

			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			String usuarioSession = auth.getName();
			Boolean propia = usuarioSession.equals(aInsertar.getUsuarioNombre());
			aInsertar.setPropia(propia);

			buscadas.add(aInsertar);
			aInsertar = null;
		}
		return buscadas;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AnotacionListadoForm> buscar(String textoBuscar) {

		TextCriteria textCriteria = TextCriteria.forDefaultLanguage().matching(textoBuscar);

		List<Anotacion> anotaciones = anotacionDao.findByAndPrivacidadOrderByAjusteBusqueda(textCriteria, true);

		List<AnotacionListadoForm> buscadas = modelarListado(anotaciones);

		return buscadas;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AnotacionVerForm findByContadorMasVistas() {

		AnotacionVerForm anotacionVerForm = new AnotacionVerForm();
		Anotacion anotacion = anotacionDao.findFirstByOrderByContadorVistasDesc();

		anotacionVerForm.setNombre(anotacion.getNombre());
		anotacionVerForm.setDescripcion(anotacion.getDescripcion());
		anotacionVerForm.getCampos().addAll(anotacion.getMasDatos());
		anotacionVerForm.getComentarios().addAll(anotacion.getComentarios());
		anotacionVerForm.setTipoDeAnotacion(tipoAnotacionDao.findOne(anotacion.getTipoDeAnotacionId()));
		anotacionVerForm.setTipoAnotacionNombre(tipoAnotacionDao.findOne(anotacion.getTipoDeAnotacionId()).getNombre());
		String usuarioAnotacion = usuarioDao.findOne(anotacion.getUsuarioId()).getUsername();
		anotacionVerForm.setUsuario(usuarioAnotacion);
		anotacionVerForm.setAnotacionId(anotacion.getId());
		anotacionVerForm.setPuntuacion(anotacion.getPuntuacion().getPuntuacion());
		anotacionVerForm.setNumVistas(anotacion.getContadorVistas());

		return anotacionVerForm;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AnotacionEditarForm verEditar(String id) {
		Anotacion anotacion = anotacionDao.findOne(id);
		return new AnotacionEditarForm(contexto, anotacion);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void editar(String id, AnotacionEditarForm anotacionEditarForm) throws ElementoNoEncontradoException {

		Anotacion anotacion = anotacionDao.findOne(id);
		if (anotacion == null) {
			throw new ElementoNoEncontradoException();
		}

		anotacionEditarForm.reemplazarCampos(contexto, anotacion);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RespuestaVotacionForm votar(VotoForm votoForm, String usuario)
			throws ElementoNoEncontradoException, ElementoRepetidoException {

		Anotacion anotacion = anotacionDao.findOne(votoForm.getIdAnotacion());
		if (anotacion == null) {
			throw new ElementoNoEncontradoException();
		}

		if (!anotacion.getPuntuacion().votar(votoForm.getPuntuacion(), usuario)) {
			throw new ElementoRepetidoException();
		}

		anotacionDao.save(anotacion);

		return new RespuestaVotacionForm(anotacion.getPuntuacion().getVotos(),
				anotacion.getPuntuacion().getPuntuacion());
	}

}
