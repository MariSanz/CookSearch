package es.uniovi.cooksearch.negocio;

import es.uniovi.cooksearch.excepciones.ElementoRepetidoException;
import es.uniovi.cooksearch.vista.RegistroForm;

/**
 * Clase que gestiona los metodos disponibles en los servicios de las usuarios.
 * Aplica el patron service
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public interface UsuarioService {

	/**
	 * Método para registrar un usuario tramitando los atributos del formulario
	 * de registro
	 * 
	 * @param registro
	 *            {@link RegistroForm}
	 * @throws ElementoRepetidoException
	 */
	void registrar(RegistroForm registro) throws ElementoRepetidoException;

	/**
	 * Método para obtener el identificador del usuario cuyo nombre se ha pasado
	 * como parametro
	 * 
	 * @param nombreUsuario
	 *            nombre del usuario al que buscar el id
	 * @return
	 */
	String getIdByName(String nombreUsuario);
}
