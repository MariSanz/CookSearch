package es.uniovi.cooksearch.negocio.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import es.uniovi.cooksearch.modelo.Anotacion;
import es.uniovi.cooksearch.modelo.Comentario;
import es.uniovi.cooksearch.negocio.ComentarioService;
import es.uniovi.cooksearch.persistencia.AnotacionDao;
import es.uniovi.cooksearch.persistencia.ComentarioDao;
import es.uniovi.cooksearch.vista.comentarios.ComentarioListarForm;

/**
 * Clase que implementa los servicios de la interfaz, conectando con los objetos
 * de la persistencia para Comentario. Es un objeto service de Spring
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Service
public class ComentarioServiceImpl implements ComentarioService {

	@Autowired
	private AnotacionDao anotacionDao;

	@Autowired
	private ComentarioDao comentarioDao;

	@Override
	public Comentario guardar(String nombreUsuario, String nuevoComentario, String anotacionId) {

		Anotacion anotacion = anotacionDao.findOne(anotacionId);

		Comentario comentario = new Comentario();
		comentario.setContenido(nuevoComentario);
		comentario.setUsuario(nombreUsuario);
		comentario.setAnotacionId(anotacionId);
		comentario.setAprobado(false);

		Comentario guardado = comentarioDao.save(comentario);
		anotacion.getComentarios().add(guardado);

		anotacionDao.save(anotacion);

		return guardado;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Comentario> buscarTodosAprobadosByAnotacion(String anotacionId) {

		return comentarioDao.findByAnotacionIdAndAprobado(anotacionId, true);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ComentarioListarForm> findUltimos20Comentarios() {
		// List<Comentario> comentarios = comentarioDao.findAll(new
		// Sort("fecha"));

		List<Comentario> comentarios = comentarioDao.findByAprobado(new Sort("fecha"), true);

		int maxIndex = comentarios.size() >= 20 ? 20 : comentarios.size();
		comentarios.subList(0, maxIndex);

		List<ComentarioListarForm> listados = new ArrayList<>();

		for (Comentario c : comentarios) {
			Anotacion a = anotacionDao.findOne(c.getAnotacionId());
			if (a != null) {
				ComentarioListarForm form = new ComentarioListarForm(c.getUsuario(), a.getNombre(), c.getAnotacionId(),
						c.getContenido(), c.getFecha(), c.getId(), c.getAprobado());
				listados.add(form);
			}
		}
		return listados;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ComentarioListarForm> findAll() {
		List<Comentario> comentarios = comentarioDao.findAll();

		List<ComentarioListarForm> form = modelarComentarios(comentarios);

		return form;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ComentarioListarForm> findByAprobar(Boolean aprobado) {
		List<Comentario> comentarios = comentarioDao.findByAprobado(new Sort("fecha"), aprobado);
		List<ComentarioListarForm> form = modelarComentarios(comentarios);

		return form;
	}

	/**
	 * Método para modelar los comentarios por los atributos de la base de datos
	 * a los atributos a listar en los formularios de la vista
	 * 
	 * @param comentarios
	 *            {@link List} {@link Comentario}
	 * @return {@link List} {@link ComentarioListarForm}
	 */
	private List<ComentarioListarForm> modelarComentarios(List<Comentario> comentarios) {
		List<ComentarioListarForm> form = new ArrayList<>();
		if (comentarios.size() > 0) {
			for (Comentario c : comentarios) {
				Anotacion a = anotacionDao.findOne(c.getAnotacionId());
				if (a != null) {
					form.add(new ComentarioListarForm(c.getUsuario(), a.getNombre(), c.getAnotacionId(),
							c.getContenido(), c.getFecha(), c.getId(), c.getAprobado()));
				}
			}
		}
		return form;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ComentarioListarForm> buscar(String contenido, String usuario) {

		List<Comentario> comentarios = new ArrayList<>();
		if (contenido.equals("") && !usuario.equals("")) {
			comentarios.addAll(comentarioDao.findByUsuario(usuario));
		} else if (contenido.equals("") && !usuario.equals("")) {
			comentarios.addAll(comentarioDao.findByContenido(contenido));
		} else {
			comentarios.addAll(comentarioDao.findByUsuarioOrContenido(usuario, contenido));
		}

		List<ComentarioListarForm> form = modelarComentarios(comentarios);
		return form;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean borrar(String idComentario, String idAnotacion) {
		try {
			comentarioDao.delete(idComentario);
			Anotacion a = anotacionDao.findOne(idAnotacion);

			for (Comentario c : a.getComentarios()) {
				if (c.getId().equals(idComentario)) {
					a.getComentarios().remove(c);
					break;
				}
			}
			anotacionDao.save(a);

		} catch (Exception e) {
			return false;
		}

		if (comentarioDao.findOne(idComentario) == null) {
			return true;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Comentario aprobar(String comentarioAprobadoId) {
		Comentario actualizar = comentarioDao.findOne(comentarioAprobadoId);

		actualizar.setAprobado(true);
		comentarioDao.save(actualizar);

		return actualizar;
	}

}
