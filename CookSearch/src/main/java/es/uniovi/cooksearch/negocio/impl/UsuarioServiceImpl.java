package es.uniovi.cooksearch.negocio.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import es.uniovi.cooksearch.excepciones.ElementoRepetidoException;
import es.uniovi.cooksearch.modelo.Usuario;
import es.uniovi.cooksearch.negocio.UsuarioService;
import es.uniovi.cooksearch.persistencia.UsuarioDao;
import es.uniovi.cooksearch.vista.RegistroForm;

/**
 * Clase que implementa los servicios de la interfaz, conectando con los objetos
 * de la persistencia para Usuario. Es un objeto service de Spring
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Service
public class UsuarioServiceImpl implements UsuarioService, UserDetailsService {

	@Autowired
	private UsuarioDao usuarioDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void registrar(RegistroForm registro) throws ElementoRepetidoException {

		Usuario usuario = registro.toUsuario();

		if (usuarioDao.findOneByEmail(usuario.getEmail()) != null
				|| usuarioDao.findOneByUsuario(usuario.getUsuario()) != null) {
			throw new ElementoRepetidoException();
		}

		usuarioDao.insert(usuario);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserDetails loadUserByUsername(String usuario) throws UsernameNotFoundException {

		UserDetails usr = usuarioDao.findOneByUsuario(usuario);

		if (usr == null) {
			throw new UsernameNotFoundException("no encontrada");
		}

		return usr;
	}

	@Override
	public String getIdByName(String nombreUsuario) {

		return usuarioDao.findOneByUsuario(nombreUsuario).getId();
	}

}
