package es.uniovi.cooksearch.negocio;

import java.util.List;

import es.uniovi.cooksearch.modelo.Comentario;
import es.uniovi.cooksearch.vista.comentarios.ComentarioListarForm;

/**
 * Clase que gestiona los metodos disponibles en los servicios de los
 * comentarios. Aplica el patron service
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public interface ComentarioService {

	/**
	 * Método para guardar un comentario en la base de datos, procesar los datos
	 * desde otros servicios como los de las anotaciones
	 * 
	 * @param nombreUsuario nombre del usuario que realiza el comentario
	 * @param nuevoComentario
	 *            contenido del comentario
	 * @param anotacionId
	 *            identificador de la anotacion a la que pertenezca el
	 *            comentario
	 * @return {@link Comentario}
	 */
	Comentario guardar(String nombreUsuario, String nuevoComentario, String anotacionId);

	/**
	 * Método para listar los ultimos 20 comentario pulicados (es decir que
	 * esten aprobados)
	 * 
	 * @return {@link List} {@link ComentarioListarForm}
	 */
	List<ComentarioListarForm> findUltimos20Comentarios();

	/**
	 * Método para obtener todos los comentarios de la base de datos, esten
	 * aproados o no
	 * 
	 * @return {@link List} {@link ComentarioListarForm}
	 */
	List<ComentarioListarForm> findAll();

	/**
	 * Método para buscar un comentario por su contenido y usuario. Es por
	 * patron que coincida por delante, al medio, o al final
	 * 
	 * @param contenido
	 *            informacion buscada
	 * @param usuario
	 * @return {@link List} {@link ComentarioListarForm}
	 */
	List<ComentarioListarForm> buscar(String contenido, String usuario);

	/**
	 * Método para borrar un comentario por su identificador
	 * 
	 * @param id identificador del comentario a borrar
	 * @param idAnotacion 
	 * @return true si el comentario fue borrado, false si no pudo ser borrado
	 */
	boolean borrar(String idComentario, String idAnotacion);

	/**
	 * Método para buscar los comentarios aprobados de la anotación que
	 * corresponde por el identificador de parametro
	 * 
	 * @param anotacionId
	 *            identificador de la anotación dentro de la cual buscar
	 * @param aprobado
	 *            true si está aprobado false en caso contrario
	 * @return
	 */
	List<Comentario> buscarTodosAprobadosByAnotacion(String anotacionId);

	/**
	 * Método por el cual cambiar a aprobados el comentario cuyo id pasado como
	 * parametro coincide en la base de datos
	 * 
	 * @param comentarioAprobadoId
	 * @return {@link Comentario}
	 */
	Comentario aprobar(String comentarioAprobadoId);

	/**
	 * Método para buscar los comentarios por su atributo aprobado
	 * 
	 * @param aprobado
	 *            true listara los comentarios aprobados, false los no aprobados
	 * @return {@link List} {@link ComentarioListarForm}
	 */
	List<ComentarioListarForm> findByAprobar(Boolean aprobado);

}
