package es.uniovi.cooksearch.negocio.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.uniovi.cooksearch.modelo.tipo.TipoDeAnotacion;
import es.uniovi.cooksearch.negocio.TipoDeAnotacionService;
import es.uniovi.cooksearch.persistencia.TipoDeAnotacionDao;
import es.uniovi.cooksearch.persistencia.UsuarioDao;

/**
 * Clase que implementa los servicios de la interfaz, conectando con los objetos
 * de la persistencia para Tipo de Anotacion. Es un objeto service de Spring
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Service
public class TipoDeAnotacionServiceImpl implements TipoDeAnotacionService {

	@Autowired
	private TipoDeAnotacionDao tipoAnotacionDao;

	@Autowired
	private UsuarioDao usuaioDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TipoDeAnotacion> getTodosTipoAnotacion() {

		return tipoAnotacionDao.findAll();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TipoDeAnotacion> getTipoAnotacionUsuario(String usuarioNombre) {
		
		String usuarioId = "";
		if(!usuarioNombre.equals("anonymousUser")){
			usuarioId = usuaioDao.findOneByUsuario(usuarioNombre).getId();
		}

		List<TipoDeAnotacion> tiposDeAnotacion = tipoAnotacionDao.findByUsuarioId(usuarioId);
		tiposDeAnotacion.addAll(tipoAnotacionDao.findByUsuarioIdNull());

		return tiposDeAnotacion;
	}

}
