package es.uniovi.cooksearch.negocio;

import java.util.List;

import es.uniovi.cooksearch.excepciones.DatosObligatoriosException;
import es.uniovi.cooksearch.excepciones.ElementoNoEncontradoException;
import es.uniovi.cooksearch.excepciones.ElementoRepetidoException;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionBuscarForm;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionEditarForm;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionListadoForm;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionNuevaForm;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionVerForm;
import es.uniovi.cooksearch.vista.anotaciones.FicheroForm;
import es.uniovi.cooksearch.vista.anotaciones.RespuestaVotacionForm;
import es.uniovi.cooksearch.vista.anotaciones.VotoForm;

/**
 * Clase que gestiona los metodos disponibles en los servicios de las
 * anotaciónes. Aplica el patron service
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public interface AnotacionService {

	/**
	 * Método para guardar la anotación, asocia los datos del formulario de la
	 * vista con los datos atributos de la anotación en la base de datos y el
	 * modelo
	 * 
	 * @param anotacion
	 *            {@link AnotacionNuevaForm}
	 * @throws DatosObligatoriosException
	 *             {@link DatosObligatoriosException}
	 */
	void guardar(AnotacionNuevaForm anotacion) throws DatosObligatoriosException;

	/**
	 * Método para realizar la busqueda sencilla de un texto en todos los campos
	 * de la anotación, busca entre las anotaciónes del usuario pasado como
	 * parametro. Anotaciones privadas
	 * 
	 * @param textoBuscar
	 *            {@link String} texto sencillo a buscar tal cual en la BD
	 * @param usuarioNombre
	 *            nombre del usuario que ha iniciado sesión en la aplicación
	 * @return {@link List} {@link AnotacionListadoForm}
	 */
	List<AnotacionListadoForm> buscar(String textoBuscar, String usuarioNombre);

	/**
	 * Método para borrar la anotación segun su ID
	 * 
	 * @param id
	 *            {@link String} identificador de la anotación en la base de
	 *            datos
	 * @return true si se pudo borrar, false en caso contrario
	 */
	boolean borrar(String id);

	/**
	 * Método para buscar anotaciones segun los parametros del formulario de
	 * buscar, los filtros de la busqueda
	 * 
	 * @param busqueda
	 *            {@link AnotacionBuscarForm}
	 * @return {@link List} {@link AnotacionListadoForm}
	 */
	List<AnotacionListadoForm> buscar(AnotacionBuscarForm busqueda);

	/**
	 * Método para retornar la anotación que corresponde al ID que se pasa como
	 * parametro. Visualizar anotación
	 * 
	 * @param id
	 *            identificador de la anotación a retornar
	 * @param usuarioSession 
	 * @return {@link AnotacionVerForm}
	 */
	AnotacionVerForm ver(String id, String usuarioSession);

	/**
	 * Método para visualizar el fichero cuya ruta en el servidor se ha guardado
	 * 
	 * @param id
	 *            identificador del fichero
	 * @return {@link FicheroForm}
	 * @throws ElementoNoEncontradoException
	 *             fichero no encontrado en la base de datos o el servidor
	 */
	FicheroForm verFichero(String id) throws ElementoNoEncontradoException;

	/**
	 * Método para buscar anotaciones publicas que coincidan en su contenido con
	 * el texto proporcionado para buscar
	 * 
	 * @param textoBuscar
	 *            texto exacto a buscar
	 * @return {@link AnotacionListadoForm}
	 */
	List<AnotacionListadoForm> buscar(String textoBuscar);

	/**
	 * Método para obtener la anotación con más vistas
	 * 
	 * @return {@link AnotacionVerForm}
	 */
	AnotacionVerForm findByContadorMasVistas();

	/**
	 * Método para obtener la anotación con el formato del formulario necesario
	 * para editar la anotación
	 * 
	 * @param id
	 *            identificador de la anotación
	 * @return {@link AnotacionEditarForm}
	 */
	AnotacionEditarForm verEditar(String id);

	/**
	 * Método para guardar la anotación editada
	 * 
	 * @param id
	 *            identificación de la anotación
	 * @param anotacionEditarForm
	 *            {@link AnotacionEditarForm}
	 * @throws ElementoNoEncontradoException
	 */
	void editar(String id, AnotacionEditarForm anotacionEditarForm) throws ElementoNoEncontradoException;

	/**
	 * Método para votar en la anotación, devuelve un objeto que procesara la
	 * vista para su presentación
	 * 
	 * @param votoForm
	 *            {@link VotoForm}
	 * @param usuario
	 *            nombre del usuario que vota
	 * @return {@link RespuestaVotacionForm}
	 * @throws ElementoNoEncontradoException
	 * @throws ElementoRepetidoException
	 */
	RespuestaVotacionForm votar(VotoForm votoForm, String usuario)
			throws ElementoNoEncontradoException, ElementoRepetidoException;

}
