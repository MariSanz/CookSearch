package es.uniovi.cooksearch.negocio;

import java.util.List;

import es.uniovi.cooksearch.modelo.tipo.TipoDeAnotacion;

/**
 * Clase que gestiona los metodos disponibles en los servicios de los tipos de
 * anotaciones. Aplica el patron service
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public interface TipoDeAnotacionService {

	/**
	 * Métodos para obtener todos los tipos de anotación almacenados en la base
	 * de datos
	 * 
	 * @return {@link List} {@link TipoDeAnotacion}
	 */
	List<TipoDeAnotacion> getTodosTipoAnotacion();

	/**
	 * Método para listar los tipos de anotación segun el usuario al que
	 * pertenecen, si el usuario es null se listan los tipos de anotación
	 * predefinidos en la base de datos
	 * 
	 * @param usuarioId
	 *            identificador del usuario
	 * @return {@link List} {@link TipoDeAnotacion}
	 */
	List<TipoDeAnotacion> getTipoAnotacionUsuario(String usuarioId);
}
