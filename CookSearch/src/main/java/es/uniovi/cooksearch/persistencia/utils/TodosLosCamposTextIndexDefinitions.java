package es.uniovi.cooksearch.persistencia.utils;

import org.springframework.data.mongodb.core.index.IndexDefinition;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.query.TextCriteria;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Clase para crear los propios indices de busqueda en las anotaciones cuando se
 * use {@link TextCriteria}, se redefinen los metodos de {@link IndexDefinition}
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public class TodosLosCamposTextIndexDefinitions implements IndexDefinition {

	private TextIndexDefinition.TextIndexDefinitionBuilder builder = TextIndexDefinition.builder();

	/**
	 * Método para cambiar el peso de los atributos de las anotaciones
	 * 
	 * @param campo
	 *            atributo al que cambiar el peso (nombre, descripcion, etc)
	 * @param peso
	 *            peso a asignar, formato 2f ejemplo
	 */
	public void addPeso(String campo, float peso) {
		builder.onField(campo, peso);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DBObject getIndexKeys() {
		DBObject keys = new BasicDBObject();
		keys.put("$**", "text");

		return keys;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DBObject getIndexOptions() {
		return builder.build().getIndexOptions();
	}

}
