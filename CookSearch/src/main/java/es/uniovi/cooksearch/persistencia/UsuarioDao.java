package es.uniovi.cooksearch.persistencia;

import org.springframework.data.mongodb.repository.MongoRepository;

import es.uniovi.cooksearch.modelo.Usuario;

/**
 * Clase con los servicios directos a la base de datos, extiende la clase
 * {@link MongoRepository} para que apartir de patrones en la signatura de los
 * metodos se realicen consultas directas. Documentos Usuario
 * 
 * @see MongoRepository
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public interface UsuarioDao extends MongoRepository<Usuario, String> {

	/**
	 * Método para buscar el email que coincide con el del parametro
	 * 
	 * @param email
	 *            correo electronico a buscar
	 * @return {@link Usuario}
	 */
	Usuario findOneByEmail(String email);

	/**
	 * Método para buscar un usuario por el nombre del usuario
	 * 
	 * @param usuario
	 *            nombre del usuario
	 * @return {@link Usuario}
	 */
	Usuario findOneByUsuario(String usuario);

}
