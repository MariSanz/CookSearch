package es.uniovi.cooksearch.persistencia;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import es.uniovi.cooksearch.modelo.Anotacion;
import es.uniovi.cooksearch.persistencia.utils.TodosLosCamposTextIndexDefinitions;

/**
 * Clase con los servicios directos a la base de datos, extiende la clase
 * {@link MongoRepository} para que apartir de patrones en la signatura de los
 * metodos se realicen consultas directas. Documento Anotacion
 * 
 * @see MongoRepository
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public interface AnotacionDao extends MongoRepository<Anotacion, String>{

	/**
	 * Método para buscar por los indixes de la anotación el texto concreto
	 * 
	 * @see TodosLosCamposTextIndexDefinitions
	 * @param criteria
	 * @return
	 */
	List<Anotacion> findBy(TextCriteria criteria);

	/**
	 * Método para buscar en las anotación por una consulta especifica pasada
	 * por criteria
	 * 
	 * @param criteria
	 *            {@link Criteria}
	 * @return {@link List} {@link Anotacion}
	 */
	List<Anotacion> findBy(Criteria criteria);

	/**
	 * Método para buscar por los indices de las anotaciones y devolver el
	 * resultado ordenado por el ajuste de busqueda que es el indice de
	 * coincidencia de la información buscada en las anotaciones
	 * 
	 * @param textCriteria
	 *            {@link TextCriteria}
	 * @return {@link List} {@link Anotacion}
	 */
	List<Anotacion> findByOrderByAjusteBusqueda(TextCriteria textCriteria);

	/**
	 * Método para buscar anotaciones por el identificador del usuario ordenando
	 * el resultado por el indice de coincidencia del texto buscado con las
	 * anotaciones
	 * 
	 * @param textCriteria
	 *            {@link TextCriteria}
	 * @param id
	 * @return {@link List} {@link Anotacion}
	 */
	List<Anotacion> findByAndUsuarioIdOrderByAjusteBusqueda(TextCriteria textCriteria, String id);

	/**
	 * Método para buscar anotaciones por la privacidad de la anotación el
	 * resultado por el indice de coincidencia del texto buscado con las
	 * anotaciones
	 * 
	 * @param textCriteria
	 *            {@link TextCriteria}
	 * @param b
	 *            true si es privada las anotaciones listadas y false si es
	 *            privada
	 * @return {@link List} {@link Anotacion}
	 */
	List<Anotacion> findByAndPrivacidadOrderByAjusteBusqueda(TextCriteria textCriteria, boolean b);

	/**
	 * Método para buscar la anotación a la que pertenece el fichero buscado
	 * 
	 * @param idFichero
	 *            identificador del fichero
	 * @return {@link Anotacion}
	 */
	@Query("{\"masDatos.fichero\": ?0}, {\"masDatos.$\": 1}")
	Anotacion findByFichero(String idFichero);

	/**
	 * Método para buscar anotaciones por el identificador del usuario y la
	 * privacidad de esta, se ordena el resultado por el indice de coincidencia
	 * del texto buscado con la anotación
	 * 
	 * @param textCriteria
	 *            {@link TextCriteria}
	 * @param id
	 *            identificador de usuario
	 * @param b
	 *            true si la anotacion debe ser publica y false si debe ser
	 *            falsa
	 * @return {@link List} {@link Anotacion}
	 */
	List<Anotacion> findByAndUsuarioIdAndPrivacidadOrderByAjusteBusqueda(TextCriteria textCriteria, String id,
			boolean b);

	/**
	 * Método para buscar la anotación con más vistas
	 * 
	 * @return {@link Anotacion}
	 */
	Anotacion findFirstByOrderByContadorVistasDesc();

}
