package es.uniovi.cooksearch.persistencia;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import es.uniovi.cooksearch.modelo.tipo.TipoDeAnotacion;

/**
 * Clase con los servicios directos a la base de datos, extiende la clase
 * {@link MongoRepository} para que apartir de patrones en la signatura de los
 * metodos se realicen consultas directas. Documentos Tipos de Anotacion
 * 
 * @see MongoRepository
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public interface TipoDeAnotacionDao extends MongoRepository<TipoDeAnotacion, String> {

	/**
	 * Método para buscar un tipo de anotación por el identificador del usuario
	 * al que pertenece
	 * 
	 * @param usuarioId
	 *            identificador del usuario
	 * @return {@link List} {@link TipoDeAnotacion}
	 */
	List<TipoDeAnotacion> findByUsuarioId(String usuarioId);

	/**
	 * Método para buscar los tipos de anotaciones cuyo identificador de usuario
	 * sea null, estos tipos son los predefinidos por el sistema
	 * 
	 * @return {@link List} {@link TipoDeAnotacion}
	 */
	List<TipoDeAnotacion> findByUsuarioIdNull();

	/**
	 * Método para buscar un tipo de anotación por su nombre y por el
	 * identificador del usuario que lo realizo
	 * 
	 * @param nombre
	 *            nombre de la anotación
	 * @param usuarioId
	 *            identificador del usuario
	 * @return {@link TipoDeAnotacion}
	 */
	TipoDeAnotacion findByNombreAndUsuarioId(String nombre, String usuarioId);

	/**
	 * Método para buscar una anotación por el nombre del tipo de la anotación
	 * 
	 * @param tipoDeAnotacion
	 *            nombre de la anotación
	 * @return {@link TipoDeAnotacion}
	 */
	TipoDeAnotacion findByNombre(String tipoDeAnotacion);

}
