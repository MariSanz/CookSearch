package es.uniovi.cooksearch.persistencia;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import es.uniovi.cooksearch.modelo.Comentario;

/**
 * Clase con los servicios directos a la base de datos, extiende la clase
 * {@link MongoRepository} para que apartir de patrones en la signatura de los
 * metodos se realicen consultas directas. Documentos Comentario
 * 
 * @see MongoRepository
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public interface ComentarioDao extends MongoRepository<Comentario, String> {

	/**
	 * Método para buscar la lista de comentarios pertenecientes a la
	 * identificador de la anotación
	 * 
	 * @param anotacionId
	 *            identificador de la anotación
	 * @return {@link List} {@link Comentario}
	 */
	List<Comentario> findByAnotacionId(String anotacionId);

	/**
	 * Método buscar comentario por el identificador del usuario que realizo la
	 * busqueda y la anotación donde se busca
	 * 
	 * @param nombreUsuario
	 *            nombre del usuario que realizo el comentario
	 * @param anotacionId
	 *            identificador de la anotación donde buscar el comentario
	 * @return {@link Comentario}
	 */
	Comentario findByUsuarioAndAnotacionId(String nombreUsuario, String anotacionId);

	/**
	 * Método para buscar los comentarios por el texto del contenido donde
	 * buscar o por el usuario que la realizo
	 * 
	 * @param textCriteria
	 *            {@link TextCriteria}
	 * @param usuario
	 *            nombre del usuario que realizo la busqueda
	 * @return {@link List} {@link Comentario}
	 */
	List<Comentario> findByOrUsuario(TextCriteria textCriteria, String usuario);

	/**
	 * Método para buscar comentarios por el patron que puede seguir el nombre
	 * del usuario que los realizo, busca al inicio, el medio y el final del
	 * nombre del usuario que realizo el comentario
	 * 
	 * @param usuario
	 *            nombre del usuario a buscar
	 * @return {@link Collection} {@link Comentario}
	 */
	@Query("{\"usuario\": {$regex: ?0}}")
	Collection<? extends Comentario> findByUsuario(String usuario);

	/**
	 * Método para buscar comentarios por el patron que puede seguir el
	 * contenido del comentario, busca al inicio, el medio y el final de todo el
	 * contenido del comentario
	 * 
	 * @param contenido
	 *            contenido, información a buscar
	 * @return {@link Collection} {@link Comentario}
	 */
	@Query("{\"contenido\": {$regex: ?0}}")
	Collection<? extends Comentario> findByContenido(String contenido);

	/**
	 * Método para buscar los comentarios de una anotación segun esten aprobados
	 * o no
	 * 
	 * @param anotacionId
	 *            identificador de la anotacion donde buscar
	 * @param b
	 *            true si el comentario está aprobado false si no es así
	 * @return {@link List} {@link Comentario}
	 */
	List<Comentario> findByAnotacionIdAndAprobado(String anotacionId, boolean b);

	/**
	 * Método para buscar comentarios segun su atributo aprobado o no. Los
	 * retorna ordenados segun el objeto {@link Sort} que se le suministre
	 * 
	 * @param sort
	 *            {@link Sort}
	 * @param b
	 *            true si el comentario esta aprobado, false si no esta aprobado
	 * @return {@link List} {@link Comentario}
	 */
	List<Comentario> findByAprobado(Sort sort, boolean b);

	/**
	 * Método para buscar por contenido o usuario los comentarios, se busca por
	 * patrón al inicio, medio o fin de los atributos contenido y usuario en la
	 * base de datos
	 * 
	 * @param usuario
	 *            nombre del usuario a buscar
	 * @param contenido
	 *            información buscada en el contenido del comentario
	 * @return {@link Collection} {@link Comentario}
	 */
	@Query("{$or : [{\"usuario\": {$regex: ?0}}, {\"contenido\": {$regex: ?1}}]}")
	Collection<? extends Comentario> findByUsuarioOrContenido(String usuario, String contenido);

}
