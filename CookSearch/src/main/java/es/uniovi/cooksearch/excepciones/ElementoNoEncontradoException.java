package es.uniovi.cooksearch.excepciones;

/**
 * Clase de excepción de elemento no encontrado en la base de datos,
 * según el proceso
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public class ElementoNoEncontradoException extends Exception {

	private static final long serialVersionUID = 1L;

}
