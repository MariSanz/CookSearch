package es.uniovi.cooksearch.excepciones;

/**
 * Clase de excpeción de guardar en la base de datos un elemento cuyo
 * id ya existe en la base de datos
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public class ElementoRepetidoException extends Exception {

	private static final long serialVersionUID = 1L;

}
