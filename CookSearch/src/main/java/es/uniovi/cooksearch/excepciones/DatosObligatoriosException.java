package es.uniovi.cooksearch.excepciones;

/**
 * Clase de excepción cuando no se cumple con datos obligatorios
 * en los servicios de la aplicación, ejemplo correo electronico 
 * en el registro de usuarios
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public class DatosObligatoriosException extends Exception {

	private static final long serialVersionUID = 1L;

}
