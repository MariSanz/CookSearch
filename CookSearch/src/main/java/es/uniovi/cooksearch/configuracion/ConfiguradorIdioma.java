package es.uniovi.cooksearch.configuracion;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * Clase para configurar la internacionalización en la aplicación.
 * @see WebMvcConfigurerAdapter
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Configuration
public class ConfiguradorIdioma extends WebMvcConfigurerAdapter{
	
	/**
	 * Método para configurar el acceso al fichero con los mensajes de aviso que se mandan del controlador
	 * a la vista
	 * @return {@link MessageSource}
	 */
	@Bean
	public MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("messages");
		return messageSource;
	}
	
	/**
	 * Método para configurar el idioma por defecto de la aplicación al español (es)
	 * @return {@link LocaleResolver}
	 */
	@Bean
	public LocaleResolver localeResolver() {
		
		SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();
		sessionLocaleResolver.setDefaultLocale(new Locale("es"));
		return sessionLocaleResolver;
	}
	
	/**
	 * Método para indicar el nombre del parametro que se recoge para el cambio de idioma en 
	 * la aplicación. El nombre del parametro es "locale" y se cambia por el controlador
	 * y la vista según petición del usuario
	 * @param {@link InterceptorRegistry}
	 * {@inheritDoc}
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		
		LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
		localeChangeInterceptor.setParamName("locale");
		registry.addInterceptor(localeChangeInterceptor);
		
	}

}
