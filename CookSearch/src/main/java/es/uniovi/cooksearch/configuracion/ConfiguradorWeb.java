package es.uniovi.cooksearch.configuracion;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesView;

import es.uniovi.cooksearch.vista.DateConverter;

/**
 * Clase parala configuración de los controladores y las vistas de la
 * aplicación. Se define el controlador que responde al módulo de Spring
 * Security y el Bean para usar Apache Tiles para definir plantillas en las
 * páginas JSP de las vistas
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 */
@Configuration
@EnableWebMvc
public class ConfiguradorWeb extends WebMvcConfigurerAdapter {

	/**
	 * Método para cambiar la vista de la pagina de login, se personaliza y
	 * cambia la por defecto de Spring Security {@inheritDoc}
	 */
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {

		registry.addViewController("/login").setViewName("inicio.login");
	}

	/**
	 * Bean para que resuelva las vistas Apache Tiles
	 * 
	 * @return {@link ViewResolver}
	 */
	@Bean
	public ViewResolver getViewResolver() {
		UrlBasedViewResolver viewResolver = new UrlBasedViewResolver();
		viewResolver.setViewClass(TilesView.class);
		return viewResolver;
	}

	/**
	 * Bean para configurar la ruta de las vistas que resolverá Apache Tiles
	 * 
	 * @return {@link TilesConfigurer}
	 */
	@Bean
	public TilesConfigurer getTilesConfigurer() {
		TilesConfigurer tilesConfigurer = new TilesConfigurer();
		tilesConfigurer.setDefinitions("/WEB-INF/defs/**");
		return tilesConfigurer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addFormatters(FormatterRegistry registry) {
		super.addFormatters(registry);
		registry.addConverter(new DateConverter());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		if (!registry.hasMappingForPattern("/bootstrap/**")) {
			registry.addResourceHandler("/bootstrap/**").addResourceLocations("/bootstrap/");
		}
		if (!registry.hasMappingForPattern("/css/**")) {
			registry.addResourceHandler("/css/**").addResourceLocations("/css/");
		}
		if (!registry.hasMappingForPattern("/image/**")) {
			registry.addResourceHandler("/image/**").addResourceLocations("/image/");
		}
		if (!registry.hasMappingForPattern("/jquery/**")) {
			registry.addResourceHandler("/jquery/**").addResourceLocations("/jquery/");
		}
	}

}
