package es.uniovi.cooksearch.configuracion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.PlaintextPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Clase para configurar los permisos de los usuarios en la aplicación.
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Configuration
@EnableWebSecurity
public class ConfiguracionSeguridad extends WebSecurityConfigurerAdapter {
	
	/*
	 * Interfaz de Spring para buscar usuario, clave y los roles
	 */
	@Autowired
	private UserDetailsService usuarioService;

	/**
	 * Configura los filtros de acceso a las rutas de las páginas a los usuarios
	 * @param {@link HttpSecurity}
	 * {@inheritDoc}
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http
			.authorizeRequests()
				.antMatchers("/usuarios/registro").permitAll()
				.antMatchers("/anotaciones/nueva").hasRole("USUARIO")
				.antMatchers("/anotaciones/editar/**").hasRole("USUARIO")
				.antMatchers("/anotaciones/borrar/**").hasRole("USUARIO")
				//.antMatchers("/admin/**").hasRole("ADMIN")
				.antMatchers("/**").access("isAnonymous() or hasRole('USUARIO')")
			.and()
			.rememberMe()
				.key("j3jqir048jnxflk013872hacnjij23jfua093jned93hdl329")
			.and()
			.formLogin()
				.loginPage("/login");
			
	}
	
	/**
	 * Método para indicar el modelo que sirve a los datos de los usuarios en la aplicación y la codificación
	 * con la que se gestiona las contraseñas de acceso
	 * @param {@link AuthenticationManagerBuilder}
	 * {@inheritDoc}
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	
		auth
			.userDetailsService(usuarioService)
			.passwordEncoder(new PlaintextPasswordEncoder());
	}
}
