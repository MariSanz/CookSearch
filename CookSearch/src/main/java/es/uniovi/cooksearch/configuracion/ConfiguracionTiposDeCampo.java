package es.uniovi.cooksearch.configuracion;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Clase que gestiona el bean por xml de los tipos de campos 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Configuration
@ImportResource("classpath:/tipos_de_campo.xml")
public class ConfiguracionTiposDeCampo {

}
