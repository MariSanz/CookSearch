package es.uniovi.cooksearch.configuracion;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Clase que configura el fichero desde el que se lee propiedades usadas en la aplicación
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Configuration
@PropertySource("classpath:/config.properties")
public class ConfiguracionGlobal {

}
