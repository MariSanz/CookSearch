package es.uniovi.cooksearch.controlador;

import org.springframework.web.bind.annotation.ModelAttribute;

import es.uniovi.cooksearch.vista.anotaciones.AnotacionBuscarForm;

/**
 * Clase de controlador para extender la funcionalidad de buscar en todas las
 * páginas (controladores) de la web
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 */
public class BaseController {

	/**
	 * Método que retornar el formulario de buscar formulario necesario en los
	 * controladores de las páginas desde las que se puede acceder a buscar
	 * 
	 * @return {@link AnotacionBuscarForm}
	 */
	@ModelAttribute("anotacionBuscarForm")
	public AnotacionBuscarForm getAnotacionBuscarForm() {
		return new AnotacionBuscarForm();

	}
}