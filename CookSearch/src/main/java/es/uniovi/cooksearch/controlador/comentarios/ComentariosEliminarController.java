package es.uniovi.cooksearch.controlador.comentarios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import es.uniovi.cooksearch.negocio.ComentarioService;

/**
 * Clase del controlador para procesar las peticiones de eliminar comentarios
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 */
@Controller
public class ComentariosEliminarController {

	/*
	 * Servicio para guardar los comentarios en la base de datos
	 */
	@Autowired
	private ComentarioService comentarioService;

	/**
	 * Método de borrado para la lista de comentarios sin aprobar. Se diferencia en 
	 * dos metodos para procesar el borrado de comentarios para redirigir a los dos
	 * listados diferentes de los comentarios
	 * @param id identificador del comentarios a borrar
	 * @param model {@link Model}
	 * @return {@link String} redirect:/admin/comentarios/listado-aprobar
	 */
	@RequestMapping(value = "/admin/comentarios/borrar/sa", method = RequestMethod.POST)
	public String borrarSinAprobar(@RequestParam("idComentario") String idComentario, @RequestParam("idAnotacion") String idAnotacion, Model model) {
		
		boolean correcta = comentarioService.borrar(idComentario, idAnotacion);
		
		if (correcta) {
			model.addAttribute("mensaje", "comentario.borrarCorrecto");

		} else {
			model.addAttribute("mensaje", "comentario.borrarIncorrecto");
		}
		
		return "redirect:/admin/comentarios/listado-aprobar";
		
	}
	
	/**
	 * Método para borrar comentarios de la lista de comentarios aprobados
	 * @param id identificador del comentario a borrar
	 * @param model {@link Model}
	 * @return ruta a comentarios aprobados
	 */
	@RequestMapping(value = "/admin/comentarios/borrar", method = RequestMethod.POST)
	public String borrarAprobados(@RequestParam("idComentario") String idComentario, @RequestParam("idAnotacion") String idAnotacion, Model model) {
		
		boolean correcta = comentarioService.borrar(idComentario, idAnotacion );
		
		if (correcta) {
			model.addAttribute("mensaje", "comentario.borrarCorrecto");

		} else {
			model.addAttribute("mensaje", "comentario.borrarIncorrecto");
		}
		
		return "redirect:/admin/comentarios/listado";
		
	}

}
