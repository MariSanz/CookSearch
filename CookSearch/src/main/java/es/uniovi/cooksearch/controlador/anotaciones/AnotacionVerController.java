package es.uniovi.cooksearch.controlador.anotaciones;

import java.security.Principal;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import es.uniovi.cooksearch.controlador.BaseController;
import es.uniovi.cooksearch.negocio.AnotacionService;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionVerForm;

/**
 * Clase controlador del proceso de buscar anotaciones
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Controller
public class AnotacionVerController  extends BaseController{

	/*
	 * Servicio de las anotaciones a la base de datos
	 */
	@Autowired
	private AnotacionService anotacionService;

	/**
	 * Método para visualizar las anotaciones por el ID en la ruta
	 * 
	 * @param id
	 *            string variable Id de la ruta de petición
	 * @param br
	 *            {@link BindingResult} recoger posibles errores de la vista al
	 *            procesar los datos del usuario
	 * @param model
	 *            {@link Model} modelo para dotar de atributos la pagina de
	 *            respuesta, la anotación obtenida en el servicio
	 * @return {@link String} ruta de la pagina a recargar luego de procesar.
	 */
	@RequestMapping(value = "/anotaciones/ver/{indice}-{id}", method = RequestMethod.GET)
	public String ver(@PathVariable("id") final String id, Model model, Principal principal) {
		/* Obtener el usuario que está en sessión */
		String usuarioSession ;
		
		if(principal == null){
			usuarioSession = "anonymousUser";
		}else{
			usuarioSession = principal.getName();
		}

		AnotacionVerForm anotacion = anotacionService.ver(id, usuarioSession);

		Boolean propia = usuarioSession.equals(anotacion.getUsuario());
		anotacion.setPropia(propia);

		model.addAttribute("usuarioSession", usuarioSession);
		model.addAttribute("anotacion", anotacion);

		/*
		 * Para recuperar la anotación que se está visualizando en acciones como
		 * puntuar y comentar o marcha atrás
		 */
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();

		session.setAttribute("anotacion_ver", anotacion);

		return "anotaciones.ver";
	}

}
