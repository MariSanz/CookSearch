package es.uniovi.cooksearch.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import es.uniovi.cooksearch.negocio.AnotacionService;
import es.uniovi.cooksearch.negocio.ComentarioService;
import es.uniovi.cooksearch.negocio.TipoDeAnotacionService;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionBuscarForm;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionVerForm;
import es.uniovi.cooksearch.vista.comentarios.ComentarioListarForm;

/**
 * Controlador de la página de inicio de la aplicación, tanto para usuarios
 * anonimos, el administrador y otros usuarios registrados
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Controller
public class IndexController extends BaseController{

	/*
	 * Servicio de las anotaciones a la base de datos
	 */
	@Autowired
	private AnotacionService anotacionService;

	/*
	 * Servicio de los comentarios a la base de datos
	 */
	@Autowired
	private ComentarioService comentarioService;
	
	@Autowired
	private TipoDeAnotacionService tipoAnotacionService;

	/**
	 * Método de la ruta de acceso a la página principal de la aplicación, ruta
	 * raíz. Si el usuario es el admnistrador redirige a una pagina distinta que
	 * el usuario publico o que ha iniciado sesión. Se cargan los datos
	 * necesarios en la vista.
	 * 
	 * @param model
	 *            {@link Model}
	 * @return String ruta en tiles de la página
	 */
	@RequestMapping("/")
	public String index(Model model) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String nombreUsuario = auth.getName();

		if (nombreUsuario.equals("admin")) {
			return "admin.index";
		} else {

			// Nombre anonimo del usuario en el sistema: anonymousUser
			AnotacionVerForm anotacion = anotacionService.findByContadorMasVistas();

			model.addAttribute("masVista", anotacion);

			List<ComentarioListarForm> ultimosComentarios = comentarioService.findUltimos20Comentarios();
			model.addAttribute("ultimosComentarios", ultimosComentarios);

			model.addAttribute("anotacionBuscarForm", new AnotacionBuscarForm());
			model.addAttribute("tiposDeAnotacion", tipoAnotacionService.getTipoAnotacionUsuario(nombreUsuario));

			return "publico.index";
		}
	}

	/**
	 * Método de la ruta de acceso a la página de visualizar las condiciones de
	 * uso
	 * 
	 * @param model
	 *            {@link Model}
	 * @return String ruta en tiles de la página
	 */
	@RequestMapping("/condiciones-uso")
	public String condiciones(Model model) {

		return "publico.condicionesUso";
	}

	/**
	 * Método de la ruta de acceso a la página de visualizar la política de
	 * privacidad
	 * 
	 * @param model
	 *            {@link Model}
	 * @return String ruta en tiles de la página
	 */
	@RequestMapping("/politica-privacidad")
	public String privacidad(Model model) {

		return "publico.politicaPrivacidad";
	}

	/**
	 * Método de la ruta de acceso a la página de visualizar el Aviso Legal
	 * 
	 * @param model
	 *            {@link Model}
	 * @return String ruta en tiles de la página
	 */
	@RequestMapping("/aviso-legal")
	public String avisoLegal(Model model) {

		return "publico.avisoLegal";
	}

	/**
	 * Método de la ruta de acceso a la página de visualizar la política de
	 * cookies
	 * 
	 * @param model
	 *            {@link Model}
	 * @return String ruta en tiles de la página
	 */
	@RequestMapping("/politica-cookies")
	public String cookies(Model model) {

		return "publico.politicaCookies";
	}

}
