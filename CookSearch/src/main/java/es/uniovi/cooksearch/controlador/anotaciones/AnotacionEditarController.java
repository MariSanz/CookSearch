package es.uniovi.cooksearch.controlador.anotaciones;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.uniovi.cooksearch.controlador.BaseController;
import es.uniovi.cooksearch.excepciones.ElementoNoEncontradoException;
import es.uniovi.cooksearch.modelo.campos.TipoDeCampo;
import es.uniovi.cooksearch.modelo.tipo.TipoDeAnotacion;
import es.uniovi.cooksearch.negocio.AnotacionService;
import es.uniovi.cooksearch.negocio.TipoDeAnotacionService;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionEditarForm;

/**
 * Clase del controlador para procesar las peticiones de editar anotaciones
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Controller
public class AnotacionEditarController extends BaseController{

	/*
	 * En el se cargan las propiedades de configuración de la aplicación
	 */
	@Autowired
	private ApplicationContext context;

	/*
	 * Servicio de los tipos de anotación a la base de datos
	 */
	@Autowired
	private TipoDeAnotacionService tipoAnotacionService;

	/*
	 * Servicio de las anotaciones a la base de datos
	 */
	@Autowired
	private AnotacionService anotacionService;
	
	/**
	 * Método para procesar la edición de las anotaciones
	 * 
	 * @param id
	 *            string variable Id de la ruta de petición
	 * @param model
	 *            {@link Model} modelo para dotar de atributos la pagina de
	 *            respuesta
	 * @return {@link String} ruta de la pagina a recargar luego de procesar.
	 */
	@RequestMapping(value = "/anotaciones/editar/{indice}-{id}", method = RequestMethod.GET)
	public String editar(@PathVariable("id") final String id, Model model) {

		model.addAttribute("anotacionEditarForm", anotacionService.verEditar(id));

		return "anotaciones.editar";
	}
	
	/**
	 * Método que procesa la edición de anotaciones
	 * @param id
	 * @param anotacionEditarForm {@link AnotacionEditarForm}
	 * @param br
	 * @return 
	 * @throws ElementoNoEncontradoException
	 */
	@RequestMapping(value = "/anotaciones/editar/{indice}-{id}", method = RequestMethod.POST)
	public String ejecutar(@PathVariable String id, AnotacionEditarForm anotacionEditarForm, BindingResult br)
		throws ElementoNoEncontradoException{
		
		if (br.hasErrors()) {
			
			return "anotaciones.editar";
		}
		
		anotacionService.editar(id, anotacionEditarForm);
		
		return "redirect:/anotaciones/ver/" + id;
		
	}
	
	/**
	 * Método para simplificar la logica del método editar, este metodo procesa directamente
	 * el modelo del atributo tipos de campo
	 * @return 
	 */
	@ModelAttribute("tiposDeCampo")
	public Iterable<Map.Entry<String, TipoDeCampo>> getTiposDeCampo() {
		Map<String, TipoDeCampo> tiposDeCampo = context.getBeansOfType(TipoDeCampo.class);
		return tiposDeCampo.entrySet();
	}
	
	/**
	 * Método para simplificar la logica del método editar, este metodo procesa directamente
	 * el modelo del atributo tipos de anotación
	 * @return 
	 */
	@ModelAttribute("tiposDeAnotacion")
	public List<TipoDeAnotacion> getTiposDeAnotacion(Principal principal) {
		String nombreUsuario = principal.getName();
		return tipoAnotacionService.getTipoAnotacionUsuario(nombreUsuario);
	}

}
