package es.uniovi.cooksearch.controlador.anotaciones;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import es.uniovi.cooksearch.controlador.BaseController;
import es.uniovi.cooksearch.negocio.AnotacionService;
import es.uniovi.cooksearch.vista.UltimaBusqueda;

/**
 * Clase del controlador para procesar las peticiones de borrar anotaciones
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Controller
public class AnotacionBorrarController extends BaseController{

	/*
	 * Servicio de las anotaciones a la base de datos
	 */
	@Autowired
	private AnotacionService anotacionService;

	/**
	 * Método para borrar las anotaciones por el ID en la ruta
	 * 
	 * @param id
	 *            string variable Id de la ruta de petición
	 * @param br
	 *            {@link BindingResult} recoger posibles errores de la vista al
	 *            procesar los datos del usuario
	 * @param model
	 *            {@link Model} modelo para dotar de atributos la pagina de
	 *            respuesta
	 * @return {@link String} ruta de la pagina a recargar luego de procesar.
	 */
	@RequestMapping(value = "/anotaciones/borrar/{id}", method = RequestMethod.POST)
	public String borar(@RequestParam("id") final String id, Model model, HttpSession sesion) {

		boolean correcta = anotacionService.borrar(id);
		
		if (correcta) {
			model.addAttribute("mensaje", "anotacion.borrarCorrecto");

		} else {
			model.addAttribute("mensaje", "anotacion.borrarIncorrecto");
		}


		UltimaBusqueda ub = (UltimaBusqueda)sesion.getAttribute("busqueda");
		return "redirect:" + ub.toUrl();
	}

}
