package es.uniovi.cooksearch.controlador.anotaciones;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.uniovi.cooksearch.controlador.BaseController;
import es.uniovi.cooksearch.excepciones.DatosObligatoriosException;
import es.uniovi.cooksearch.excepciones.ElementoRepetidoException;
import es.uniovi.cooksearch.modelo.campos.TipoDeCampo;
import es.uniovi.cooksearch.negocio.AnotacionService;
import es.uniovi.cooksearch.negocio.TipoDeAnotacionService;
import es.uniovi.cooksearch.negocio.UsuarioService;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionNuevaForm;

/**
 * Clase del controlador para procesar las peticiones de nuevas anotaciones
 * @author María José Sánchez Doria
 * @version 1.0
 */
@Controller
public class AnotacionNuevaController extends BaseController{

	/*
	 * En el se cargan las propiedades de configuración de la aplicación
	 */
	@Autowired
	private ApplicationContext context;

	/*
	 * Servicio de las anotaciones en la base de datos
	 */
	@Autowired
	private AnotacionService anotacionService;

	/*
	 * Servicio de los usuarios en la base de datos
	 */
	@Autowired
	private UsuarioService usuarioService;

	/*
	 * Servicio de los tipos de anotacion en la base de datos
	 */
	@Autowired
	private TipoDeAnotacionService tipoAnotacionService;

	/**
	 * Método para servir la ruta de la pagina para nuevas anotaciones, se
	 * cargan de la base de datos los datos que necesita este formulario, como
	 * es los tipos de anotación predefinidos y los propios del usuario
	 * 
	 * @param model
	 *            {@link Model} modelo para dotar de atributos la pagina de
	 *            respuesta
	 * @return {@link String} ruta de la pagina a recargar luego de procesar.
	 */
	@RequestMapping(value = "/anotaciones/nueva", method = RequestMethod.GET)
	public String ver(Model model) {

		Map<String, TipoDeCampo> tiposDeCampo = context.getBeansOfType(TipoDeCampo.class);
		model.addAttribute("tiposDeCampo", tiposDeCampo.entrySet());

		// consulto todos los tipos de anotacion predefinidos mas del usuario
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String nombreUsuario = auth.getName();

		model.addAttribute("tiposDeAnotacion", tipoAnotacionService.getTipoAnotacionUsuario(nombreUsuario));

		model.addAttribute("anotacionNuevaForm", new AnotacionNuevaForm());
		return "anotaciones.nueva";
	}

	/**
	 * Método para procesar la nueva anotación, los datos y enviarlos a los
	 * servicios para guardarla en la base de datos
	 * 
	 * @param nuevaAnotacionForm
	 *            {@link AnotacionNuevaForm} Objeto que recoge los datos de la
	 *            vista (del formulario)
	 * @param br
	 *            {@link BindingResult} recoger posibles errores de la vista al
	 *            procesar los datos del usuario
	 * @param model
	 *            {@link Model} modelo para dotar de atributos la pagina de
	 *            respuesta
	 * @return {@link String} ruta de la pagina a recargar luego de procesar.
	 * @throws ElementoRepetidoException
	 *             {@link ElementoRepetidoException} excepción propia del modelo
	 */
	@RequestMapping(value = "/anotaciones/nueva", method = RequestMethod.POST)
	public String ejecutar(@Valid AnotacionNuevaForm nuevaAnotacionForm, BindingResult br, Model model)
			throws ElementoRepetidoException {

		if (br.hasErrors()) {
			// consulto todos los tipos de anotacion predefinidos mas del
			// usuario

			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String nombreUsuario = authentication.getName();

			model.addAttribute("tiposDeAnotacion", tipoAnotacionService.getTipoAnotacionUsuario(nombreUsuario));

			return "anotaciones.nueva";
		}

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String nombreUsuario = auth.getName();

		nuevaAnotacionForm.setUsuarioId(usuarioService.getIdByName(nombreUsuario));

		try {
			anotacionService.guardar(nuevaAnotacionForm);
		} catch (DatosObligatoriosException e) {
			e.printStackTrace();
		}

		return "redirect:/";
	}

}
