package es.uniovi.cooksearch.controlador.comentarios;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import es.uniovi.cooksearch.modelo.Comentario;
import es.uniovi.cooksearch.negocio.ComentarioService;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionVerForm;

/**
 * Controlador de los nuevos comentarios que realizan los usuarios
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Controller
public class ComentariosNuevoController {

	/*
	 * Servicio para guardar los comentarios en la base de datos
	 */
	@Autowired
	private ComentarioService comentarioService;

	/**
	 * Método que procesa el id de la anotación, el usuario que realiza el
	 * comenario y lo envía al servicio para que lo guarde en la base de datos
	 * 
	 * @param anotacion
	 *            formulario de la anotación desde el que se leen los datos
	 *            necesarios para guardar los comentarios
	 *            {@link AnotacionVerForm}
	 * @param model
	 *            Modelo de los atributos que se pasan al formulario de la vista
	 *            {@link Model}
	 * @return ruta de la vista a la que redirigir cuando se procese la petición
	 */
	@RequestMapping(value = "/comentarios/guardar", method = RequestMethod.POST)
	public ResponseEntity<Comentario> comentar(@RequestBody AnotacionVerForm anotacion, Model model,
			Principal principal) {
		String nombreUsuario = principal.getName();

		Comentario comentario = comentarioService.guardar(nombreUsuario, anotacion.getNuevoComentario(),
				anotacion.getAnotacionId());

		if (comentario != null) {
			return ResponseEntity.ok(comentario);
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
	}

	/**
	 * Método que el administrador apruebe los comentarios
	 * 
	 * @param id
	 *            identificador del comentario a borrar
	 * @param model {@link Model}
	 * @param principal
	 *            {@link Principal} objeto con los parametros de la sesión y las
	 *            peticiones
	 * @return redirect:/admin/comentarios/listado-aprobar
	 */
	@RequestMapping(value = "admin/comentarios/aprobar", method = RequestMethod.POST)
	public String aprobar(@RequestParam("id") String id, Model model, Principal principal) {

		comentarioService.aprobar(id);

		return "redirect:/admin/comentarios/listado-aprobar";
	}

}
