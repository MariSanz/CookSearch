package es.uniovi.cooksearch.controlador.comentarios;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.uniovi.cooksearch.negocio.ComentarioService;
import es.uniovi.cooksearch.vista.comentarios.ComentarioListarForm;

/**
 * Controlador para procesar el listado de los comentarios
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 */
@Controller
public class ComentariosListarController {

	/*
	 * Servicio para guardar los comentarios en la base de datos
	 */
	@Autowired
	private ComentarioService comentarioService;

	/**
	 * Atiende la ruta del listado de todos los comentarios al administrador
	 * 
	 * @param model
	 *            {@link Model}
	 * @return admin.comentarios
	 */
	@RequestMapping(value = "/admin/comentarios/listado", method = RequestMethod.GET)
	public String listar(Model model) {

		List<ComentarioListarForm> comentarios = comentarioService.findByAprobar(true);

		model.addAttribute("listadoComentarios", comentarios);

		return "admin.comentarios";

	}

	/**
	 * Método para listar los comentarios no aprobados por el administrador
	 * 
	 * @param model
	 *            {@link Model}
	 * @return admin.comentariosSinAprobar
	 */
	@RequestMapping(value = "/admin/comentarios/listado-aprobar", method = RequestMethod.GET)
	public String listarNoAprobados(Model model) {
		List<ComentarioListarForm> comentarios = comentarioService.findByAprobar(false);
		model.addAttribute("listadoComentarios", comentarios);
		return "admin.comentariosSinAprobar";

	}

	/**
	 * Método para buscar comentarios por el contenido del comentario o el
	 * nombre del usuario que realizo el comentario. Busca entre los comentarios
	 * aprobados y no aprobados
	 * 
	 * @param form
	 *            {@link ComentarioListarForm}
	 * @param model
	 *            {@link Model}
	 * @return admin.comentarios
	 */
	@RequestMapping(value = "/admin/comentarios/buscar", method = RequestMethod.POST)
	public String buscar(@Valid ComentarioListarForm form, Model model) {

		List<ComentarioListarForm> comentarios = comentarioService.buscar(form.getContenido(), form.getUsuario());

		model.addAttribute("listadoComentarios", comentarios);

		return "admin.comentariosBuscar";

	}

}
