package es.uniovi.cooksearch.controlador.anotaciones;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import es.uniovi.cooksearch.negocio.AnotacionService;
import es.uniovi.cooksearch.negocio.TipoDeAnotacionService;
import es.uniovi.cooksearch.vista.UltimaBusqueda;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionBuscarForm;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionListadoForm;

/**
 * Clase controlador del proceso de buscar anotaciones
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Controller
public class AnotacionBuscarController {

	/*
	 * Servicio de las anotaciones a la base de datos
	 */
	@Autowired
	private AnotacionService anotacionService;

	@Autowired
	private TipoDeAnotacionService tipoAnotacionService;

	/**
	 * Método para refrescar el listado de anotaciones buscadas en acciones de
	 * borrado o vuelta atras en el navegador, es el metodo Get del listado
	 * 
	 * @param buscarAnotacionForm
	 *            {@link AnotacionBuscarForm} Objeto que recoge los datos de la
	 *            vista (del formulario)
	 * @param model
	 *            {@link Model} modelo para dotar de atributos la pagina de
	 *            respuesta
	 * @param br
	 *            {@link BindingResult} recoger posibles errores de la vista al
	 *            procesar los datos del usuario
	 * @return {@link String} ruta de la pagina a recargar luego de procesar.
	 */
	@RequestMapping(value = "/anotaciones/listado", method = RequestMethod.GET)
	public String buscar(@Valid AnotacionBuscarForm buscarAnotacionForm, Model model, BindingResult br,
			Principal principal) {

		if (br.hasErrors()) {
			return "redirect:/";
		}

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();

		String textoBuscar = (String) session.getAttribute("textoBuscar");

		List<AnotacionListadoForm> anotacionesForm = null;

		String nombreUsuario = principal.getName();

		if (nombreUsuario.equals("anonymousUser")) {
			anotacionesForm = anotacionService.buscar(textoBuscar);
		} else {
			// añado las propias
			anotacionesForm = anotacionService.buscar(textoBuscar, nombreUsuario);
			// añado todas las publicas de otros usuarios del usuario registrado
			anotacionesForm.addAll(anotacionService.buscar(textoBuscar));
		}

		model.addAttribute("anotacionesListadoForm", anotacionesForm);
		model.addAttribute("tiposDeAnotacion", tipoAnotacionService.getTipoAnotacionUsuario(nombreUsuario));


		return "anotaciones.listado";

	}

	/**
	 * Método para listar las anotaciones que el usuario busca, es el metodo
	 * Post del listado Atiende las búsqueda simple y la avanzado
	 * 
	 * @param buscarAnotacionForm
	 *            {@link AnotacionBuscarForm} Objeto que recoge los datos de la
	 *            vista (del formulario)
	 * @param model
	 *            {@link Model} modelo para dotar de atributos la pagina de
	 *            respuesta
	 * @param br
	 *            {@link BindingResult} recoger posibles errores de la vista al
	 *            procesar los datos del usuario
	 * @return {@link String} ruta de la pagina a recargar luego de procesar.
	 */
	@RequestMapping(value = "/anotaciones/listado-simple", method = RequestMethod.GET)
	public String buscarPost(@Valid AnotacionBuscarForm buscarAnotacionForm, Model model, BindingResult br, HttpSession sesion) {

		if (br.hasErrors()) {
			return "redirect:/";
		}

		String textoBuscar = buscarAnotacionForm.getTextoBuscar();
		
		sesion.setAttribute("busqueda", UltimaBusqueda.simple(textoBuscar));

		List<AnotacionListadoForm> anotacionesForm = null;

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String nombreUsuario = auth.getName();

		if (nombreUsuario.equals("anonymousUser")) {
			anotacionesForm = anotacionService.buscar(textoBuscar);// añado solo
																	// las
																	// publicas
		} else {
			// añado las propias
			anotacionesForm = anotacionService.buscar(textoBuscar, nombreUsuario);
			// añado todas las publicas de otros usuarios del usuario registrado
			anotacionesForm.addAll(anotacionService.buscar(textoBuscar));
		}

		model.addAttribute("anotacionesListadoForm", anotacionesForm);

		/*
		 * Para recuperar el listado de anotaciones que se está visualizando,
		 * cuando el usuario vuelva a tras
		 */
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		HttpSession session = attr.getRequest().getSession();

		session.setAttribute("anotacion_listado", anotacionesForm);
		session.setAttribute("textoBuscar", textoBuscar);

		return "anotaciones.listado";

	}

	/**
	 * Método para procesar la recogida de todos los datos (filtros) de la
	 * búqueda avanzada
	 * 
	 * @param texto
	 *            {@link String} texto a buscar entre los datos de las
	 *            anotaciones
	 * @param puntos
	 *            {@link Integer} puntuación minima a buscar
	 * @param desde
	 *            fecha en String que recoge la vista para desde
	 * @param hasta
	 *            fecha en String que recoge la vista para hasta
	 * @param publica
	 *            {@link Boolean} privacidad de la anotacion, true es publica
	 * @param privada
	 *            {@link Boolean} privacidad de la anotacion, true es privada,
	 *            se puede buscar con ambas privacidades
	 * @param model
	 *            {@link Model} modelo para dotar de atributos la pagina de
	 *            respuesta
	 * @return {@link String} ruta de la pagina a recargar luego de procesar.
	 */
	@RequestMapping(value = "/anotaciones/listado_avanzado", method = RequestMethod.GET)
	public String buscarAvanzada(AnotacionBuscarForm anotacionBuscarForm, BindingResult br, Model model, HttpSession sesion) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String nombreUsuario = auth.getName();
		if (br.hasErrors()) {

			throw new RuntimeException();
		}
		
		sesion.setAttribute("busqueda", UltimaBusqueda.avanzada(anotacionBuscarForm));

		List<AnotacionListadoForm> anotacionesForm = anotacionService.buscar(anotacionBuscarForm);

		model.addAttribute("anotacionesListadoForm", anotacionesForm);

		sesion.setAttribute("anotacion_listado", anotacionesForm);
		
		model.addAttribute("tiposDeAnotacion", tipoAnotacionService.getTipoAnotacionUsuario(nombreUsuario));

		return "anotaciones.listado";
	}

	/**
	 * Método que convierte una fecha en String a un objeto Date. Convertir el
	 * Date de los objetos del modelo de la vista a la fecha que se guardan en
	 * la base como string
	 * 
	 * @param fechaString
	 *            String
	 * @return {@link Date}
	 */
	public Date convertirDate(final String fechaString) {
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");

		if (fechaString != null) {
			Date fecha = null;

			try {

				fecha = formatoDelTexto.parse(fechaString);

			} catch (ParseException ex) {

				ex.printStackTrace();

			}

			return fecha;
		} else {
			return null;
		}

	}

}
