package es.uniovi.cooksearch.controlador;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.uniovi.cooksearch.excepciones.ElementoRepetidoException;
import es.uniovi.cooksearch.negocio.UsuarioService;
import es.uniovi.cooksearch.vista.RegistroForm;

/**
 * Clase controlador del proceso de registro de usuarios
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Controller
public class RegistroController {

	/*
	 * Servicio de los usuarios a la base de datos
	 */
	@Autowired
	private UsuarioService usuarioService;

	/**
	 * Método de la ruta de acceso a la página de registro, sirve la página y
	 * los datos que necesita. Otro metodo se encarga del proceso de recoger los
	 * datos del usuario y comprobarlos
	 * 
	 * @param model
	 *            {@link Model}
	 * @return String ruta en tiles de la página
	 */
	@RequestMapping(value = "/usuarios/registro", method = RequestMethod.GET)
	public String registro(Model model) {
		model.addAttribute("registroForm", new RegistroForm());
		return "inicio.registro";
	}

	/**
	 * Método para procesar el proceso de registro del usuario
	 * 
	 * @param registroForm
	 *            {@link RegistroForm} Objeto que recoge los datos de la vista
	 *            (del formulario)
	 * @param br
	 *            {@link BindingResult} recoger posibles errores de la vista al
	 *            procesar los datos del usuario
	 * @param ra
	 *            {@link RedirectAttributes} envio de mensajes de errores y
	 *            respuesta a la vista actual
	 * @return {@link String} ruta de la pagina a recargar luego de procesar.
	 *         Index si ha sido exitoso el registro la pagina de registro con
	 *         los errores
	 */
	@RequestMapping(value = "/usuarios/registro", method = RequestMethod.POST)
	public String registro(@Valid RegistroForm registroForm, BindingResult br, RedirectAttributes ra) {

		if (br.hasErrors()) {// hubo errores en los datos de la vista
			return "inicio.registro";
		}

		// si la contraseña y su repetición coinciden
		if (!registroForm.getPassword().equals(registroForm.getRepPassword())) {
			br.addError(new FieldError("registroForm", "repPassword", "{repetir_clave}"));
			return "publico.registro";
		} else if (registroForm.getUsuario() == null) {
			br.addError(new FieldError("registroForm", "usuario", "{usuario_necesario}"));
		}

		try {
			// se trata el la logica de registro desde los servicios
			usuarioService.registrar(registroForm);

		} catch (ElementoRepetidoException e) {
			br.addError(new FieldError("registroForm", "email", "{usuario_repetido}"));
			return "inicio.registro";
		}

		ra.addFlashAttribute("mensaje", "usuario_registrado");

		return "redirect:/";
	}

}
