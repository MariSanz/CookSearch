package es.uniovi.cooksearch.controlador.anotaciones;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import es.uniovi.cooksearch.excepciones.ElementoNoEncontradoException;
import es.uniovi.cooksearch.negocio.AnotacionService;
import es.uniovi.cooksearch.vista.anotaciones.FicheroForm;

/**
 * Clase para procesar la descarga o muestra del fichero almacenado en el
 * servidor, se recoge el path del fichero desde la base de datos y se devuelve
 * su contenido
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 */
@Controller
public class AccesoFicheroController {

	/*
	 * Acceso por inyección de dependencia del Service de Anotacion
	 */
	@Autowired
	private AnotacionService anotacionService;

	/**
	 * Método para representar el contenido de los ficheros según el tipo del
	 * fichero
	 * 
	 * @param id
	 *            {@link String}
	 * @return {@link ResponseEntity}
	 * @throws ElementoNoEncontradoException
	 */
	@RequestMapping("/ficheros/{id}")
	public ResponseEntity<InputStreamResource> ver(@PathVariable String id) throws ElementoNoEncontradoException {

		FicheroForm fichero = anotacionService.verFichero(id);

		return ResponseEntity.ok().header("Content-Type", fichero.getContentType())
				.header("Content-Disposition", ";filename=\"" + fichero.getNombre() + "\"")
				.body(new InputStreamResource(fichero.getInputStream()));
	}

	/**
	 * Descargar fichero
	 * 
	 * @param id
	 *            del fichero en el servidor
	 * @return {@link ResponseEntity}
	 * @throws ElementoNoEncontradoException
	 *             excepción cuando el fichero no fue encontrado en la base de
	 *             datos o en el servidor
	 */
	@RequestMapping("/ficheros/{id}/descargar")
	public ResponseEntity<InputStreamResource> descargar(@PathVariable String id) throws ElementoNoEncontradoException {

		FicheroForm fichero = anotacionService.verFichero(id);

		return ResponseEntity.ok().header("Content-Type", fichero.getContentType())
				.header("Content-Disposition", "attachment;filename=\"" + fichero.getNombre() + "\"")
				.body(new InputStreamResource(fichero.getInputStream()));
	}
}
