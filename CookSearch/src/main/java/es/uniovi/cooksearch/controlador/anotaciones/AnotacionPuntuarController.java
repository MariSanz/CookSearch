package es.uniovi.cooksearch.controlador.anotaciones;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import es.uniovi.cooksearch.controlador.BaseController;
import es.uniovi.cooksearch.excepciones.ElementoNoEncontradoException;
import es.uniovi.cooksearch.excepciones.ElementoRepetidoException;
import es.uniovi.cooksearch.negocio.AnotacionService;
import es.uniovi.cooksearch.vista.anotaciones.AnotacionVerForm;
import es.uniovi.cooksearch.vista.anotaciones.RespuestaVotacionForm;
import es.uniovi.cooksearch.vista.anotaciones.VotoForm;

/**
 * Clase del controlador para actualizar las puntuaciones de una anotación
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Controller
public class AnotacionPuntuarController extends BaseController {

	/*
	 * Servicio de las anotaciones a la base de datos
	 */
	@Autowired
	private AnotacionService anotacionService;

	/**
	 * Método para actualizar la puntuación del 1 al 5 de las anotaciones. La
	 * petición es lanzada por Ajax, el data se formaliza por JSON con el nombre
	 * de los atributos que corresponden en la clase AnotacionVerForm. De está
	 * manera Spring nos devuleve encapculado en este tipo de objeto los
	 * atributos que nos interesa de él
	 * 
	 * @param anotacionForm
	 *            {@link AnotacionVerForm}
	 * @return Ruta de la pagina a pintar en la vista
	 */
	@RequestMapping(value = "/puntuaciones/guardar", method = RequestMethod.POST)
	public ResponseEntity<RespuestaVotacionForm> puntuar(VotoForm votoForm, Model model, Principal principal)
			throws ElementoNoEncontradoException {

		String nombreUsuario = principal.getName();

		try {

			RespuestaVotacionForm respuesta = anotacionService.votar(votoForm, nombreUsuario);
			return ResponseEntity.ok(respuesta);
		} catch (ElementoRepetidoException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
		}
	}

}
