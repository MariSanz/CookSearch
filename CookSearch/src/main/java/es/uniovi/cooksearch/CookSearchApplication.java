package es.uniovi.cooksearch;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoOperations;

import es.uniovi.cooksearch.modelo.Anotacion;
import es.uniovi.cooksearch.persistencia.utils.TodosLosCamposTextIndexDefinitions;

@SpringBootApplication
public class CookSearchApplication extends SpringBootServletInitializer  {

	public static void main(String[] args) {
		SpringApplication.run(CookSearchApplication.class, args);
	}
	
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CookSearchApplication.class);
    }
	
	@Bean
	public CommandLineRunner crearIndices(final MongoOperations mongoOperations) {
		
		return new CommandLineRunner() {
			
			@Override
			public void run(String... arg0) throws Exception {
				
				TodosLosCamposTextIndexDefinitions indiceAnotacion = new TodosLosCamposTextIndexDefinitions();
				indiceAnotacion.addPeso("nombre", 5f);
				indiceAnotacion.addPeso("descripcion", 2f);

				mongoOperations
					.indexOps(Anotacion.class)
					.ensureIndex(indiceAnotacion);

			}
		};
	}

	
//	@Bean
//	public CommandLineRunner cargarTiposDeContenido(final TipoDeAnotacionDao tipoDeAnotacionDao) {
//		return new CommandLineRunner() {
//			
//			@Override
//			public void run(String... arg0) throws Exception {
//				
//			/*
//				
//				tipoDeAnotacionDao.save(new TipoDeAnotacion("anotacion.tipoReceta"));
//				tipoDeAnotacionDao.save(new TipoDeAnotacion("anotacion.tipoRestaurante"));
//				tipoDeAnotacionDao.save(new TipoDeAnotacion("anotacion.tipoLibro"));
//				tipoDeAnotacionDao.save(new TipoDeAnotacion("anotacion.tipoTecnica"));
//				tipoDeAnotacionDao.save(new TipoDeAnotacion("anotacion.tipoUtencilio"));
//				tipoDeAnotacionDao.save(new TipoDeAnotacion("anotacion.tipoProveedor"));
//				*/
//				
//				//tipoDeAnotacionDao.save(new TipoDeContenido("Foto de plato", "572b1ff4e8ba66004177e7f8"));
//				
//			}
//		};
//	}
//	

}
