package es.uniovi.cooksearch.vista;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import es.uniovi.cooksearch.modelo.Usuario;

public class RegistroForm {
	
	@NotBlank(message = "{usuario_necesario}")
	private String usuario;
	@NotBlank(message = "{email_necesario}")
	private String password;
	@NotBlank(message="{repetir_clave}")
	private String repPassword;
	@NotBlank
	@Email
	private String email;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRepPassword() {
		return repPassword;
	}
	public void setRepPassword(String repPassword) {
		this.repPassword = repPassword;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getUsuario() {
		return usuario;
	}
	
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public Usuario toUsuario() {
		return new Usuario(usuario,email, password);
	}
	
	

}
