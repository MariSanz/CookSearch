package es.uniovi.cooksearch.vista.anotaciones;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

import org.springframework.context.ApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import es.uniovi.cooksearch.modelo.Anotacion;
import es.uniovi.cooksearch.modelo.campos.Campo;
import es.uniovi.cooksearch.modelo.campos.TipoDeCampo;

public class AnotacionEditarForm extends AnotacionNuevaForm {
	
	public static class InfoCampo {
		
		private Campo campo;
		private TipoDeCampo tipoDeCampo;
		
		public InfoCampo(Campo campo, ApplicationContext context) {
			this.campo = campo;
			this.tipoDeCampo = (TipoDeCampo)context.getBean(campo.getTipo());
		}
		
		public Campo getCampo() {
			return campo;
		}
		
		public TipoDeCampo getTipoDeCampo() {
			return tipoDeCampo;
		}
	}
	
	private List<InfoCampo> campos = new ArrayList<>();
	private String [] idCampoFile;
	private String [] idCampoTexto;
	
	public AnotacionEditarForm() {
	}

	public AnotacionEditarForm(ApplicationContext context, Anotacion anotacion) {
		setNombre(anotacion.getNombre());
		setDescripcion(anotacion.getDescripcion());
		setPrivacidad(anotacion.isPrivacidad());
		setTipoDeAnotacion(anotacion.getTipoDeAnotacionId());

		for (Campo campo:anotacion.getMasDatos()) {
			campos.add(new InfoCampo(campo, context));
		}
		
		/*
		List<String> tiposDeCampoFile = new ArrayList<>();
		List<String> tiposDeCampoTexto = new ArrayList<>();
		List<String> etiquetasFile = new ArrayList<>();
		List<String> etiquetasTexto = new ArrayList<>();
		
//		List<String> camposTexto = new ArrayList<>();
		
		/*
		for (Campo campo: anotacion.getMasDatos()) {
			String nombreTipoDeCampo = campo.getTipo();
			TipoDeCampo tipoDeCampo = (TipoDeCampo)context.getBean(nombreTipoDeCampo);
			if (tipoDeCampo.getCampoForm().isSubirComoFichero()) {
				tiposDeCampoFile.add(nombreTipoDeCampo);
				etiquetasFile.add(campo.getEtiqueta());
			}
			else{
				tiposDeCampoTexto.add(nombreTipoDeCampo);
				etiquetasTexto.add(campo.getEtiqueta());
			}
		}
		
		setTipoCampoFile(tiposDeCampoFile.toArray(new String[0]));
		setTipoCampoTexto(tiposDeCampoTexto.toArray(new String[0]));
		setEtiquetaFile(etiquetasFile.toArray(new String[0]));
		setEtiquetaTexto(etiquetasTexto.toArray(new String[0]));
		*/
	}
	
	public List<InfoCampo> getCampos() {
		return campos;
	}
	
	public String[] getIdCampoFile() {
		return idCampoFile;
	}
	
	public void setIdCampoFile(String[] idCampoFichero) {
		this.idCampoFile = idCampoFichero;
	}
	
	public String[] getIdCampoTexto() {
		return idCampoTexto;
	}
	
	public void setIdCampoTexto(String[] idCampoTexto) {
		this.idCampoTexto = idCampoTexto;
	}
	
	public void reemplazarCampos(ApplicationContext context, Anotacion anotacion) {
		
		/*Actualizo campos de tipo fichero*/
		if (getCampoFile() != null && getEtiquetaFile() != null && getTipoCampoFile() != null && idCampoFile != null
				&& getCampoFile().length == getEtiquetaFile().length && getCampoFile().length == getTipoCampoFile().length
				&& getCampoFile().length == idCampoFile.length) {

			for (int i = 0; i < getCampoFile().length; i++) {
				MultipartFile fichero = getCampoFile()[i];
				String etiqueta = getEtiquetaFile()[i];
				String nombreTipoCampo = getTipoCampoFile()[i];

				Campo campoAnterior = anotacion.getCampo(idCampoFile[i]);
				TipoDeCampo tipoDeCampo = (TipoDeCampo) context.getBean(nombreTipoCampo);
				Campo campoNuevo = tipoDeCampo.getProcesador().procesar(etiqueta, fichero, campoAnterior);
				if (campoAnterior != campoNuevo) {
					anotacion.getMasDatos().add(campoNuevo);
				}
			}
		}
		
		/*Actualizo campos de tipo texto*/
		if (getCampoTexto() != null && getEtiquetaTexto()!=null && getTipoCampoTexto() != null 
				&& getCampoTexto().length == getEtiquetaTexto().length && getCampoTexto().length == getTipoCampoTexto().length) {
			
			for (int i = 0; i < getCampoTexto().length; i++) {
				String valor = getCampoTexto()[i];
				String etiqueta = getEtiquetaTexto()[i];
				String nombreTipoCampo = getTipoCampoTexto()[i];

				Campo campoAnterior = anotacion.getCampo(idCampoTexto[i]);
				TipoDeCampo tipoDeCampo = (TipoDeCampo) context.getBean(nombreTipoCampo);
				Campo campoNuevo = tipoDeCampo.getProcesador().procesar(etiqueta, valor, campoAnterior);
				if (campoAnterior != campoNuevo) {
					anotacion.getMasDatos().add(campoNuevo);
				}
			}
		}
			

		List<String> idsCampo = new ArrayList<>();
		if(idCampoFile!=null){
			idsCampo.addAll(Arrays.asList(idCampoFile));
		}
		if(idCampoTexto!=null){
			idsCampo.addAll(Arrays.asList(idCampoTexto));
		}
		
		ListIterator<Campo> itCampos = anotacion.getMasDatos().listIterator();
		while (itCampos.hasNext()) {
			Campo c = itCampos.next();
			if (!idsCampo.contains(c.getId())) {
				TipoDeCampo tipoDeCampo = (TipoDeCampo) context.getBean(c.getTipo());
				tipoDeCampo.borrar(c);
				itCampos.remove();
			}
		}
	}

}
