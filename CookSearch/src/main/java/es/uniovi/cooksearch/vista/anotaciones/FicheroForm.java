package es.uniovi.cooksearch.vista.anotaciones;

import java.io.InputStream;

public class FicheroForm {

	private InputStream inputStream;
	private String contentType;
	private String nombre;
	
	public FicheroForm(InputStream inputStream, String contentType, String nombre) {
		this.inputStream = inputStream;
		this.contentType = contentType;
		this.nombre = nombre;
	}

	public String getContentType() {
		return contentType;
	}
	
	public InputStream getInputStream() {
		return inputStream;
	}
	
	public String getNombre() {
		return nombre;
	}
	
}
