package es.uniovi.cooksearch.vista.anotaciones;

import java.io.IOException;

import com.github.slugify.Slugify;

import es.uniovi.cooksearch.modelo.tipo.TipoDeAnotacion;

public class AnotacionListadoForm {

	private String anotacionId;
	private String nombre;
	private String descripcion;
	private TipoDeAnotacion tipoDeAnotacion;
	private String usuarioNombre;
	private int puntuacion;
	private boolean privacidad;
	private String predefinido;
	private float ajusteBusqueda;
	private Boolean propia;
	private String indexador;
	private Slugify slg;

	public AnotacionListadoForm() {
		try {
			slg = new Slugify();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public String getAnotacionId() {
		return anotacionId;
	}

	public void setAnotacionId(String anotacionId) {
		this.anotacionId = anotacionId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public TipoDeAnotacion getTipoDeAnotacion() {
		return tipoDeAnotacion;
	}

	public String getUsuarioNombre() {
		return usuarioNombre;
	}

	public void setUsuarioNombre(String usuarioNombre) {
		this.usuarioNombre = usuarioNombre;
	}

	public boolean isPrivacidad() {
		return privacidad;
	}

	public void setPrivacidad(boolean privacidad) {
		this.privacidad = privacidad;
	}

	public void setPuntuacion(int puntuacion) {
		this.puntuacion = puntuacion;
	}

	public int getPuntuacion() {
		return puntuacion;
	}

	public String getPredefinido() {
		return predefinido;
	}
	
	public float getAjusteBusqueda() {
		return ajusteBusqueda;
	}
	
	public void setAjusteBusqueda(float ajusteBusqueda) {
		this.ajusteBusqueda = ajusteBusqueda;
	}

	public void setTipoDeAnotacion(TipoDeAnotacion tipoDeAnotacion) {
		this.tipoDeAnotacion = tipoDeAnotacion;
	}
	
	public Boolean getPropia() {
		return propia;
	}
	
	public void setPropia(Boolean propia) {
		this.propia = propia;
	}
	
	public String getIndexador() {
		this.indexador = slg.slugify(nombre);
		return indexador;
	}

}
