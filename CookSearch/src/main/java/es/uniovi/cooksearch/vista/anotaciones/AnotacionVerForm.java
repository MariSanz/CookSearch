package es.uniovi.cooksearch.vista.anotaciones;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.github.slugify.Slugify;

import es.uniovi.cooksearch.modelo.Comentario;
import es.uniovi.cooksearch.modelo.campos.Campo;
import es.uniovi.cooksearch.modelo.tipo.TipoDeAnotacion;

public class AnotacionVerForm {

	private String nombre;
	private String descripcion;
	private String tipoAnotacionNombre;
	private TipoDeAnotacion tipoDeAnotacion;
	private String usuario;// nombre del usuario dueño de la anotación
	private String anotacionId;
	private String nuevoComentario;
	private Boolean propia;
	private Boolean yaVoto;
	private Boolean yaComento;
	private Integer puntuacion;
	private Integer votos;
	private Integer nuevaPuntuacion;
	private Integer numComentarios;
	private Integer numVistas;
	private String 	comentarioAprobadoId;
	private String fechaPublicacion;
	private String indexador;
	private Slugify slg;

	private List<Comentario> comentarios = new ArrayList<>();

	private List<Campo> campos = new ArrayList<>();
	
	public AnotacionVerForm() {
		try {
			slg = new Slugify();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getAnotacionId() {
		return anotacionId;
	}

	public void setAnotacionId(String anotacionId) {
		this.anotacionId = anotacionId;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getTipoAnotacionNombre() {
		return tipoAnotacionNombre;
	}

	public void setTipoAnotacionNombre(String tipoAnotacionNombre) {
		this.tipoAnotacionNombre = tipoAnotacionNombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Campo> getCampos() {
		return campos;
	}

	public List<Comentario> getComentarios() {
		return comentarios;
	}

	public void setNuevoComentario(String nuevoComentario) {
		this.nuevoComentario = nuevoComentario;
	}

	public String getNuevoComentario() {
		return nuevoComentario;
	}

	public void setPropia(Boolean propia) {
		this.propia = propia;
	}

	public Boolean getPropia() {
		return propia;
	}

	public Integer getPuntuacion() {
		return puntuacion;
	}

	public void setPuntuacion(Integer puntuacion) {
		this.puntuacion = puntuacion;
	}

	public void setNuevaPuntuacion(Integer nuevaPuntuacion) {
		this.nuevaPuntuacion = nuevaPuntuacion;
	}

	public Integer getNuevaPuntuacion() {
		return nuevaPuntuacion;
	}

	public void setYaVoto(Boolean yaVoto) {
		this.yaVoto = yaVoto;
	}

	public Boolean getYaVoto() {
		return yaVoto;
	}

	public Boolean getYaComento() {
		return yaComento;
	}

	public void setYaComento(Boolean yaComento) {
		this.yaComento = yaComento;
	}

	public Integer getNumComentarios() {
		this.numComentarios = comentarios.size();
		return numComentarios;
	}

	public void setTipoDeAnotacion(TipoDeAnotacion tipoDeAnotacion) {
		this.tipoDeAnotacion = tipoDeAnotacion;
	}

	public TipoDeAnotacion getTipoDeAnotacion() {
		return tipoDeAnotacion;
	}
	
	public String getComentarioAprobadoId() {
		return comentarioAprobadoId;
	}
	
	public void setComentarioAprobadoId(String comentarioAprobadoId) {
		this.comentarioAprobadoId = comentarioAprobadoId;
	}
	
	public Integer getVotos() {
		return votos;
	}
	
	public void setVotos(Integer votos) {
		this.votos = votos;
	}
	
	public Integer getNumVistas() {
		return numVistas;
	}
	
	public void setNumVistas(Integer numVistas) {
		this.numVistas = numVistas;
	}
	
	public String getFechaPublicacion() {
		return fechaPublicacion;
	}
	
	public void fechaPublicacion(Date fecha){
		DateFormat formato = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
		this.fechaPublicacion = formato.format(fecha);
	}
	
	public String getIndexador() {
		this.indexador = slg.slugify(nombre);
		return indexador;
	}

}
