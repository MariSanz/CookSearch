package es.uniovi.cooksearch.vista;

import org.hibernate.validator.constraints.NotBlank;

public class LoginForm {

	@NotBlank
	private String user;
	@NotBlank
	private String password;
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
