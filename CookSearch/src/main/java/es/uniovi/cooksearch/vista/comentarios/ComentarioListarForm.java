package es.uniovi.cooksearch.vista.comentarios;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.github.slugify.Slugify;

/**
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public class ComentarioListarForm {

	private String usuario;
	private String anotacionNombre;
	private String anotacionId;
	private String contenido;
	private String fecha;
	private Boolean aprobado;
	private String id;
	private String indexador;
	private Slugify slg;

	public ComentarioListarForm(String usuario, String anotacionNombre, String anotacionId, String contenido,
			Date fecha, String id, Boolean aprobado) {
		this.usuario = usuario;
		this.anotacionNombre = anotacionNombre;
		this.anotacionId = anotacionId;
		this.contenido = contenido;
		this.aprobado = aprobado;
		DateFormat formato = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
		String convertido = formato.format(fecha);
		this.fecha = convertido;
		this.id = id;
		try {
			slg = new Slugify();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ComentarioListarForm() {

	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getAnotacionNombre() {
		return anotacionNombre;
	}

	public void setAnotacionNombre(String anotacionNombre) {
		this.anotacionNombre = anotacionNombre;
	}

	public String getAnotacionId() {
		return anotacionId;
	}

	public void setAnotacionId(String anotacionId) {
		this.anotacionId = anotacionId;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public String getFecha() {
		return fecha;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getAprobado() {
		return aprobado;
	}

	public void setAprobado(Boolean aprobado) {
		this.aprobado = aprobado;
	}
	
	public String getIndexador() {
		this.indexador = slg.slugify(anotacionNombre);
		return indexador;
	}

}
