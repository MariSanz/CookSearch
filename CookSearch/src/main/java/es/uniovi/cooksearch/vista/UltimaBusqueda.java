package es.uniovi.cooksearch.vista;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import es.uniovi.cooksearch.vista.anotaciones.AnotacionBuscarForm;

public class UltimaBusqueda {

	private boolean simple;

	private String cadena;
	private int puntuacion;

	private AnotacionBuscarForm form;

	private String tipoDeAnotacion;

	private Date fechaDesde;

	private Date fechaHasta;

	private boolean publica;

	private boolean privada;

	private UltimaBusqueda() {
	}

	public static UltimaBusqueda simple(String cadena) {
		UltimaBusqueda ub = new UltimaBusqueda();
		ub.simple = true;
		ub.cadena = cadena;
		return ub;
	}

	public static UltimaBusqueda avanzada(AnotacionBuscarForm anotacionBuscarForm) {
		UltimaBusqueda ub = new UltimaBusqueda();
		ub.simple = false;
		ub.cadena = anotacionBuscarForm.getTextoBuscar();
		ub.puntuacion = anotacionBuscarForm.getPuntuacion();
		ub.tipoDeAnotacion = anotacionBuscarForm.getTipoDeAnotacion();
		ub.fechaDesde= anotacionBuscarForm.getFechaDesde();
		ub.fechaHasta= anotacionBuscarForm.getFechaHasta();
		ub.publica = anotacionBuscarForm.isPublica();
		ub.privada = anotacionBuscarForm.isPrivada();

		return ub;
	}

	public String toUrl() {
		if (simple) {
			return "/listado-simple?textoBuscar=" + cadena;
		} else {
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			try {
				String urlCadena = cadena == null ? "" : URLEncoder.encode(cadena, "UTF-8");
				String urlPuntuacion = Integer.toString(puntuacion);
				String urlTipoDeAnotacion = tipoDeAnotacion == null ? "" : URLEncoder.encode(tipoDeAnotacion, "UTF-8");
				String urlFechaDesde = fechaDesde == null ? "" : df.format(fechaDesde);
				String urlFechaHasta = fechaHasta == null ? "" : df.format(fechaHasta);
				String urlPublica = publica ? "on" : "off";
				String urlPrivada = privada ? "on" : "off";
				
				return "/anotaciones/listado_avanzado?textoBuscar=" + urlCadena + "&puntuacion=" + urlPuntuacion
						+ "&tipoDeAnotacion=" + urlTipoDeAnotacion + "&fechaDesde=" + urlFechaDesde
						+ "&fechaHasta=" + urlFechaHasta + "&_publica=" + urlPublica + "&_privada="
						+ urlPrivada;
				

			} catch (UnsupportedEncodingException e) {
				throw new RuntimeException(e);
			}

		}
	}

}
