package es.uniovi.cooksearch.vista.anotaciones;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class AnotacionBuscarForm {

	//@NotBlank(message = "{texto_necesario}")
	private String textoBuscar;
	private int puntuacion;
	private boolean publica;
	private boolean privada;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaDesde;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fechaHasta;
	private String tipoDeAnotacion;

	public String getTextoBuscar() {
		return textoBuscar;
	}

	public void setTextoBuscar(String textoBuscar) {
		this.textoBuscar = textoBuscar;
	}

	public int getPuntuacion() {
		return puntuacion;
	}

	public void setPuntuacion(int puntuacion) {
		this.puntuacion = puntuacion;
	}

	public boolean isPublica() {
		return publica;
	}

	public void setPublica(boolean publica) {
		this.publica = publica;
	}

	public boolean isPrivada() {
		return privada;
	}

	public void setPrivada(boolean privada) {
		this.privada = privada;
	}

	public Date getFechaDesde() {
		return fechaDesde;
	}

	public void setFechaDesde(Date fechaDesde) {
		this.fechaDesde = fechaDesde;
	}

	public Date getFechaHasta() {
		return fechaHasta;
	}

	public void setFechaHasta(Date fechaHasta) {
		this.fechaHasta = fechaHasta;
	}

	public String getTipoDeAnotacion() {
		return tipoDeAnotacion;
	}
	
	public void setTipoDeAnotacion(String tipoDeAnotacion) {
		this.tipoDeAnotacion = tipoDeAnotacion;
	}

}
