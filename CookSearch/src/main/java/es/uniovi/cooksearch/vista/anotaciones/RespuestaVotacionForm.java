package es.uniovi.cooksearch.vista.anotaciones;

public class RespuestaVotacionForm {

	private int votos;
	private int puntuacion;
	
	public RespuestaVotacionForm(int votos, int puntuacion) {
		this.votos = votos;
		this.puntuacion = puntuacion;
	}
	
	public int getVotos() {
		return votos;
	}
	
	public int getPuntuacion() {
		return puntuacion;
	}
}
