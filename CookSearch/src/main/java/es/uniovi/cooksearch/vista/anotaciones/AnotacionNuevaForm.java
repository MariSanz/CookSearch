package es.uniovi.cooksearch.vista.anotaciones;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.context.ApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import es.uniovi.cooksearch.modelo.Anotacion;
import es.uniovi.cooksearch.modelo.campos.Campo;
import es.uniovi.cooksearch.modelo.campos.TipoDeCampo;
import es.uniovi.cooksearch.modelo.tipo.TipoDeAnotacion;

public class AnotacionNuevaForm {

	@NotBlank(message = "{nombre_necesario}")
	private String nombre;
	@NotBlank(message = "{descripcion_necesario}")
	private String descripcion;
	private String usuarioId;
	private String tipoDeAnotacion;
	private String tipoDeContenidoOtro;
	private String[] etiquetaFile;
	private String[] tipoCampoFile;
	private MultipartFile[] campoFile;
	private String[] etiquetaTexto;
	private String[] campoTexto;
	private String[] tipoCampoTexto;
	private Boolean privacidad;
	
	/*Utilidades*/
	private String tipoOtro;
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTipoDeAnotacion() {
		return tipoDeAnotacion;
	}

	public void setTipoDeAnotacion(String tipoDeAnotacion) {
		this.tipoDeAnotacion = tipoDeAnotacion;
	}

	public boolean isIdTipoDeAnotacionIndicado() {
		return tipoDeAnotacion != null && tipoDeContenidoOtro.isEmpty();
	}

	public String getTipoDeContenidoOtro() {
		return tipoDeContenidoOtro;
	}

	public void setTipoDeContenidoOtro(String tipoDeContenidoOtro) {
		this.tipoDeContenidoOtro = tipoDeContenidoOtro;
	}

	public Boolean getPrivacidad() {
		return privacidad;
	}

	public void setPrivacidad(Boolean privacidad) {
		this.privacidad = privacidad;
	}

	public String[] getEtiquetaFile() {
		return etiquetaFile;
	}

	public void setEtiquetaFile(String[] etiquetaFile) {
		this.etiquetaFile = etiquetaFile;
	}

	public String[] getTipoCampoFile() {
		return tipoCampoFile;
	}

	public void setTipoCampoFile(String[] tipoCampoFile) {
		this.tipoCampoFile = tipoCampoFile;
	}

	public MultipartFile[] getCampoFile() {
		return campoFile;
	}

	public void setCampoFile(MultipartFile[] campoFile) {
		this.campoFile = campoFile;
	}

	public String[] getEtiquetaTexto() {
		return etiquetaTexto;
	}

	public void setEtiquetaTexto(String[] etiquetaTexto) {
		this.etiquetaTexto = etiquetaTexto;
	}

	public String[] getCampoTexto() {
		return campoTexto;
	}

	public void setCampoTexto(String[] campoTexto) {
		this.campoTexto = campoTexto;
	}

	public String getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}
	
	public void setTipoOtro(String tipoOtro) {
		this.tipoOtro = tipoOtro;
	}
	
	public String getTipoOtro() {
		return tipoOtro;
	}
	
	public String[] getTipoCampoTexto() {
		return tipoCampoTexto;
	}
	
	public void setTipoCampoTexto(String[] tipoCampoTexto) {
		this.tipoCampoTexto = tipoCampoTexto;
	}

	public List<Campo> getCampos(ApplicationContext contexto) {

		List<Campo> campos = new ArrayList<>();

		if (campoFile != null && etiquetaFile != null && tipoCampoFile != null
				&& campoFile.length == etiquetaFile.length && campoFile.length == tipoCampoFile.length) {

			for (int i = 0; i < campoFile.length; i++) {
				MultipartFile fichero = campoFile[i];
				String etiqueta = etiquetaFile[i];
				String nombreTipoCampo = tipoCampoFile[i];

				TipoDeCampo tipoDeCampo = (TipoDeCampo) contexto.getBean(nombreTipoCampo);
				campos.add(tipoDeCampo.getProcesador().procesar(etiqueta, fichero, null));
			}
		}

		if (campoTexto != null && etiquetaTexto!=null && tipoCampoTexto != null 
				&& campoTexto.length == etiquetaTexto.length && campoTexto.length == tipoCampoTexto.length) {
			
		
			for (int j = 0; j < campoTexto.length; j++) {
				String contenido = campoTexto[j];
				String etiqueta = etiquetaTexto[j];
				String nombreTipoCampo = tipoCampoTexto[j];

				TipoDeCampo tipoDeCampo = (TipoDeCampo) contexto.getBean(nombreTipoCampo);
				campos.add(tipoDeCampo.getProcesador().procesar(etiqueta, contenido, null));
			}
		}

		return campos;
	}

	public Anotacion getAnotacion(ApplicationContext contexto) {

		List<Campo> campos = getCampos(contexto);
		
		String tipoDeAnotacionId = tipoDeAnotacion.trim().length() == 0 ? null : tipoDeAnotacion;

		return new Anotacion(nombre, descripcion, privacidad, tipoDeAnotacionId, campos);

	}

	public TipoDeAnotacion getNuevoTipoDeAnotacion() {
		return new TipoDeAnotacion(getTipoDeContenidoOtro(), getUsuarioId());
	}

}
