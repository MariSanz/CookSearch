package es.uniovi.cooksearch.vista.anotaciones;

public class VotoForm {

	private String idAnotacion;
	private int puntuacion;
	
	public String getIdAnotacion() {
		return idAnotacion;
	}
	public void setIdAnotacion(String idAnotacion) {
		this.idAnotacion = idAnotacion;
	}
	public int getPuntuacion() {
		return puntuacion;
	}
	public void setPuntuacion(int puntuacion) {
		this.puntuacion = puntuacion;
	}
	
	
}
