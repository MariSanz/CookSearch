package es.uniovi.cooksearch.modelo.campos.tipos;

import org.springframework.data.annotation.Id;

import es.uniovi.cooksearch.modelo.campos.Campo;
import es.uniovi.cooksearch.modelo.campos.CampoTexto;
import es.uniovi.cooksearch.modelo.campos.Procesador;
import es.uniovi.cooksearch.modelo.campos.TipoDeCampo;
import es.uniovi.cooksearch.modelo.campos.procesadores.ProcesarTexto;

/**
 * Clase que indica el tipo de un campo del atributo masDatos del documento
 * Anotacion tipo del campo texto largo
 * 
 * @see CampoTexto
 * @author María José Sánchez Doria
 * @version 1.0
 */
public class TipoDeCampoTextoLargo implements TipoDeCampo {

	@Id
	private String id;

	/*
	 * Contenido en texto del campo
	 */
	private String contenido;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CampoForm getCampoForm() {

		return CampoForm.TEXTO_LARGO;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Procesador getProcesador() {
		return new ProcesarTexto(true);
	}

	/**
	 * Método que retorna el contenido del campo
	 * 
	 * @return {@link String}
	 */
	public String getContenido() {
		return contenido;
	}

	/**
	 * Método para cambiar el contenido del campo
	 * 
	 * @param contenido
	 *            {@link String}
	 */
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	@Override
	public void borrar(Campo campo) {
	}
	
	@Override
	public String getNombre() {
		return "Texto largo";
	}
}
