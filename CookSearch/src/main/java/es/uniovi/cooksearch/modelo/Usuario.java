package es.uniovi.cooksearch.modelo;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.data.annotation.Id;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Clase que representa la entidad usuario de la aplicación, con el nombre de
 * usuario y la clase si se requiere para guardarla encriptada o para consultar
 * la base de datos
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public class Usuario implements UserDetails {

	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private String usuario;
	private String email;
	private String password;

	public Usuario(String usuario, String email, String password) {
		super();
		this.usuario = usuario;
		this.password = password;
		this.email = email;
	}

	public String getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public String getPassword() {
		return password;
	}

	public String getUsuario() {
		return usuario;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
	
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_USUARIO"));
	}

	@Override
	public String getUsername() {
		return usuario;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
