package es.uniovi.cooksearch.modelo.campos.tipos;

import es.uniovi.cooksearch.modelo.campos.Campo;
import es.uniovi.cooksearch.modelo.campos.CampoTexto;
import es.uniovi.cooksearch.modelo.campos.Procesador;
import es.uniovi.cooksearch.modelo.campos.TipoDeCampo;
import es.uniovi.cooksearch.modelo.campos.procesadores.ProcesarTexto;

/**
 * Clase que indica el tipo de un campo del atributo masDatos del documento
 * Anotacion tipo del campo texto corto
 * 
 * @see CampoTexto
 * @author María José Sánchez Doria
 * @version 1.0
 */
public class TipoDeCampoTextoCorto implements TipoDeCampo {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CampoForm getCampoForm() {

		return CampoForm.TEXTO;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Procesador getProcesador() {
		return new ProcesarTexto(false);
	}
	
	@Override
	public void borrar(Campo campo) {
	}

	@Override
	public String getNombre() {
		return "Texto corto";
	}
}
