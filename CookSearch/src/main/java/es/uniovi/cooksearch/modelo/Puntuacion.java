package es.uniovi.cooksearch.modelo;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase que modela la puntuación de las anotaciones. La votación se calcula
 * segun la media del numero de votos que tiene la anotación y para que los
 * usuarios solo puedan votar una vez se guarda una lista con los nombres de los
 * usuarios que votaron
 * 
 * @see Anotacion
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public class Puntuacion {

	/* Media de la anotación, numero entre 0 y 5 */
	private int puntuacionAcumulada;
	/* Número de votos que contabilizados */
	private int votos;
	private int puntuacion;
	/*
	 * Lista de nombres de usuarios (clave única) que han votado en la anotación
	 */
	private List<String> usuarios;

	/**
	 * Constructor del objeto Puntuación. La puntuacion acumulada inicial es 0,
	 * el numero de votos es 0, y la lista de usuarios votados está vacia.
	 */
	public Puntuacion() {
		this.puntuacionAcumulada = 0;
		this.votos = 0;
		this.puntuacion = 0;
		this.usuarios = new ArrayList<>();
	}

	/**
	 * Método que retorna la media de puntuación de la anotación
	 * 
	 * @return
	 */
	public int getPuntuacion() {
		return puntuacion;
	}

	/**
	 * Método que realiza el proceso de votar en la anotación. Se calcula la
	 * nueva media aumentando el numero de votos y con la nueva votación
	 * obtenida.
	 * 
	 * @param puntuacion
	 *            Puntuacion obtenida por el usuario
	 * @param usuario
	 *            Usuario que vota, nombre del usuario
	 * @return true si se voto (el usuario no ha votado antes) y false si ya el
	 *         usaurio ha votado antes
	 */
	public boolean votar(int puntuacion, String usuario) {
		if (usuarios.indexOf(usuario) == -1) { // si el usuario ya ha votado no
												// permite nuevos votos
			this.puntuacionAcumulada += puntuacion;
			this.votos += 1;
			this.usuarios.add(usuario);
			
			this.puntuacion = puntuacionAcumulada / votos;
			
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Método para obtender el numero de votos actuales de la anotación
	 * @return
	 */
	public int getVotos() {
		return votos;
	}

	/**
	 * Método para obtener la puntuación acumulada, guardada actualmente
	 * @return puntuación media de la anotacion
	 */
	public int getPuntuacionAcumulada() {
		return puntuacionAcumulada;
	}

	/**
	 * Lista del nombre de todos los usuarios que han votado en la anotación
	 * @return {@link List} nombre de los usuario
	 */
	public List<String> getUsuarios() {
		return usuarios;
	}

}
