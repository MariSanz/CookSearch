package es.uniovi.cooksearch.modelo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.TextScore;

import es.uniovi.cooksearch.modelo.campos.Campo;
import es.uniovi.cooksearch.modelo.tipo.TipoDeAnotacion;

/**
 * Clase principal del modelo, representa las notas, anotaciones, publicaciones
 * que se gestionan en la aplicación, tiene una estructura que busca ser
 * flexible y simula dinamismo (personalización de anotaciones)
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Document
public class Anotacion {

	@Id
	private String id;

	private String nombre;
	private String descripcion;
	private boolean privacidad;
	private List<Comentario> comentarios;
	private Puntuacion puntuacion;
	private String usuarioId;
	private int contadorVistas;
	private List<Campo> masDatos;
	private String tipoDeAnotacionId;
	private Date fechaPublicacion;

	/*
	 * Atributo que representa el peso que tiene la anotación en el resultado de
	 * la consulta realizada por el usuario
	 */
	@TextScore
	private Float ajusteBusqueda;

	/**
	 * Constructor de la clase Anotacion, representa documentos de una colección
	 * del mismo nombre en la base de datos.
	 * 
	 * @param nombre
	 *            {@link String} Nombre de la anotación
	 * @param descripcion
	 *            {@link String} Descripción de la anotación
	 * @param privacidad
	 *            {@link Boolean} true si la privacidad es publica, false si la
	 *            privacidad es privada
	 * 
	 * @param tipoDeAnotacionId
	 *            {@link String} id del {@link TipoDeAnotacion} que tiene la
	 *            anotación
	 * @param masDatos
	 *            Lista de los campos personalizados del usuario en la anotación
	 */
	public Anotacion(String nombre, String descripcion, boolean privacidad, String tipoDeAnotacionId,
			List<Campo> masDatos) {

		this.nombre = nombre;
		this.descripcion = descripcion;
		this.privacidad = privacidad;
		this.comentarios = new ArrayList<>();
		this.puntuacion = new Puntuacion();
		this.usuarioId = "";
		this.contadorVistas = 0;
		this.masDatos = masDatos;
		this.tipoDeAnotacionId = tipoDeAnotacionId;
		//DateFormat fecha = new SimpleDateFormat("yyyy/MM/dd");
		//String convertido = fecha.format(new Date());
		this.fechaPublicacion = new Date();
	}

	/**
	 * Método que devuelve el nombre de la anotación
	 * 
	 * @return {@link String}
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Método para cambiar el nombre de la anotacion
	 * 
	 * @param nombre
	 *            {@link String}
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Método que devuelve la lista de comentarios de la anotación
	 * 
	 * @return {@link List} {@link Comentario}
	 */
	public List<Comentario> getComentarios() {
		return comentarios;
	}

	/**
	 * Método que devuelve el número de puntuación que tiene la anotación
	 * 
	 * @return {@link Integer} del 1 al 5, 1 mínima y 5 máxima
	 */
	public Puntuacion getPuntuacion() {
		return puntuacion;
	}

	/**
	 * Método que devuelve el ID del usuario en la base de datos dueño de la
	 * anotación
	 * 
	 * @return {@link String} ID del usuario
	 */
	public String getUsuarioId() {
		return usuarioId;
	}

	/**
	 * Método para cambiar el ID del usuario en la base de datos dueño de la
	 * anotación
	 * 
	 * @param usuarioId
	 *            {@link String}
	 */
	public void setUsuarioId(String usuarioId) {
		this.usuarioId = usuarioId;
	}

	/**
	 * Método de retorno de la lista de campos extras de la anotación, que se
	 * personalizó el usuario
	 * 
	 * @return {@link List} {@link Campo} Puede ser tipo Fichero, Texto y URL
	 */
	public List<Campo> getMasDatos() {
		return masDatos;
	}

	/**
	 * Método que devuelve el contador de vistas de la anotación, el número de
	 * veces que un usuario la visita en la interfaz
	 * 
	 * @return {@link Integer}
	 */
	public Integer getContadorVistas() {
		return contadorVistas;
	}

	/**
	 * Método que retorna el ID del tipo de anotación asignado por el usuario
	 * 
	 * @return {@link String}
	 */
	public String getTipoDeAnotacionId() {
		return tipoDeAnotacionId;
	}

	/**
	 * Método para cambiar el ID del tipo de anotación asignado por el usuario
	 * 
	 * @param tipoDeAnotacionId
	 */
	public void setTipoDeAnotacionId(String tipoDeAnotacionId) {
		this.tipoDeAnotacionId = tipoDeAnotacionId;
	}

	/**
	 * Método que retorna la fecha de publicación
	 * 
	 * @return {@link String} formato yyyy/mm/dd
	 */
	public Date getFechaPublicacion() {
		return fechaPublicacion;
	}

	/**
	 * Método que retorna la descripción de la anotación
	 * 
	 * @return {@link String}
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Método para cambiar la descripción de la anotación
	 * 
	 * @param descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Método que retorna el ID de la noatación en la base de datos
	 * 
	 * @return {@link String} ID en la base de datos
	 */
	public String getId() {
		return id;
	}

	/**
	 * Método para cambiar el ID de la noatación en la base de datos
	 * 
	 * @return {@link String} ID en la base de datos
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Método que devuelve si una anotación es publica o privada
	 * 
	 * @return {@link Boolean} true si la privacidad es publica, false si la
	 *         privacidad es privada
	 * 
	 */
	public boolean isPrivacidad() {
		return privacidad;
	}

	/**
	 * Método para cambiar la privacidad de la anotación
	 * 
	 * @param privacidad
	 *            {@link Boolean} true si la privacidad es pública, false si la
	 *            privacidad es privada
	 * 
	 */
	public void setPrivacidad(boolean privacidad) {
		this.privacidad = privacidad;
	}

	/**
	 * Método para cambiar la lista de los comentarios de la anotacion
	 * 
	 * @param comentarios
	 *            {@link List} {@link Comentario}
	 */
	public void setComentarios(List<Comentario> comentarios) {
		this.comentarios = comentarios;
	}

	/**
	 * Método para cambiar la puntuación de la anotación
	 * 
	 * @param puntuacion
	 *            {@link Integer} 1 como mínimo y 5 como máximo
	 */
	public void setPuntuacion(Puntuacion puntuacion) {
		this.puntuacion = puntuacion;
	}

	/**
	 * Método para devolver el contador de vistas de la anotación, el número de
	 * veces que un usuario la visita en la interfaz
	 * 
	 * @return {@link Integer}
	 */
	public void setContadorVistas(int contadorVistas) {
		this.contadorVistas = contadorVistas;
	}

	/**
	 * Método de retorno de la lista de campos extras de la anotación, que se
	 * personalizó el usuario
	 * 
	 * @return {@link List} {@link Campo} Puede ser tipo Fichero, Texto y URL
	 */
	public void setMasDatos(List<Campo> masDatos) {
		this.masDatos = masDatos;
	}

	/**
	 * Método para cambiar la fecha de la publicación
	 * 
	 * @param fechaPublicacion
	 *            {@link String} formato yyyy/mm/dd
	 */
	public void setFechaPublicacion(Date fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	/**
	 * Método para devolver el ajuste de búsqueda que le da la base de datos a
	 * esta anotación en los resultados de una búsqueda concreta
	 * 
	 * @return {@link Float}
	 */
	public Float getAjusteBusqueda() {
		return ajusteBusqueda;
	}

	/**
	 * Método para obtener un campo concreto de la anotación segun el ID de ese
	 * campo
	 * 
	 * @param id
	 *            identificador del campo a buscar
	 * @return {@link Campo} campo resultado de la busqueda
	 */
	public Campo getCampo(String id) {
		for (Campo campo : masDatos) {
			if (campo.getId().equals(id)) {
				return campo;
			}
		}
		return null;
	}

}
