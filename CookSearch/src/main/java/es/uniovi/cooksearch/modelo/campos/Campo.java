package es.uniovi.cooksearch.modelo.campos;

import es.uniovi.cooksearch.modelo.Anotacion;

/**
 * Interfaz que define los metodos que necesitamos de los campos a guardar en la
 * anotacion. Cada objeto de tipo Campo representa el atributo masDatos de los
 * documentos Anotacion.
 * 
 * @see Anotacion
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public interface Campo {

	String getId();
	
	/**
	 * Método que retorna el nombre del campo procesado
	 * 
	 * @return {@link String}
	 */
	String getEtiqueta();

	/**
	 * Método que retorna el tipo con el que representar el campo. Existe
	 * representadores: texto, fichero y url
	 * 
	 * @return
	 */
	String getPresentador();
	
	/**
	 * Método para conocer el tipo concreto del campo
	 * @return
	 */
	String getTipo();
		
}
