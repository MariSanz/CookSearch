package es.uniovi.cooksearch.modelo.campos;

import java.util.UUID;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Clase que representa el campo de tipo fichero de las anotaciones
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Document
public class CampoFichero implements Campo {

	private String id;

	/*
	 * Nombre del campo
	 */
	private String etiqueta;
	/*
	 * Path del fichero, al servidor
	 */
	private String fichero;
	/*
	 * Contenido de texto en el fichero descargado con OCR
	 */
	private String textoOcr;

	/*
	 * Valor del tipo del fichero almacenado (imagen, pdf)
	 */
	private String contentType;

	private String nombreRealFichero;

	public CampoFichero(String etiqueta, String fichero, String textoOcr, String contentType,
			String nombreRealFichero) {
		this.id = UUID.randomUUID().toString();
		this.etiqueta = etiqueta;
		this.fichero = fichero;
		this.textoOcr = textoOcr;
		this.contentType = contentType;
		this.nombreRealFichero = nombreRealFichero;
	}

	public String getId() {
		return id;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEtiqueta() {
		return etiqueta;
	}

	/**
	 * Método que retorna el path del fichero que se guarda en el servidor
	 * 
	 * @return {@link String}
	 */
	public String getFichero() {
		return fichero;
	}

	/**
	 * Método que retorna el texto del contenido detectado por el OCR en el
	 * fichero
	 * 
	 * @return
	 */
	public String getTextoOcr() {
		return textoOcr;
	}

	/*
	 * 
	 * (non-Javadoc)
	 * 
	 * @see es.uniovi.cooksearch.modelo.campos.Campo#getPresentador()
	 */
	@Override
	public String getPresentador() {

		return "fichero";
	}

	/**
	 * Método para obtener el tipo del fichero a guardar, es importante para su
	 * representación en la interfaz vista
	 * 
	 * @return {@link String}
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * Método que retorna el nombre que tenia el fichero antes de ser procesado
	 * por el sistema
	 * 
	 * @return
	 */
	public String getNombreRealFichero() {
		return nombreRealFichero;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getTipo() {
		return "tipo-de-campo-file";
	}

	/**
	 * Los campos de fichero pueden ser editados, cambiando el nombre de la
	 * etiqueta o del contenido del fichero manteniendo el nombre de la
	 * etiqueta, este metodo realiza el proceso
	 * 
	 * @param etiqueta
	 *            nombre del campo
	 * @param textoOcr
	 *            texto textual reconocido en el fichero por el OCR
	 * @param contentType
	 *            tipo de contenido del fichero
	 * @param nombreRealFichero
	 *            nombre real del fichero que el cliente guardo
	 */
	public void actualizar(String etiqueta, String textoOcr, String contentType, String nombreRealFichero) {
		this.etiqueta = etiqueta;
		this.textoOcr = textoOcr;
		this.contentType = contentType;
		this.nombreRealFichero = nombreRealFichero;
	}

}
