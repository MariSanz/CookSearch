package es.uniovi.cooksearch.modelo.campos;

import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Clase que representa el campo de tipo texto de las anotaciones
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Document
public class CampoTexto implements Campo {

	@Id
	private String id;

	private String etiqueta;
	private String contenido;
	private boolean largo;

	/**
	 * Constructor del campo de tipo texto del atributo masDatos de los
	 * documentos Anotacion
	 * 
	 * @param etiqueta
	 *            {@link String} Nombre del campo
	 * @param contenido
	 *            {@link String} Contenido texto del campo
	 * @param largo true si es largo el contenido del campo y false si es corto
	 */
	public CampoTexto(String etiqueta, String contenido, boolean largo) {
		this.id = UUID.randomUUID().toString();
		this.etiqueta = etiqueta;
		this.contenido = contenido;
		this.largo = largo;
	}

	public String getId() {
		return id;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEtiqueta() {

		return etiqueta;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getPresentador() {
		return "texto";
	}

	/**
	 * Método que devuelve el contenido de tipo texto del campo
	 * 
	 * @return {@link String} Contenido del campo
	 */
	public String getContenido() {
		return contenido;
	}

	/**
	 * Método para cambiar el contenido del campo
	 * 
	 * @param contenido contenido textual del campo
	 */
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	
	/**
	 * Método que retorna el tipo concreto del campo, este retorna "tipo-de-campo-texto-largo"
	 * si es laro y "tipo-de-campo-texto-corto" si es corto 
	 */
	@Override
	public String getTipo() {
		if (largo) {
			return "tipo-de-campo-texto-largo";
		}
		else {
			return "tipo-de-campo-texto-corto";
		}
	}
	
	/**
	 * Es campo de texto corto retorna false, si es largo retorna true
	 * @return true si es largo el contenido del campo, false en caso contrario
	 */
	public boolean isLargo() {
		return largo;
	}
	
	public void setLargo(boolean largo) {
		this.largo = largo;
	}

	/**
	 * Los campos de texto pueden ser editados, cambiando el nombre de la etiqueta
	 * o del contenido del campo manteniendo el nombre de la etiqueta, este metodo
	 * realiza el proceso
	 * @param etiqueta nombre del campo
	 * @param valor contenido del campo
	 */
	public void actualizar(String etiqueta, String valor) {
		this.etiqueta = etiqueta;
		this.contenido = valor;
	}

}
