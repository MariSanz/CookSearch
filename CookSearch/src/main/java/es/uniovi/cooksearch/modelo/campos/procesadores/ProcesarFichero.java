package es.uniovi.cooksearch.modelo.campos.procesadores;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.springframework.web.multipart.MultipartFile;

import com.dropbox.core.DbxAppInfo;
import com.dropbox.core.DbxAuthFinish;
import com.dropbox.core.DbxClient;
import com.dropbox.core.DbxEntry;
import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.DbxWebAuthNoRedirect;
import com.dropbox.core.DbxWriteMode;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import es.uniovi.cooksearch.modelo.campos.Campo;
import es.uniovi.cooksearch.modelo.campos.CampoFichero;
import es.uniovi.cooksearch.modelo.campos.Procesador;

/**
 * Clase para procesar los datos que se necesitan guardar del fichero subido por
 * el usuario. Se procesa la creación del contenido de los objetos CampoFichero
 * que será lo que se guarda en la base de datos
 * 
 * @see CampoFichero
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public class ProcesarFichero implements Procesador {

	private static final Map<String, String> CODIGOS_OCR;

	static {
		CODIGOS_OCR = new HashMap<>();
		CODIGOS_OCR.put("ES", "spa");
	}

	private String pathFicheros;
	private String license_code = "C1DF29E2-3648-4D71-97B3-11025E372D30";
	private String user_name = "marijsanz";

	/**
	 * Constructor de la clase, donde se recibe el path del fichero que se
	 * guarda en el servidor
	 * 
	 * @param pathFicheros
	 *            {@link String} Ruta del fichero en el servidor
	 */
	public ProcesarFichero(String pathFicheros) {
		this.pathFicheros = pathFicheros;
	}

	/**
	 * Se procesa por OCR el contenido del fichero para extraer el texto y
	 * referenciar busquedas {@inheritDoc}
	 */
	@Override
	public Campo procesar(String etiqueta, Object fichero, Campo anterior) {

		MultipartFile mFichero = (MultipartFile) fichero;
		CampoFichero cfAnterior = (CampoFichero) anterior;
		
		try {

		if (mFichero.isEmpty()) {

			if (anterior == null) {
				throw new RuntimeException();
			}

			return anterior;
		}

		if (cfAnterior == null) {

			String uuid = UUID.randomUUID().toString();
			copiarADestino(new ByteArrayInputStream(mFichero.getBytes()), uuid);
			String ocrReconocido = extraerTextoDelFichero(mFichero.getBytes());

			return new CampoFichero(etiqueta, uuid, ocrReconocido, mFichero.getContentType(),
					Paths.get(mFichero.getOriginalFilename()).getFileName().toString());
		} else {

			String uuid = cfAnterior.getFichero();
			copiarADestino(new ByteArrayInputStream(mFichero.getBytes()), uuid);
			String ocrReconocido = extraerTextoDelFichero(mFichero.getBytes());

			cfAnterior.actualizar(etiqueta, ocrReconocido, mFichero.getContentType(),
					Paths.get(mFichero.getOriginalFilename()).getFileName().toString());
			return cfAnterior;
		}
		}
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Método que procesa recibir el fichero y guardar en la ruta, convertir de
	 * MultiparFile a File del servidor
	 * 
	 * @param fichero
	 *            {@link MultipartFile}
	 * @param uuid
	 *            Ruta destino del fichero {@link String}
	 * @return {@link File}
	 */
	private void copiarADestino(InputStream inputStream, String uuid) {

		try {
			
	        DbxRequestConfig config = new DbxRequestConfig(
	            "CookSearch", "es");
		        
			DbxClient client = new DbxClient(config, "2U7JlZqbXH4AAAAAAAAXZ4GJpMlS-RopER_sC-LOxVn84rycxRs11pX_jIRbXRgt");
			
		    client.uploadFile("/" + uuid,
		        DbxWriteMode.add(), -1, inputStream);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		    if (inputStream != null)
				try {
					inputStream.close();
				} catch (IOException e) {
				}
		}
		
		/*
		File path = new File(pathFicheros);
		File fFichero = new File(path, uuid);

		try {
			IOUtils.copy(fichero.getInputStream(), new FileOutputStream(fFichero));
		} catch (IOException e) {

			e.printStackTrace();
		}

		return fFichero;
		*/
	}

	/**
	 * Método para extraer con el OCR el contenido del texto del fichero
	 * 
	 * @param fFichero
	 *            {@link File}
	 * @return Texto contenido del fichero {@link String}
	 * @throws IOException
	 */
	private String extraerTextoDelFichero(byte [] fileContent) {

		String ocrURL = "http://www.ocrwebservice.com/restservices/processDocument?language=english,spanish&gettext=true";

		
		try {

			URL url = new URL(ocrURL);

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");

			connection.setRequestProperty("Authorization",
					"Basic " + Base64.getEncoder().encodeToString((user_name + ":" + license_code).getBytes()));

			connection.setRequestProperty("Content-Type", "application/json");

			connection.setRequestProperty("Content-Length", Integer.toString(fileContent.length));

			OutputStream stream = connection.getOutputStream();

			stream.write(fileContent);
			stream.close();

			int httpCode = connection.getResponseCode();

			System.out.println("HTTP Response code: " + httpCode);

			// Success request
			if (httpCode == HttpURLConnection.HTTP_OK) {
				// Get response stream
				String jsonResponse = GetResponseToString(connection.getInputStream());

				ObjectMapper mapper = new ObjectMapper();
				Map<?, ?> resultado = mapper.readValue(jsonResponse, Map.class);
				ArrayList<List<String>> cadenas = (ArrayList<List<String>>)resultado.get("OCRText");
				StringBuilder cadena = new StringBuilder();
				for (List<String> cadenasInternas: cadenas) {
					for (String c: cadenasInternas) {
						cadena.append(c);
					}
				}
				return cadena.toString();
				// Parse and print response from OCR server
				
			} else if (httpCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
				System.out.println("OCR Error Message: Unauthorizied request");
			} else {
				// Error occurred
				String jsonResponse = GetResponseToString(connection.getErrorStream());

				/*
				 * JSONParser parser = new JSONParser(); JSONObject jsonObj =
				 * (JSONObject) parser.parse(jsonResponse);
				 */

				// Error message
				// System.out.println("Error Message: " +
				// jsonObj.get("ErrorMessage"));
			}

			connection.disconnect();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		String ocrReconocido = "";

		return ocrReconocido;

		/*
		 * Locale locale = LocaleContextHolder.getLocale(); String codigo =
		 * CODIGOS_OCR.get(locale.getCountry()); if (codigo == null) { return
		 * "spa"; }
		 */

	}

	private static String GetResponseToString(InputStream inputStream) throws IOException {
		InputStreamReader responseStream = new InputStreamReader(inputStream);

		BufferedReader br = new BufferedReader(responseStream);
		StringBuffer strBuff = new StringBuffer();
		String s;
		while ((s = br.readLine()) != null) {
			strBuff.append(s);
		}

		return strBuff.toString();
	}

	// Download converted output file from OCRWebService
	private static void DownloadConvertedFile(String outputFileUrl) throws IOException {
		URL downloadUrl = new URL(outputFileUrl);
		HttpURLConnection downloadConnection = (HttpURLConnection) downloadUrl.openConnection();

		if (downloadConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {

			InputStream inputStream = downloadConnection.getInputStream();

			// opens an output stream to save into file
			FileOutputStream outputStream = new FileOutputStream("C:\\converted_file.doc");

			int bytesRead = -1;
			byte[] buffer = new byte[4096];
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}

			outputStream.close();
			inputStream.close();
		}

		downloadConnection.disconnect();
	}
	
	public static void main(String[] args) throws IOException, DbxException {
	        // Get your app key and secret from the Dropbox developers website.
	        final String APP_KEY = "mwfsut697ww216e";
	        final String APP_SECRET = "3v9bh1hkk0hjm5z";

	        DbxAppInfo appInfo = new DbxAppInfo(APP_KEY, APP_SECRET);

	        DbxRequestConfig config = new DbxRequestConfig(
	            "CookSearch", "es");
	        DbxWebAuthNoRedirect webAuth = new DbxWebAuthNoRedirect(config, appInfo);
	
	     // Have the user sign in and authorize your app.
	        String authorizeUrl = webAuth.start();
	        System.out.println("1. Go to: " + authorizeUrl);
	        System.out.println("2. Click \"Allow\" (you might have to log in first)");
	        System.out.println("3. Copy the authorization code.");
	        String code = new BufferedReader(new InputStreamReader(System.in)).readLine().trim();
	        
	        DbxAuthFinish authFinish = webAuth.finish(code);
	        String accessToken = authFinish.accessToken;
	        
	        System.out.println(accessToken);
	}

}
