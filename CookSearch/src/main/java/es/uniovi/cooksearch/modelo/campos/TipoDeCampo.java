package es.uniovi.cooksearch.modelo.campos;

/**
 * Interfaz que representa el tipo de dato extra que podrá personalizar el
 * usuario a su anotación
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public interface TipoDeCampo {
	
	/**
	 * Enumerado de los tipos mimes de los campos que se representan en las
	 * paginas de la intefaz
	 * @author María José Sánchez Doria
	 *
	 */
	public static enum CampoForm {
		TEXTO,
		TEXTO_LARGO,
		FICHERO;
	}

	/**
	 * Método para que cada campo devuelva el tipo input que representar 
	 * en la página de intefaz de la aplicación
	 * @return {@link CampoForm} 
	 */
	public CampoForm getCampoForm();

	/**
	 * Método que devuleve el procesador específico que debe tratar la información de cada campo
	 * @return {@link Procesador}
	 */
	public Procesador getProcesador();
	
	/**
	 * Para procesar el borrado del campo durante la edición, cada campo 
	 * procesa su propio borrado
	 * @param campo
	 */
	public void borrar(Campo campo);
	
	public String getNombre();

}
