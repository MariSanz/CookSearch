package es.uniovi.cooksearch.modelo;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Clase del modelo de la base de datos para representar los comentarios de las
 * anotaciones en la base de datos
 * 
 * @see Anotacion
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
@Document
public class Comentario {

	@Id
	private String id;

	private String usuario;
	private String contenido;
	private String anotacionId;
	private Date fecha;
	private Boolean aprobado;

	public Comentario() {
		this.fecha = new Date();
	}

	/**
	 * Método que retorna el ID del usuario que realizo el comentario en l
	 * anotación
	 * 
	 * @return {@link String}
	 */
	public String getUsuario() {
		return usuario;
	}

	/**
	 * Método para cambiar el ID del usuario que realizo el comentario
	 * 
	 * @param usuario nombre del usuario
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	/**
	 * Método que retorna el contenido del comentario
	 * 
	 * @return {@link String}
	 */
	public String getContenido() {
		return contenido;
	}

	/**
	 * Método para cambiar el contenido del comentario
	 * 
	 * @param contenido
	 *            {@link String}
	 */
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	/**
	 * Método para obtener el ID de la anotación a la que pertenece el
	 * comentario
	 * 
	 * @return Identificador de la anotación
	 */
	public String getAnotacionId() {
		return anotacionId;
	}

	/**
	 * Método para cambiar el ID de la anotación a la que pertenece el
	 * comentario
	 * 
	 * @param anotacionId identificador de la anotación 
	 */
	public void setAnotacionId(String anotacionId) {
		this.anotacionId = anotacionId;
	}

	/**
	 * Método para obtener la fecha en la que se realizó el comentario
	 * 
	 * @return {@link Date}
	 */
	public Date getFecha() {
		return fecha;
	}

	/**
	 * Método para asignar la fecha en que se realiza el comentario
	 * 
	 * @param fecha
	 *            {@link Date}
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	/**
	 * Método para obtener el boolean si el comentario ha sido aprobado o no
	 * 
	 * @return
	 */
	public Boolean getAprobado() {
		return aprobado;
	}

	/**
	 * Método para saber si el comentario está aprobado o no
	 * @param aprobado
	 */
	public void setAprobado(Boolean aprobado) {
		this.aprobado = aprobado;
	}

	/**
	 * Méodo para obtener el ID del comentario en la base de datos
	 * @return {@link String}
	 */
	public String getId() {
		return id;
	}

	/**
	 * Método para asignar el identificador del comentario en la base de datos
	 * @param id {@link String}
	 */
	public void setId(String id) {
		this.id = id;
	}

}
