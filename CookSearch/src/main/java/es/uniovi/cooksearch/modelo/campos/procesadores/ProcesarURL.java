package es.uniovi.cooksearch.modelo.campos.procesadores;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import es.uniovi.cooksearch.modelo.campos.Campo;
import es.uniovi.cooksearch.modelo.campos.CampoURL;
import es.uniovi.cooksearch.modelo.campos.Procesador;

/**
 * Clase para procesar la creación del contenido de un objeto
 * tipo CampoURL. Aqui se procesa la descarga del contenido de la URL y 
 * los datos que nos interesa como son el nombre del campo y dicha 
 * ruta de URL
 * @see CampoURL
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public class ProcesarURL implements Procesador {

	/**
	 * Método que procesa la creación del objeto CampoURL, se inicia el proceso
	 * descargar el contenido textual de la web
	 * {@inheritDoc} Interfaz Campo
	 */
	@Override
	public Campo procesar(String etiqueta, Object valor, Campo campo) {

		CampoURL campoURL = (CampoURL)campo;
		String ruta = (String) valor;

		if (campoURL == null) {
			String ocrReconocido = getTextoContenidoURL(ruta);
			return new CampoURL(etiqueta, ruta, ocrReconocido);
		}
		else {
			if (!campoURL.getContenido().equals(ruta)) {
				String ocrReconocido = getTextoContenidoURL(ruta);
				campoURL.actualizar(etiqueta, ruta, ocrReconocido);
				return campoURL;
			}
			else {
				return campoURL;
			}
		}
	}

	/**
	 * Método para devolver el texto descargado del contenido de la URL. Se descarga 
	 * por conexión a internet el contenido de la pagina web de la url, este texto es 
	 * limpiado de las etiquetas html y entonces se guarda en la base de datos
	 * @param ruta ULR de la web
	 * @return Contenido de la url 
	 */
	private String getTextoContenidoURL(String ruta) {

		StringBuilder builder = new StringBuilder();
		try {

			URL url = new URL(ruta);
			URLConnection urlc = url.openConnection();

			BufferedInputStream buffer = new BufferedInputStream(urlc.getInputStream());

			int byteRead;
			while ((byteRead = buffer.read()) != -1)
				builder.append((char) byteRead);

			buffer.close();

		} catch (MalformedURLException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}

		return builder.toString().replaceAll("\\<.*?\\>", "");
	}

}
