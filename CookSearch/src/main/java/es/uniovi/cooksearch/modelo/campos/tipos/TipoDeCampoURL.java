package es.uniovi.cooksearch.modelo.campos.tipos;

import es.uniovi.cooksearch.modelo.campos.Campo;
import es.uniovi.cooksearch.modelo.campos.CampoURL;
import es.uniovi.cooksearch.modelo.campos.Procesador;
import es.uniovi.cooksearch.modelo.campos.TipoDeCampo;
import es.uniovi.cooksearch.modelo.campos.procesadores.ProcesarURL;

/**
 * Clase que indica el tipo de un campo del atributo masDatos del documento
 * Anotacion tipo del campo url
 * 
 * @see CampoURL
 * @author María José Sánchez Doria
 * @version 1.0
 */
public class TipoDeCampoURL implements TipoDeCampo {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CampoForm getCampoForm() {
		return CampoForm.TEXTO;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Procesador getProcesador() {
		return new ProcesarURL();
	}

	@Override
	public void borrar(Campo campo) {
	}
	
	@Override
	public String getNombre() {
		return "URL";
	}
}
