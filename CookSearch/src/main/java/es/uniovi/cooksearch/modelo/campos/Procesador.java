package es.uniovi.cooksearch.modelo.campos;

import org.springframework.web.multipart.MultipartFile;

/**
 * Interfaz para definir el metodo que procesará los campos segun las
 * necesidades del tipo Fichero, Texto y URL
 * 
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public interface Procesador {

	/**
	 * Método que procesa los datos de los campos para devolver un campo
	 * específico segun el tipo y los atributos necesarios de ese tipo
	 * especializado
	 * 
	 * @param etiqueta
	 *            {@link String}
	 * @param valor
	 *            Es object para representar los tipos {@link MultipartFile} o
	 *            {@link String}
	 * @return {@link Campo}
	 */
	public Campo procesar(String etiqueta, Object valor, Campo anterior);

}
