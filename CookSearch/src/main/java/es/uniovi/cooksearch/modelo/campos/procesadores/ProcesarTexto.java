package es.uniovi.cooksearch.modelo.campos.procesadores;

import es.uniovi.cooksearch.modelo.campos.Campo;
import es.uniovi.cooksearch.modelo.campos.CampoTexto;
import es.uniovi.cooksearch.modelo.campos.Procesador;

/**
 * Clase para procesar los datos que se necesitan guardar de los campos textos subidos por
 * el usuario. Se procesa la creación del contenido de los objetos CampoTexto
 * que será lo que se guarda en la base de datos. No contiene los campos URL
 * 
 * @see CampoTexto
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public class ProcesarTexto implements Procesador {

	/*Para saber si el campo es de tipo texto largo o corto */
	private boolean largo;

	/**
	 * Constructor de la clase, donde se recibe es si es de tipo corto o largo
	 * @param largo
	 */
	public ProcesarTexto(boolean largo) {
		this.largo = largo;
	}

	/**
	 * Se crea el objeto CampoTexto con la etiqueta y su contenido String 
	 * {@inheritDoc}
	 */
	@Override
	public Campo procesar(String etiqueta, Object valor, Campo campo) {
		if (campo == null) {
			return new CampoTexto(etiqueta, (String) valor, largo);
		} else {
			((CampoTexto) campo).actualizar(etiqueta, (String) valor);
			return campo;
		}
	}

}
