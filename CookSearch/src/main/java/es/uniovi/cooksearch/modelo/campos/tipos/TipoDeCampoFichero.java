package es.uniovi.cooksearch.modelo.campos.tipos;

import java.io.File;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.annotation.Id;
import org.springframework.web.multipart.MultipartFile;

import es.uniovi.cooksearch.modelo.campos.Campo;
import es.uniovi.cooksearch.modelo.campos.CampoFichero;
import es.uniovi.cooksearch.modelo.campos.Procesador;
import es.uniovi.cooksearch.modelo.campos.TipoDeCampo;
import es.uniovi.cooksearch.modelo.campos.procesadores.ProcesarFichero;

/**
 * Clase que indica el tipo de un campo del atributo masDatos del documento
 * Anotacion
 * 
 * @see Anotacion, CampoFichero
 * @author María José Sánchez Doria
 * @version 1.0
 *
 */
public class TipoDeCampoFichero implements TipoDeCampo {

	@Value("${path_ficheros}")
	private String pathFicheros;

	@Id
	private String id;

	/**
	 * Fichero a recibir desde el service
	 */
	private MultipartFile archivo;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CampoForm getCampoForm() {
		return CampoForm.FICHERO;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Procesador getProcesador() {
		return new ProcesarFichero(pathFicheros);
	}

	/**
	 * Método que devuelve el fichero a tratar
	 * 
	 * @return archivo {@link MultipartFile}
	 */
	public MultipartFile getArchivo() {
		return archivo;
	}

	/**
	 * Método para cambiar el fichero a tratar
	 * 
	 * @param archivo
	 *            {@link MultipartFile}
	 */
	public void setArchivo(MultipartFile archivo) {
		this.archivo = archivo;
	}
	
	@Override
	public void borrar(Campo campo) {
		CampoFichero cf = (CampoFichero)campo;
		String pathFichero = pathFicheros + "/" + cf.getFichero();
		File f = new File(pathFichero);
		f.delete();
	}
	
	@Override
	public String getNombre() {
		return "Fichero";
	}

}
