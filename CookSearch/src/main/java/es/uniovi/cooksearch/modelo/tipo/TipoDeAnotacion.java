package es.uniovi.cooksearch.modelo.tipo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class TipoDeAnotacion {
	
	/* RECETA("anotacion.tipoReceta"),
	RESTARANTE("anotacion.tipoRestaurante"),
	lIBROS_GUIAS("anotacion.tipoLibro"),
	TECNICAS("anotacion.tipoTecnica"),
	UTENCILIO("anotacion.tipoUtencilio"),
	PROVEEDOR("anotacion.tipoProveedor"),
	OTRO("anotacion.tipoOtro");*/
	
	
	
	@Id
	private String id;
	
	private String nombre;
	
	private String usuarioId;

	public TipoDeAnotacion() {}
	
	public TipoDeAnotacion(String nombre, String usuarioId) {
		this.nombre = nombre;
		this.usuarioId = usuarioId;
	}
	public TipoDeAnotacion (String nombre){
		this.nombre= nombre;
	}
	
	public String getId() {
		return id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public String getUsuarioId() {
		return usuarioId;
	}

}
