package es.uniovi.cooksearch.modelo.campos;

import java.util.UUID;

import org.springframework.data.mongodb.core.mapping.Document;

import es.uniovi.cooksearch.modelo.Anotacion;

/**
 * Clase del modelo de la base de datos que es embebido en el atributo masDatos
 * de la coleccion Anotación. La presente clase representa los datos que se
 * necesitan tratar de las url guardadas por los usuarios en sus notas
 * 
 * @see Anotacion
 * @author María José Sánchez Doria
 * @version 1.0
 */
@Document
public class CampoURL implements Campo {

	/*
	 * Representa el ID en String del documento en la base de datos
	 */
	private String id;

	/*
	 * Nombre del campo
	 */
	private String etiqueta;
	/*
	 * Dirección url que se guarda
	 */
	private String contenido;
	/*
	 * Texto descargado de leer el contenido de la url
	 */
	private String textoOcr;

	/**
	 * Constructor de la clase
	 * 
	 * @param etiqueta
	 *            Nombre del campo
	 * @param ruta
	 *            Ruta de la url
	 * @param textoOcr
	 *            Contenido descargado de la url
	 */
	public CampoURL(String etiqueta, String contenido, String textoOcr) {
		this.id = UUID.randomUUID().toString();
		this.etiqueta = etiqueta;
		this.contenido = contenido;
		this.textoOcr = textoOcr;
	}

	public String getId() {
		return id;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getEtiqueta() {
		return etiqueta;
	}

	/**
	 * Método para cambiar el valor del nombre del campo
	 * 
	 * @param etiqueta
	 *            Nombre del campo
	 */
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	/**
	 * {@inheritDoc}}
	 */
	@Override
	public String getPresentador() {

		return "texto";
	}

	/**
	 * Método que devuelve la ruta del url guardada
	 * 
	 * @return ruta de la web
	 */
	public String getContenido() {
		return contenido;
	}

	/**
	 * Método para cambiar la ruta de url
	 * 
	 * @param contenido
	 *            ruta de la web
	 */
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	/**
	 * Método que devuelve el contenido de la url
	 * 
	 * @return textoOcr Texto del contenido de la url
	 */
	public String getTextoOcr() {
		return textoOcr;
	}

	/**
	 * Método para cambiar el contenido de la descargado
	 * 
	 * @param textoOcr
	 *            Texto del contenido de la url
	 */
	public void setTextoOcr(String textoOcr) {
		this.textoOcr = textoOcr;
	}

	/**
	 * Retorna "tipo-de-campo-url"
	 */
	@Override
	public String getTipo() {
		return "tipo-de-campo-url";
	}

	/**
	 * Los campos URL pueden ser editados, cambiando el nombre de la etiqueta o
	 * la ruta de la web, y por tanto el contenido textual de esta, este metodo
	 * realiza el proceso
	 * 
	 * @param etiqueta
	 *            nombre del campo
	 * @param ruta
	 *            ruta de la web
	 * @param ocrReconocido
	 *            contenido textual descargado de la web
	 */
	public void actualizar(String etiqueta, String ruta, String ocrReconocido) {
		this.etiqueta = etiqueta;
		this.contenido = ruta;
		this.textoOcr = ocrReconocido;
	}

}
