<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8" />
<meta name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta name="_csrf_header" content="${_csrf.headerName}" />

<title><spring:message code='usuario.admin' /></title>

<link rel="stylesheet"
	href='<c:url value="/bootstrap/css/bootstrap.css" />' />
<link rel="stylesheet"
	href='<c:url value="/bootstrap/css/dashboard.css" />' />
<link rel="stylesheet" href='<c:url value="/css/principal.css" />' />
<script type="text/javascript"
	src='<c:url value="/jquery/jquery-1.12.3.min.js" />'></script>

</head>
<body>

	<nav class="navbar navbar-default navbar-fixed-top">

		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
					aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/"> <img class="logo" alt="<spring:message
										code='index.inicio' />"
					src='<c:url value="/image/logoCookSearch.png"/>' />
				</a>
			</div>

			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"> <span
							class="glyphicon glyphicon-list gi-1x" aria-hidden="true"></span>
							<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#quien_somos_modal" data-toggle="modal"><spring:message
										code='index.quienSomos' /></a></li>
							<li><a href="#contacto_admin_modal" data-toggle="modal"><spring:message
										code='index.contactoAdmin' /></a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#"><spring:message code='index.ayuda' /> <span
									class="glyphicon glyphicon-question-sign" aria-hidden="true"></span></a>
							</li>
						</ul></li>
				</ul>

				<!-- Modal -->
				<div id="quien_somos_modal" class="modal fade" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">
									<spring:message code='index.quienSomos' />
								</h4>
							</div>
							<div class="modal-body">
								<p>
									<spring:message code='index.quienSomosContenido1' />
								</p>
								<p>
									<spring:message code='index.quienSomosContenido2' />
								</p>

								<p class="text-info">
									<spring:message code='index.quienSomosContenido3' />
								</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default"
									data-dismiss="modal">
									<spring:message code='index.cerrar' />
								</button>
							</div>
						</div>

					</div>
				</div>

				<!-- Modal -->
				<div id="contacto_admin_modal" class="modal fade" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">
									<spring:message code='index.contactoAdmin' />
								</h4>
							</div>
							<div class="modal-body">
								<p>
									<spring:message code='index.contactoAdminContenido1' />
								</p>
								<p class="text-info">UO232334&#64;uniovi.es</p>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default"
									data-dismiss="modal">
									<spring:message code='index.cerrar' />
								</button>
							</div>
						</div>

					</div>
				</div>

				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false"> <spring:message
								code='index.cambiarIdioma' /><span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="?locale=es"><spring:message
										code='index.espanol' /> </a></li>
							<li><a href="?locale=en"><spring:message
										code='index.ingles' /> </a></li>
						</ul></li>
						
					<security:authorize access="isAnonymous()">
						<li><a href="/usuarios/registro"> <spring:message
									code='usuario.registrarse' /></a></li>
						<li><a href="/login"> <spring:message
									code='usuario.login' /></a></li>
					</security:authorize>
					
					<security:authorize access="isAuthenticated()">
						<li>
							<form action='<c:url value="/logout" />' method="post">
								<input type="hidden" name="${_csrf.parameterName}"
									value="${_csrf.token}" />
								<button type="submit" class="btn btn-default navbar-btn">
									<spring:message code="usuario.cerrarSesion" />
								</button>
							</form>
						</li>
					</security:authorize>
				</ul>
			</div>

		</div>

	</nav>


<div class="container separator">

		<tiles:insertAttribute name="contenido" />
	
</div>

<div id="footer separator" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

		<div class="col-md-4">
			<span class="glyphicon glyphicon-copyright-mark"></span>
			<spring:message code="index.reservaDerechos" />
		</div>

		<div class="col-md-2">
			<a class="btn btn-link" href="/condiciones-uso" >
				<spring:message code="index.condicionesUso" />
			</a>
		</div>

		<div class="col-md-2">
			<a class="btn btn-link" href="/politica-privacidad"> 
			<spring:message	code="index.politicaPrivacidad" />
			</a>
		</div>

		<div class="col-md-2">
			<a class="btn btn-link" href="/aviso-legal" >
				<spring:message code="index.avisoLegal" />
			</a>
		</div>

		<div class="col-md-2">
			<a class="btn btn-link" href="/politica-cookies" >
				<spring:message code="index.politicaCookies" />
			</a>
		</div>

	</div>

	<script type="text/javascript"
		src='<c:url value="/bootstrap/js/bootstrap.min.js" />'>
	</script>
		




</body>
</html>







