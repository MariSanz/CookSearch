<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<link rel="stylesheet"
	href='<c:url value="/bootstrap/css/sortable-theme-bootstrap.css" />' />
<div class="container separator">

	<div class="container-fluid">
	
		<div class="row">
	        <div class="col-sm-3 col-md-2 sidebar">
	          <ul class="nav nav-sidebar">
	            <li >
	            	 <a class="btn btn-link" href="#buscar_comentarios" data-toggle="modal">
		            	<spring:message code="admin.buscarComentarios" />
		            </a>
	            </li>
	            <li >
	            	<form action='<c:url value="/admin/comentarios/listado" />' method="get">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<button type="submit" class="btn btn-link">
							<spring:message code="admin.listarComentarios" />
						</button>
					</form>
				</li>
				 <li >
	            	<form action='<c:url value="/admin/comentarios/listado-aprobar" />' method="get">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<button type="submit" class="btn btn-link">
							<spring:message code="admin.aprobarComentarios" />
						</button>
					</form>
				</li>
	          </ul>
	        </div>
	        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	          <h1 class="page-header text-center"><spring:message code="admin.bienvenido" /></h1>
	          <h2  class="text-center"><spring:message code="admin.listadoComentariosSinAprobar" /> </h2>
				<c:if test="${not empty mensaje}">
					<div class="text-center"><spring:message code="${mensaje}" /></div>
				</c:if>
				<div class="row separator">
				        <div class="panel panel-primary">
				            
				<table id="comentarios" class="table table-hover table paginated sortable-theme-bootstrap" data-sortable>
					<thead class="thead-inverse">
						<tr>
							 <th>#</th>
							 <th class="text-center"><spring:message code="usuario.nombre" /></th>
							 <th class="text-justify"><spring:message code="anotacion.comentario" /></th>
							 <th class="text-center"><spring:message code="anotacion.fecha" /></th>
							 <th class="text-center"><spring:message code="comentario.estado" /></th>
							 <th class="text-center"><spring:message code="anotacion.opciones" /></th>
						</tr>
					</thead>
					<tbody>
					
						<c:choose>
							<c:when test="${empty listadoComentarios}">
								<td scope="row">--</td>
								<td class="text-center">--</td>
								<td class="text-justify"><spring:message code="comentario.noHay"/> </td>
								<td class="text-center">--</td>
								<td class="text-center">--</td>
								<td class="text-center">--</td>
							</c:when>
							<c:otherwise>
							<c:forEach items="${listadoComentarios}" var="comentario" varStatus="status">
							<tr>
								<td scope="row">${status.count}</td>
								<td class="text-center">${comentario.usuario}</td>
								<td class="text-justify">${comentario.contenido}</td>
								<td class="text-center">${comentario.fecha}</td>
								<td class="text-center">
									<c:choose>
										<c:when test="${comentario.aprobado eq true}">
											<spring:message code="comentario.aprobado"/>
										</c:when>
										<c:otherwise>
											<spring:message code="comentario.NoAprobado"/>
										</c:otherwise>
									</c:choose>
								
								</td>
								<td class="text-center" style="text-align: center;">
								
									<c:if test="${comentario.aprobado eq true}">
										<c:url value="/admin/comentarios/aprobar" var="urlAprobar"/>
										<form action="${urlAprobar}" method="post">
										
											<input type="hidden" name="${_csrf.parameterName}"
														value="${_csrf.token}" />
											
											<input name="id" value="${comentario.id}" type="hidden"/>
											<button type="submit" class="btn btn-default">
												<spring:message code="comentario.aprobar" />
											</button>
										</form>
									</c:if>
									<div class="separator">
									
										<c:url value="/admin/comentarios/borrar/sa" var="urlBorrar"/>
											<form action="${urlBorrar}" method="post">
												<input type="hidden" name="${_csrf.parameterName}"
													value="${_csrf.token}" />
												<input type="hidden" name="idAnotacion"
													value="${comentario.anotacionId}" />
												<input type="hidden" name="idComentario"
													value="${comentario.id}" />
												<button type="submit" class="btn btn-default">
													<spring:message code="comentario.eliminar" />
												</button>
											</form>
										
									</div>
								</td>
							</tr>
							</c:forEach>
						</c:otherwise>
						</c:choose>
					</tbody>
					</table>
				</div>
				</div>
	        </div>
		</div>
	</div>

</div>

<!-- Modal  busqueda avanzada-->
<div id="buscar_comentarios" class="modal fade" role="dialog">


	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<spring:message code="anotacion.busquedaAvanzada" />
				</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal"
					action='<c:url value="/admin/comentarios/buscar" />' method="post">
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" id="token_seguridad" />

					<div class="form-group">
						<spring:message code="comentario.contenido" var="msgContenido" />
						<h5> ${msgContenido}</h5> 
						<input type="text" name="contenido" class="form-control" />
					</div>

					<div class="form-group">
						<spring:message code="comentario.usuario" var="msgUsuario" />
						<h5>${msgUsuario}</h5>
						<input type="text" name="usuario" class="form-control"/>
					</div>
					
					<div class="form-group">
						<button id="boton_busqueda_avanzada" type="submit"
							class="btn btn-primary center-block">
							<spring:message code="anotacion.buscar" />
						</button>
					</div>

				</form>
			</div>

		</div>
		
	</div>

</div>

<script>


$('table.paginated').each(function() {
    var currentPage = 0;
    var numPerPage = 10;
    var $table = $(this);
    $table.bind('repaginate', function() {
        $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
    });
    $table.trigger('repaginate');
    var numRows = $table.find('tbody tr').length;
    var numPages = Math.ceil(numRows / numPerPage);
    var $pager = $('<div class="pager"></div>');
    for (var page = 0; page < numPages; page++) {
        $('<span class="page-number"></span>').text(page + 1).bind('click', {
            newPage: page
        }, function(event) {
            currentPage = event.data['newPage'];
            $table.trigger('repaginate');
            $(this).addClass('active').siblings().removeClass('active');
        }).appendTo($pager).addClass('clickable');
    }
    $pager.insertBefore($table).find('span.page-number:first').addClass('active');
});
</script>

	
<!-- Para ordenar resultados por cada campo -->
<script src='<c:url value="/jquery/sortable.min.js" />'></script>
