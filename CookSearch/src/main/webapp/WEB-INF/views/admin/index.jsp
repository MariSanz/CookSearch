<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<div class="container separator">

	<div class="container-fluid">
	
		<div class="row">
	        <div class="col-sm-3 col-md-2 sidebar">
	          <ul class="nav nav-sidebar">
	             <li>
	            	 <a class="btn btn-link" href="#buscar_comentarios" data-toggle="modal">
		            	<spring:message code="admin.buscarComentarios" />
		            </a>
	            </li>
	            <li >
	            	<form action='<c:url value="/admin/comentarios/listado" />' method="get">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<button type="submit" class="btn btn-link">
							<spring:message code="admin.listarComentarios" />
						</button>
					</form>
				</li>
				 <li>
	            	<form action='<c:url value="/admin/comentarios/listado-aprobar" />' method="get">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<button type="submit" class="btn btn-link">
							<spring:message code="admin.aprobarComentarios" />
						</button>
					</form>
				</li>
	          </ul>
	        </div>
	        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	          <h1 class="page-header text-center"><spring:message code="admin.bienvenido" /></h1>
	        </div>
		</div>
	</div>

<!-- Panel de Cookies -->

	<div class="alert alert-dismissible alert-info" id="panelCookies">
	
	
		<button type="button" class="close" data-dismiss="alert"
			onclick="controlcookies()">&times;</button>
	
		<h3>
			<strong><spring:message code="index.usoCookies" /></strong>
		</h3>
		<p class="text-justify">
			<spring:message code="index.usoCookiesContenido1" />
			<spring:message code="index.usoCookiesContenido2" />
			<spring:message code="index.usoCookiesContenido3" />
		</p>
		<p class="text-justify separator">
			<spring:message code="index.usoCookiesContenido4" />
			<a href="/"> <spring:message code='index.politicaCookies' /></a>
			<spring:message code="index.usoCookiesContenido5" />
	
		</p>
	
		<p class="text-justify separator">
			<spring:message code="index.usoCookiesContenido6" />
		</p>
	
		<button type="button" class="btn btn-primary center-block separator"
			data-dismiss="alert" onclick="controlcookies()">
			<spring:message code="index.acepto" />
		</button>
	</div>

</div>

<!-- Modal  busqueda avanzada-->
<div id="buscar_comentarios" class="modal fade" role="dialog">


	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<spring:message code="comentario.busquedaAvanzada" />
				</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal"
					action='<c:url value="/admin/comentarios/buscar" />' method="post">
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" id="token_seguridad" />

					<div class="form-group">
						<spring:message code="comentario.contenido" var="msgContenido" />
						<h5> ${msgContenido}</h5> 
						<input type="text" name="contenido" class="form-control" />
					</div>

					<div class="form-group">
						<spring:message code="comentario.usuario" var="msgUsuario" />
						<h5>${msgUsuario}</h5>
						<input type="text" name="usuario" class="form-control"/>
					</div>
					
					<div class="form-group">
						<button id="boton_busqueda_avanzada" type="submit"
							class="btn btn-primary center-block">
							<spring:message code="anotacion.buscar" />
						</button>
					</div>

				</form>
			</div>

		</div>
		
	</div>

</div>
<!-- Acceso a la politica de cookies -->
<script type="text/javascript">
	function controlcookies() {
		// si variable no existe se crea (al clicar en Aceptar)
		localStorage.controlcookie = (localStorage.controlcookie || 0);

		localStorage.controlcookie++; // incrementamos cuenta de la cookie
	}

	if (localStorage.controlcookie > 0) {
		var panelCookies = document.getElementById('panelCookies');

		panelCookies.style.visibility = 'hidden';
		panelCookies.style.display = 'none';

	}
</script>

<script>
function listarComentarios(){
	$(function() {
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		$(document).ajaxSend(function(e, xhr, options) {
			xhr.setRequestHeader(header, token);
		});
	});
	

		$.ajax({
			url : '/admin/comentarios/listado',
			contentType: "application/json",
			processData : false,
			type : 'POST',
		});
	
}
</script>





