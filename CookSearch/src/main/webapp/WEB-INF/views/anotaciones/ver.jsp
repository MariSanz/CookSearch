<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
	
<link rel="stylesheet"
	href='<c:url value="/css/jQuery.score.css" />' />
	
<div class="panel panel-primary">

	<div class="panel-heading">
		<h3 class="text-uppercase text-center font-white">
			${anotacion.nombre}
		</h3>
	</div>

	<div class="panel-body">
	
		<h3 class="text-center"><spring:message code="anotacion.informacion"/></h3>
		<hr class=".line-separator">
			<div>
				<h4><spring:message code="anotacion.descripcionAnotacion"/></h4>
				<p class="lead text-justify" >${anotacion.descripcion}</p>
			</div>
			<h4><spring:message code="anotacion.tipoContenido"/></h4>
				<p class="lead text-justify">
					<c:choose>
					  <c:when test="${not empty anotacion.tipoDeAnotacion.usuarioId}">
					   	  ${anotacion.tipoDeAnotacion.nombre}
					  </c:when>
					  <c:otherwise>
					  	<spring:message code="${anotacion.tipoDeAnotacion.nombre}" />
					  </c:otherwise>
				</c:choose>
				</p>
		<c:if test="${not empty anotacion.campos}">
		<h3 class="text-center"><spring:message code="anotacion.masDatos"/></h3>
		<hr class=".line-separator">
			<c:forEach items="${anotacion.campos}" var="campo">
				<c:set var="campo" scope="request" value="${campo}"/>
				<div class="separator">
					<h4>${campo.etiqueta}</h4>
					<div>
						<jsp:include page="/WEB-INF/views/presentador/${campo.presentador}.jsp"/>
					</div>
				</div>
				
			</c:forEach>
		</c:if>
		<div style="display: inline"> 
			<h4> <spring:message code="anotacion.fechaPublicacion"/></h4> 
			<p>${anotacion.fechaPublicacion}</p>
		</div>
	</div>
</div>

<div class="panel panel-default">
  <div class="panel-heading"><h4><spring:message code="anotacion.puntuacion"/></h4></div>
  <div class="panel-body" id="votos">
			<input type="hidden" name="anotacionId"
				value="${anotacion.anotacionId}" id="id" />
				
			<div class="score-star"></div>
			
				<p class="lead" style="display: inline"><spring:message code="anotacion.votos"/>
				<p id="numero-votos" class="lead" style="display: inline">${anotacion.votos}</p>
			
			
			
			<c:if test="${not empty mensaje}">
				<div class="alert alert-info"><spring:message code="${mensaje}"/></div>
			</c:if>
	</div>
</div>



<div class="panel panel-default">
  <div class="panel-heading"><h4><spring:message code="anotacion.comentarios"/></h4></div>
  <div class="panel-body" id="tabla_comentarios">
 
		<input type="hidden" name="anotacionId"
			value="${anotacion.anotacionId}" id="id" />
			
		<table id="comentarios" class=" table order-list">
	    <thead>
	        <tr>
	            <td class="text-center"><strong><spring:message code="usuario.nombre"/></strong></td>
	            <td class="text-justify"><strong><spring:message code="anotacion.comentario"/></strong></td>   
	        </tr>
	    </thead>
	    
	    <tbody>
	  
			<c:choose>
				<c:when	test="${not empty anotacion.comentarios}">
					<c:forEach items="${anotacion.comentarios}" var="comentario">
						<tr>
							<td class="col-sm-2 text-center">${comentario.usuario}</td>
							<td class="col-sm-6 text-justify">${comentario.contenido}</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<td><h5 class="text-center">- - -</h5></td>
						<td><h5 class="text-justify"><spring:message code="anotacion.noComentario" /></h5></td>
					</tr>
		
				</c:otherwise>
			</c:choose>
				<tr>
					<td><h5 class="text-center"><input id="usuario_comentario" type="text" class="form-control oculto" name="usuarioSession" readonly placeholder="${usuarioSession}"/></h5></td>
					<td><h5 class="text-center"><input id="contenido_comentario" type="text" class="form-control oculto" name="nuevoComentario"/></h5></td>
				</tr>
	    </tbody>
	    	
	    <tfoot>
	   
	    <security:authorize access="isAuthenticated()">
	    		<c:if test="${!anotacion.propia}">
			        <tr>
			        	 <td>
			                <input type="button" id="bt_aniadir_comentario" class="btn font-weight-bold" value='<spring:message code="anotacion.aniadirComentario"/>' onclick="aniadirComentario()"/>
			            </td>
			            <td> <button type="submit" id="bt_guardar_comentario" class="btn font-weight-bold  btn-primary oculto" onclick="guardarComentario()"><spring:message code="anotacion.guardarComentario"/></button></td>
			        </tr>
		       </c:if>
	        </security:authorize>
	        <tr>
	        </tr>
	    </tfoot>
	</table>
	 <div id="msg_info" class="alert alert-success oculto">
	    
	 </div>

  
  </div>
</div>
<security:authorize access="isAuthenticated()">
	<c:if test="${anotacion.propia}">
		<spring:url value="/anotaciones/editar/{indice}-{id}" var="url">
			<spring:param name="id" value="${anotacion.anotacionId}"/>
			<spring:param name="indice" value="${anotacion.indexador}"/>
		</spring:url>
		<a href="${url}" class="btn btn-default" type="submit">
			<span class="glyphicon glyphicon-edit"> </span>
			<spring:message code="anotacion.editar"/>
		</a>
		
			<spring:url value="/anotaciones/borrar/{id}" var="url">
			<spring:param name="id" value="${anotacion.anotacionId}"/>
		</spring:url>
		<a href="${url}" class="btn btn-default" type="submit">
			<span class="glyphicon glyphicon-trash"> </span>
			<spring:message code="anotacion.eliminar"/>
		</a>	
	</c:if>
</security:authorize>

<script type="text/javascript"
		src='<c:url value="/jquery/jQuery.score.js" />'></script>



<!-- Aniadir comentarios -->
<script>

function aniadirComentario(){
	document.getElementById("msg_info").innerHTML= "";
	document.getElementById("msg_info").style.display = "none";
	
	document.getElementById("usuario_comentario").style.display = "inline-table";
	document.getElementById("contenido_comentario").style.display = "inline-table";
	document.getElementById("bt_aniadir_comentario").style.display = "none";
	document.getElementById("bt_guardar_comentario").style.display = "block";
	
}

function errorAniadiendoComentario() {
	alert('Error');
}

function guardarComentario(){
	$(function() {
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		$(document).ajaxSend(function(e, xhr, options) {
			xhr.setRequestHeader(header, token);
		});
	});
	
	$("#tabla_comentarios").each(function(index) {
		var contenido = $(this).find('input[name="nuevoComentario"]').val();
		var id = $(this).find('input[name="anotacionId"]').val();
		
		$.ajax({
			url : '/comentarios/guardar',
			data : JSON.stringify({anotacionId: id, 
				nuevoComentario: contenido}),
			contentType: "application/json",
			processData : false,
			success: verResultado,
			error: errorAniadiendoComentario,
			type : 'POST',
		});
	})
	
	
}

function verResultado(dato){
	document.getElementById("msg_info").innerHTML= "";
	$('#msg_info')
	.append(
			$('</p>')
				.text('<spring:message code="comentario.guardado"/>'));
	document.getElementById("msg_info").style.display = "block";
	
	document.getElementById("usuario_comentario").style.display = "none";
	document.getElementById("usuario_comentario").value = "";
	document.getElementById("contenido_comentario").style.display = "none";
	document.getElementById("contenido_comentario").value = "";
	document.getElementById("bt_aniadir_comentario").style.display = "inline";
	document.getElementById("bt_guardar_comentario").style.display = "none";
	
}

</script>

<!-- Paginación del listado -->
<script>

function calculateRow(row) {
    var price = +row.find('input[name^="price"]').val();
}

function calculateGrandTotal() {
    var grandTotal = 0;
    $("table.order-list").find('input[name^="price"]').each(function () {
        grandTotal += +$(this).val();
    });
    $("#grandtotal").text(grandTotal.toFixed(2));
}
</script>

<!-- Puntuacion -->
<script>

$(function () {
	$('.score-star').score({
		number      : 5,
		size        : 26,           
		color       : 'blue', 
		score       : ${anotacion.puntuacion}, 
		vertical    : false,         
		hints       : undefined,  
		mouseover   : undefined, 
		mouseout    : undefined,  
		readOnly    : false,
		fontAwesome : false, 
		debug       : false, 
		click       : function(score, event){
	        guardarPuntuacion(score);
	    }
	
		});
		
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
		xhr.setRequestHeader(header, token);
	});
});

function guardarPuntuacion(score){
	
	var id = $('input[name="anotacionId"]').val();
		
	$.ajax({
		url : '/puntuaciones/guardar',
		data : {idAnotacion: id, 
			puntuacion: score},
		type : 'POST',
		success : function(data) {
			$("#numero-votos").text(data.votos);
			$('.score-star').score('score', data.puntuacion); 
		},
		error : function(err) {
		}
	});
}

if(${!anotacion.propia}){
	$(".score-star").unbind("click");
}


</script>
