<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>

<link rel="stylesheet"
	href='<c:url value="/bootstrap/css/sortable-theme-bootstrap.css" />' />

<c:if test="${not empty mensaje}">
	<div class="text-center"><spring:message code="${mensaje}" /></div>
</c:if>
<div class="row">
        <div class="panel panel-primary">
            
<table id="listado" class="table paginated sortable-theme-bootstrap" data-sortable>
	<thead class="thead-inverse">
		<tr>
			 <th>#</th>
	         <th class="text-justify"><spring:message code="anotacion.nombreAnotacion" /></th>
	         <th class="text-justify"><spring:message code="anotacion.descripcion" /></th>
	         <th class="text-center"><spring:message code="anotacion.tipoContenido" /></th>
	         <th class="text-center"><spring:message code="usuario.nombre" /></th>
			 <th class="text-center"><spring:message code="anotacion.puntuacion" /></th>
			 <th class="text-center" style="width: 150px"><spring:message code="anotacion.opciones" /></th>
		</tr>
	</thead>
	<tbody>
		<c:choose>
			<c:when test="${empty anotacionesListadoForm}">
				<td scope="row">--</td>
				<td class="text-justify">--</td>
				<td class="text-justify"><spring:message code="anotacion.noHay"/> </td>
				<td class="text-center">--</td>
				<td class="text-center">--</td>
				<td class="text-center">--</td>
				<td class="text-center">--</td>
			</c:when>
			<c:otherwise>

			<c:forEach items="${anotacionesListadoForm}" var="anotacionForm"
				varStatus="status">
				<tr>
					<td scope="row">${status.count}</td>
					<td class="text-justify">
						${anotacionForm.nombre}
					</td>
					<td class="text-justify">
						<p> ${anotacionForm.descripcion} </p>
					</td>
					<td class="text-center">
						<c:choose>
							  <c:when test="${not empty anotacionForm.tipoDeAnotacion.usuarioId}">
							   	  ${anotacionForm.tipoDeAnotacion.nombre}
							  </c:when>
							  <c:otherwise>
							  	<spring:message code="${anotacionForm.tipoDeAnotacion.nombre}" />
							  
							  </c:otherwise>
						</c:choose>
					</td>
					<td class="text-center">${anotacionForm.usuarioNombre}</td>
					<td class="text-center">${anotacionForm.puntuacion}</td>
					
					<td class="text-center">
						
						
							 <spring:url value="/anotaciones/ver/{indice}-{id}" var="url">
							 	<spring:param name="id" value="${anotacionForm.anotacionId}"/>
							 	<spring:param name="indice" value="${anotacionForm.indexador}"/>
							 </spring:url>
							<a href="${url}" class="btn btn-default" type="submit">
								<span class="glyphicon glyphicon-search"></span>
							</a>
					
						<security:authorize access="isAuthenticated()">
						<c:if test="${anotacionForm.propia}">
							
							<spring:url value="/anotaciones/editar/{indice}-{id}" var="url">
							 	<spring:param name="id" value="${anotacionForm.anotacionId}"/>
							 	<spring:param name="indice" value="${anotacionForm.indexador}"/>
							 </spring:url>
							<a href="${url}" class="btn btn-default" type="submit">
								<span class="glyphicon glyphicon-edit"></span>
							</a>
							<c:url value="/anotaciones/borrar/{id}" var="urlBorrar"/>
							<button type="submit" class="btn btn-default" data-toggle="modal" data-target="#popup-borrar-${anotacionForm.anotacionId}">
								<span class="glyphicon glyphicon-trash"></span>
							</button>
							
							<div id="popup-borrar-${anotacionForm.anotacionId}" class="modal fade" tabindex="-1" role="dialog">
							  <div class="modal-dialog">
							    <div class="modal-content">
									<form action="${urlBorrar}" method="post" class="form-enlace">
										<div class="modal-header">
										  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										  <span class="modal-title">Borrar anotación</span>
										</div>
										<div class="modal-body">
										  <p>¿Quieres borrar esta anotación?</p>
										</div>
										<div class="modal-footer">
										  <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
										  <button type="submit" class="btn btn-primary">Si, quiero borrarla</button>
										</div>
										<input type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}" />
										<input type="hidden" name="id"
											value="${anotacionForm.anotacionId}" />
									</form>
							    </div>
							  </div>
							</div>					
						</c:if>
						</security:authorize>				 
					</td>
				</tr>
	
			</c:forEach>
			
			</c:otherwise>
		</c:choose>


	</tbody>
</table>
</div>
</div>

<!-- Paginar los resultados cada 10 filas -->
<script>

$('table.paginated').each(function() {
    var currentPage = 0;
    var numPerPage = 10;
    var $table = $(this);
    $table.bind('repaginate', function() {
        $table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
    });
    $table.trigger('repaginate');
    var numRows = $table.find('tbody tr').length;
    var numPages = Math.ceil(numRows / numPerPage);
    var $pager = $('<div class="pager"></div>');
    for (var page = 0; page < numPages; page++) {
        $('<span class="page-number"></span>').text(page + 1).bind('click', {
            newPage: page
        }, function(event) {
            currentPage = event.data['newPage'];
            $table.trigger('repaginate');
            $(this).addClass('active').siblings().removeClass('active');
        }).appendTo($pager).addClass('clickable');
    }
    $pager.insertBefore($table).find('span.page-number:first').addClass('active');
});
</script>
	
<script src='<c:url value="/jquery/sortable.min.js" />'></script>
		
