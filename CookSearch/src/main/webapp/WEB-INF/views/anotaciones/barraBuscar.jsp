<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>

<div>

	<div id="buscador">
		<h1>
			<spring:message code="anotacion.buscarMensaje" />
		</h1>
		<c:url value="/anotaciones/listado-simple" var="url" />
		<form:form class="form-horizontal"
					action="${url}"
					method="get"
					commandName="anotacionBuscarForm">
		
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" id="token_seguridad" />
		
			<div class="input-group">
			
				<spring:message code="anotacion.buscarTexto" var="msgTexto"  />
				<form:input path="textoBuscar" cssClass="form-control"
					placeholder='${msgTexto}'/>
				
				<span class="input-group-btn">
					<button class="btn btn-primary" type="submit">
						<span class="glyphicon glyphicon-search"></span>
					</button>
				</span>
			</div>

		</form:form>
			<a class="separator-left" href="#busqueda_modal" data-toggle="modal"><spring:message
					code="anotacion.buscaquedaAvanzada" /></a>
			<security:authorize access="isAuthenticated()">
				<a class="separator-left" href="/anotaciones/nueva"><spring:message
						code='anotacion.nueva' /> </a>
			</security:authorize>

	</div>
</div>


<!-- Modal  busqueda avanzada-->
<div id="busqueda_modal" class="modal fade" role="dialog">


	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">
					<spring:message code="anotacion.busquedaAvanzada" />
				</h4>
			</div>
			<div class="modal-body">
				<c:url value="/anotaciones/listado_avanzado" var="url" />
				<form:form class="form-horizontal"
					action="${url}"
					method="get"
					commandName="anotacionBuscarForm">
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" id="token_seguridad" />

					<div class="form-group">
						<form:label path="textoBuscar">
							<spring:message code="anotacion.buscarTexto" />
						</form:label>
						<form:input path="textoBuscar" cssClass="form-control" />
					</div>
				
					<div class="form-group">
						<form:label path="textoBuscar">
							<spring:message code="anotacion.puntuacion" />
						</form:label>
						<form:input type="number" path="puntuacion" cssClass="form-control" min="0" max="5" />
					</div>
					
					<div class="form-group">
						<form:label path="textoBuscar">
							<spring:message code="anotacion.tipoContenido"/>
						</form:label>
						
						<form:select
						path="tipoDeAnotacion" name="tipo_campoContenido" class="form-control">
						<form:option value="">
							<spring:message code="anotacion.tipoTodos" />
						</form:option>
						<c:forEach items="${tiposDeAnotacion}" var="tipoAnotacion">
							<form:option value="${tipoAnotacion.id}">
								<c:choose>
							  		<c:when test="${not empty tipoAnotacion.usuarioId}">
							   	  		${tipoAnotacion.nombre}
							  		</c:when>
							 		 <c:otherwise>
							  			<spring:message code="${tipoAnotacion.nombre}" />
							  		</c:otherwise>
								</c:choose>
							</form:option>
						</c:forEach>
						
						</form:select>
					</div>

					<fieldset>
						<legend>
							<spring:message code="anotacion.fechaPublicacion" />
						</legend>

						<div class="form-group">
							<form:label path="fechaDesde">
								<spring:message code="anotacion.fechaDesde" />
							</form:label>
							<form:input type="date" path="fechaDesde" cssClass="form-control" />
						</div>
					
						<div class="form-group">
							<form:label path="fechaHasta">
								<spring:message code="anotacion.fechaHasta" />
							</form:label>
							<form:input type="date" path="fechaHasta" cssClass="form-control" />
						</div>
	
					</fieldset>

					<fieldset>
						<legend>
							<spring:message code="anotacion.privacidad" />
						</legend>
						<div class="form-group">
							<form:checkbox path="publica"/>
							<form:label path="publica">
								<spring:message code="anotacion.publica" />
							</form:label>
							<security:authorize access="!isAnonymous()">
								<form:checkbox path="privada"/>
								<form:label path="privada">
									<spring:message code="anotacion.privada" />
								</form:label>
							</security:authorize>
						</div>
					</fieldset>

					<div class="form-group">
						<button id="boton_busqueda_avanzada" type="submit"
							class="btn btn-primary center-block">
							<spring:message code="anotacion.buscar" />
						</button>
					</div>

				</form:form>

			</div>

		</div>
		
	</div>

</div>
