<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="panel panel-primary">

	<div class="panel-heading">
		<h4 class="text-uppercase text-center font-white">
			<spring:message code="anotacion.nueva" />
		</h4>
	</div>

	<div class="panel-body">

		<form:form method="post" commandName="anotacionNuevaForm"
			enctype="multipart/form-data">



			<div class="form-group">
				<spring:message code="anotacion.nombre" var="msgNombre" />
				<form:label path="nombre" cssClass="control-label">${msgNombre}</form:label>
				<form:input path="nombre" cssClass="form-control" />
				<form:errors path="nombre" />
			</div>

			<div class="form-group">
				<spring:message code="anotacion.descripcion" var="msgDescripcion" />
				<form:label path="descripcion">${msgDescripcion}</form:label>
				<form:textarea path="descripcion" cssClass="form-control" />
				<form:errors path="descripcion" />
			</div>

			<div class="form-group">

				<label> <spring:message code="anotacion.tipoContenido"
						var="msgTipoContenido" /> <span>${msgTipoContenido}</span>
					
					<form:select
						path="tipoDeAnotacion" name="tipo_campoContenido"
						class="form-control" onchange="nuevoTipoContenido(this.value);">
						<c:forEach items="${tiposDeAnotacion}" var="tipoAnotacion">
							<form:option value="${tipoAnotacion.id}">
								<c:choose>
							  		<c:when test="${not empty tipoAnotacion.usuarioId}">
							   	  		${tipoAnotacion.nombre}
							  		</c:when>
							 		 <c:otherwise>
							  			<spring:message code="${tipoAnotacion.nombre}" />
							  		</c:otherwise>
								</c:choose>
							</form:option>
						</c:forEach>
						<form:option value="">
							<spring:message code="anotacion.tipoOtro" />
						</form:option>
					</form:select>
				</label>

			</div>

		
			<div class="form-group oculto" id="tipo_no_definido">
				<form:label path="tipoDeContenidoOtro" cssClass="control-label">
					<spring:message code="anotacion.tipoNoDefinido" />
				</form:label>
				<form:input path="tipoDeContenidoOtro" cssClass="form-control" />
			</div>
			
			<div class="form-group">
				<a class="btn btn-default left-block" href="#aniadir_campos_modal"
					data-toggle="modal"> <spring:message
						code="anotacion.aniadirCampos" />
				</a>
			</div>

			<div id="campos_generados" class="form-group"></div>

			<div class="form-group">
				<spring:message code="anotacion.privacidad" var="msgPrivacidad" />
				<form:label path="privacidad"
					cssClass="control-label separator-right">${msgPrivacidad}</form:label>
				<form:radiobutton path="privacidad" value="true" checked="checked" />
				<spring:message code="anotacion.publica" />
				<form:radiobutton path="privacidad" value="false" />
				<spring:message code="anotacion.privada" />
				<form:errors path="privacidad" />
			</div>

			<div class="form-group">
				<button class="btn btn-primary center-block" type="submit">
					<spring:message code="anotacion.guardar" />
				</button>
			</div>

		</form:form>

	</div>

	<!-- Modal  aniadir campos-->
	<div id="aniadir_campos_modal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">
						<spring:message code="anotacion.aniadirCampo" />
					</h4>
				</div>
				<div class="modal-body">

					<div class="row">

						<div class="col-md-5">
							<spring:message code="anotacion.nombreCampo" var="msgNombre" />
							<label> <span class="sr-only control-label">${msgNombre}</span>
								<input type="text" name="nombre_campo" class="form-control"
								placeholder="${msgNombre}" />
							</label>
						</div>

						<div class="col-md-4">
							<spring:message code="anotacion.tipoCampo" var="msgTipo" />
							<label> <span class="sr-only">${msgTipo}</span> <select
								name="tipo_campo" class="form-control" id="tipo_seleccionado">
									<c:forEach items="${tiposDeCampo}" var="tipoDeCampo">
										<option value="${tipoDeCampo.key}"
											data-id-campo="html-campo-${tipoDeCampo.value.campoForm}">
											<spring:message code="${tipoDeCampo.key}" />
										</option>
									</c:forEach>
							</select>
							</label>
						</div>

						<div class="col-md-3">
							<button class="aniadir_campo btn btn-default pull-right">
								<span class="glyphicon glyphicon-plus"></span>
							</button>
						</div>

					</div>

				</div>
				<div class="modal-footer">
					<button id="boton_aniadir_campos" type="button"
						class="btn btn-default" data-dismiss="modal">
						<spring:message code="anotacion.aniadirCampos" />
					</button>
				</div>
			</div>

		</div>
	</div>
</div>


<script>
	$(function() {

		$('.aniadir_campo').click(
				function() {
					var boton = $(this);
					var fila = boton.closest('.row');
					var copia = fila.clone(true);
					copia.find('.form-control').val('');
					fila.after(copia);
					boton.html(
							'<span class="glyphicon glyphicon-minus"></span>')
							.off('click').click(function() {
								$(this).closest('.row').remove();
							});

				});

		$('#boton_aniadir_campos')
				.click(
						function() {
							$('#aniadir_campos_modal .row')
									.each(
											function() {

												var etiqueta = $(this)
														.find(
																'input[name="nombre_campo"]')
														.val();
												var id_tipo = $(this)
														.find(
																'select[name="tipo_campo"] option')
														.filter(':selected')
														.data('id-campo');
												
												var tipo_campo = $(this)
												.find(
														'select[name="tipo_campo"] option').filter(':selected').val();
												

												var replace_etiqueta = $('#' + id_tipo)
														.html().replace(
																/%nombre%/g,
																etiqueta);
												var html = replace_etiqueta.replace(
														/%tipoCampo%/g,
														tipo_campo);
												
												var boton = $(
														'<button class="btn btn-default" type="button">'
															+'<span class="glyphicon glyphicon-minus"></span>'
														+'</button>');
												
												boton.click(function () {
													$(this).closest('div').remove();
												})

												var divCampo = $('<div></div>');
												divCampo
													.append(html)
													.append(boton);
												
												$('#campos_generados').append(divCampo);
												
											});
						
						});
		
		

	});

	function nuevoTipoContenido(valor) {
		var otro = "";
		if (valor == otro) {

			document.getElementById("tipo_no_definido").style.display = "block";
		} else {
			document.getElementById("tipo_no_definido").style.display = "none";
		}

	}
</script>

<div class="oculto">

	<jsp:include page="/WEB-INF/views/tipo-de-campo/html-campo-FICHERO.jsp" />
	<jsp:include page="/WEB-INF/views/tipo-de-campo/html-campo-TEXTO_LARGO.jsp" />
	<jsp:include page="/WEB-INF/views/tipo-de-campo/html-campo-TEXTO.jsp" />

</div>
