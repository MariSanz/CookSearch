<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="contenedor-form">

	<div align="center">
		<img class="img-fluid" src='<c:url value="/image/usuario.png" />' />
	</div>

	<form:form method="post" commandName="registroForm" class="form-horizontal">

		<h3>
			<spring:message code="usuario.nuevaCuenta" />
		</h3>

		<div class="form-group">
			<spring:message code="usuario.nombre" var="msgUsuario" />
			<form:label path="usuario" cssClass="sr-only">${msgUsuario}</form:label>
			<form:input path="usuario" cssClass="form-control"
				placeholder="${msgUsuario}" />
			<form:errors path="usuario" />
		</div>

		<div class="form-group">
			<spring:message code="usuario.email" var="msgEmail" />
			<form:label path="email" cssClass="sr-only">${msgEmail}</form:label>
			<form:input path="email" cssClass="form-control"
				placeholder="${msgEmail}" />
			<form:errors path="email" />
		</div>

		<div class="form-group">
			<spring:message code="usuario.clave" var="msgClave" />
			<form:label path="password" cssClass="sr-only">${msgClave}</form:label>
			<form:input path="password" cssClass="form-control" type="password"
				placeholder="${msgClave}" />
			<form:errors path="password" />
		</div>

		<div class="form-group">
			<spring:message code="usuario.repetirClave" var="msgRepClave" />
			<form:label path="repPassword" cssClass="sr-only">${msgRepClave}</form:label>
			<form:input path="repPassword" cssClass="form-control" type="password"
				placeholder="${msgRepClave}" />
			<form:errors path="repPassword" />
		</div>
		<div class="form-group">
			<label> <input type="checkbox" name="aceptar_condiciones" required/> 
				<spring:message	code="usuario.aceptar" /> 
				<a href="/condiciones-uso" data-toggle="modal">
					<spring:message code="index.condicionesUso" />
				</a>
			</label>
		</div>
		<br />
		<button class="btn btn-primary center-block" type="submit">

			<spring:message code="usuario.crearCuenta" />

		</button>

	</form:form>

</div>
