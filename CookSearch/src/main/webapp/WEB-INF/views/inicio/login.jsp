<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="contenedor-form">

<div align="center">
		<img class="img-fluid" src='<c:url value="/image/usuario.png" />' />
	</div>
	<form method="post" class="form-horizontal">

		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />

		<h3>
			<spring:message code="usuario.login" />
		</h3>
		<br/>
		<div class="form-group">
			<spring:message code="usuario.nombre" var="msgUsuario" />
			<label id="username" class="sr-only">${msgUsuario}</label> <input
				type="text" id="username" name="username" class="form-control"
				placeholder="${msgUsuario}" />
		</div>
		<div class="form-group">
			<spring:message code="usuario.clave" var="msgClave" />
			<label id="password" class="sr-only">${msgClave}</label> <input
				type="password" id="password" name="password" class="form-control"
				placeholder="${msgClave}" />
		</div>
		<div>
			<label> <input type="checkbox" name="remember-me" /> <spring:message
					code="usuario.recordar" />
			</label>
		</div>
		
		<br />
		<button class="btn btn-primary center-block" type="submit">
				<spring:message code="usuario.login" />
		</button>

	</form>

</div>
