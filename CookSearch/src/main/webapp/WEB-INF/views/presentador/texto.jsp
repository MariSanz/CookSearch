<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	

		<c:choose>
			<c:when test="${campo.tipo eq 'tipo-de-campo-url'}">
				<a href="${campo.contenido}">${campo.contenido}</a>
			</c:when>
			<c:otherwise>
				<p class="text-justify">${campo.contenido}</p>
			</c:otherwise>
		</c:choose>
