<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<spring:url value="/ficheros/{id}" var="url">
	<spring:param name="id" value="${campo.fichero}"/>
</spring:url>
<c:choose>
	<c:when test="${
		campo.contentType eq 'image/png'
		or campo.contentType eq 'image/jpeg'
		or campo.contentType eq 'image/gif'}">
		<img src="${url}" alt="${campo.etiqueta}" class="img-responsive"/>
	</c:when>
	<c:otherwise>
		<a href="${url}">Descargar</a>
	</c:otherwise>
</c:choose>


