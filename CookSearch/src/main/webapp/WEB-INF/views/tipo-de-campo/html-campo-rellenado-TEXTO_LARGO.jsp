<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<div id="html-campo-TEXTO_LARGO" class="form-group separator">
		<label class="control-label" >
			<input	type='text' name="etiquetaTexto" class="form-control" value="${campo.etiqueta}"> 
			<textarea cols="80" rows="10" name="campoTexto" class="form-control">${campo.contenido}</textarea> 
			<input	type="hidden" name="tipoCampoTexto" value="${campo.tipo}">
			<input type="hidden" name="idCampoTexto" value="${campo.id}">
			
		</label>
		
	<button class="borrar_campo btn btn-default boton-borrado-campo">
		<span class="glyphicon glyphicon-minus"></span>
	</button>
	

</div>