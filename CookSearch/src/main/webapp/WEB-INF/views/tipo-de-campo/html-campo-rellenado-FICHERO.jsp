<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>



<div id="html-campo-FICHERO" class="form-group separator upload">
	<label class="control-label">
		<input type="text" name="etiquetaFile" class="form-control" value="${campo.etiqueta}">
		<input type="file" name="campoFile" class="form-control" style="color: transparent"/>
		<input type="hidden" name="tipoCampoFile" value="${campo.tipo}">
		<input type="hidden" name="idCampoFile" value="${campo.id}">
		<spring:url value="/ficheros/{id}/descargar" var="url">
			<spring:param name="id" value="${campo.fichero}"/>
		</spring:url>
		<a href="${url}" class="separator"> <spring:message code="anotacion.verFichero"/> </a>
		
	</label>
	
	<button class="borrar_campo btn btn-default boton-borrado-campo">
		<span class="glyphicon glyphicon-minus"></span>
	</button>
	

</div>

