<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div id="html-campo-TEXTO" class="form-group separator bloque-campo">
	<label class="control-label">
		<input	type="text" name="etiquetaTexto" class="form-control" value="${campo.etiqueta}">
		
		<c:choose>
			<c:when test="${campo.tipo=='tipo-de-campo-url'}">
				<input type="text" name="campoTexto" class="form-control" value="${campo.contenido}" /> 
			</c:when>
			<c:otherwise>
				<input	type="text" name="campoTexto" class="form-control" value="${campo.contenido}" /> 
			</c:otherwise>
		</c:choose>
		
		<input	type="hidden" name="tipoCampoTexto" value="${campo.tipo}">
		<input type="hidden" name="idCampoTexto" value="${campo.id}">
	</label>
	<button class="borrar_campo btn btn-default boton-borrado-campo">
		<span class="glyphicon glyphicon-minus"></span>
	</button>
	
</div>
