<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="contenedor-fluid">

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="text-uppercase text-center font-white">
				<spring:message code="index.politicaCookies" />
			</h3>
		</div>
		<div class="panel-body">

			<h4 class="text-uppercase">
				<spring:message code="index.politicaCookiesTitulo1" />
			</h4>

			<p class="text-justify separator">
				<spring:message code="index.politicaCookiesContenido1" />
			</p>

			<h4 class="text-uppercase">
				<spring:message code="index.politicaCookiesTitulo2" />
			</h4>

			<p class="text-justify separator">
				<spring:message code="index.politicaCookiesContenido2" />
			</p>
			<p class="text-justify separator">
				<spring:message code="index.politicaCookiesContenido3" />
			</p>

			<h4 class="text-uppercase text-left">
				<spring:message code="index.politicaCookiesTitulo3" />
			</h4>

			<p class="text-justify separator">
				<spring:message code="index.politicaCookiesContenido4" />
			</p>
			
				<ul>
					<li>
					<p class="text-justify separator">
					<spring:message code="index.politicaCookiesContenido5" />
					</p>
					</li>
				</ul>

			<h4 class="text-uppercase text-left">
				<spring:message code="index.politicaCookiesTitulo4" />
			</h4>

			<p class="text-justify separator">
				<spring:message code="index.politicaCookiesContenido6" />
			</p>
			
			<ul>
				<li>
					<p class="text-justify separator">
					<spring:message code="index.politicaCookiesContenido7" />
					</p>
				</li>
				<li>
					<p class="text-justify separator">
					<spring:message code="index.politicaCookiesContenido8" />
					</p>
				</li>
			</ul>

			<h4 class="text-uppercase text-left">
				<spring:message code="index.politicaCookiesTitulo6" />
			</h4>

			<p class="text-justify separator">
				<spring:message code="index.politicaCookiesContenido9" />
			</p>

			

		</div>
	</div>

</div>