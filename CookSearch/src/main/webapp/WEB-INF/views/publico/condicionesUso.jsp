<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="contenedor-fluid">

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="text-uppercase text-center font-white">
				<spring:message code="index.condionesUso" />
			</h3>
		</div>
		<div class="panel-body">

			<h4 class="text-uppercase">
				<spring:message code="index.condionesUsoTitulo1" />
			</h4>

			<p class="text-justify separator">
				<spring:message code="index.condionesUsoContenido1" />
			</p>
			<p class="text-justify separator">
				<spring:message code="index.condionesUsoContenido2" />
				<a class="btn btn-link" href="/politica-privacidad"> 
					<spring:message code="index.politicaPrivacidad" />
				</a>
				<spring:message code="index.condionesUsoContenido3" />
			</p>

			<h4 class="text-uppercase">
				<spring:message code="index.condionesUsoTitulo2" />
			</h4>

			<p class="text-justify separator">
				<spring:message code="index.condionesUsoContenido4" />
				
			</p>

			<h4 class="text-uppercase text-left">
				<spring:message code="index.condionesUsoTitulo3" />
			</h4>

			<p class="text-justify separator">
				<spring:message code="index.condionesUsoContenido5" />
			</p>

			<h4 class="text-uppercase text-left">
				<spring:message code="index.condionesUsoTitulo4" />
				
			</h4>

			<p class="text-justify separator">
				<spring:message code="index.condionesUsoContenido6" />
				<spring:message code="index.condionesUsoContenido7" />
			</p>

			<h4 class="text-uppercase text-left">
				<spring:message code="index.condionesUsoTitulo5" />
			</h4>

			<p class="text-justify separator">
				<spring:message code="index.condionesUsoContenido8" />
				<spring:message code="index.condionesUsoContenido9" />
			</p>

			<h4 class="text-uppercase text-left">
				<spring:message code="index.condionesUsoTitulo6" />
			</h4>

			<p class="text-justify separator"><spring:message code="index.condionesUsoContenido10" /></p>
			<ul>
				<li><spring:message code="index.condionesUsoContenido11" /></li>
				<li><spring:message code="index.condionesUsoContenido12" /></li>
				<li><spring:message code="index.condionesUsoContenido13" /></li>
			</ul>
			<p class="text-justify separator"><spring:message code="index.condionesUsoContenido14" /></p>
			<ul>
				<li><spring:message code="index.condionesUsoContenido15" /></li>
			</ul>
	
			<h4 class="text-uppercase text-left">
				<spring:message code="index.condionesUsoTitulo7" />
			</h4>

			<p class="text-justify separator"><spring:message code="index.condionesUsoContenido16" /></p>
			<ul>
				<li><spring:message code="index.condionesUsoContenido16" /></li>
				<li><spring:message code="index.condionesUsoContenido17" /></li>
				<li><spring:message code="index.condionesUsoContenido18" /></li>
				<li><spring:message code="index.condionesUsoContenido19" /></li>
			</ul>

			<h4 class="text-uppercase text-left">
				<spring:message code="index.condionesUsoTitulo8" />
			</h4>

			<p class="text-justify separator"><spring:message code="index.condionesUsoContenido20" />
				<a class="btn btn-link" href="/politica-cookies">
					<spring:message code="index.politicaCookies" />
				</a>
			</p>

			<h4 class="text-uppercase text-left">
				<spring:message code="index.condionesUsoTitulo9" />
			</h4>
			<p class="text-justify separator"><spring:message code="index.condionesUsoContenido21" />
			<a class="btn btn-link" href="/politica-privacidad"> 
				<spring:message code="index.politicaPrivacidad" />
			</a>
			</p>

		</div>
	</div>

</div>