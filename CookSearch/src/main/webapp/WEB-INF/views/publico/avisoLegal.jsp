<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="contenedor-fluid">

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="text-uppercase text-center font-white">
				<spring:message code="index.avisoLegal" />
			</h3>
		</div>
		<div class="panel-body">


			<p class="text-justify separator"><spring:message code="index.avisoLegalContenido1" /></p>
			<p class="text-justify"><spring:message code="index.avisoLegalContenido2" />
				<a class="btn btn-link" href="/condiciones-uso"> 
					<spring:message code="index.condicionesUso" />
				</a>
			</p>
			<p class="text-justify">
				<spring:message code="index.avisoLegalContenido3" />
				<a class="btn btn-link" href="/politica-privacidad"> 
					<spring:message	code="index.politicaPrivacidad" />
				</a> y 
				<a class="btn btn-link" href="/politica-cookies">
					<spring:message code="index.politicaCookies" />
				</a>
			</p>
			<p class="text-justify separator"><spring:message code="index.avisoLegalContenido4" /></p>

			
		</div>
	</div>

</div>