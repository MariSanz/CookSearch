<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="contenedor-fluid"></div>

<div class="panel panel-primary">
  <div class="panel-heading"><h4 class="text-uppercase text-center font-white"><spring:message code="index.destacada"/></h4></div>
  <div class="panel-body">	
  
  <h4 class="text-uppercase text-justify">
		${masVista.nombre}
	</h4>
	<p class="text-justify lead"> ${masVista.descripcion}</p>
	
	<div class="row">
	  <div class="col-md-3"> <h4><spring:message code="anotacion.tipoContenido"/></h4>
		<p class="lead">
			<c:choose>
			  <c:when test="${not empty masVista.tipoDeAnotacion.usuarioId}">
			  	 ${masVista.tipoDeAnotacion.nombre}
			  </c:when>
			  <c:otherwise>
			 	 <spring:message code="${masVista.tipoDeAnotacion.nombre}" />
			  </c:otherwise>
			</c:choose>
		</p>
	</div>
	  <div class="col-md-2">
	  	<h4><spring:message code="usuario.nombre" /></h4>
		<p class="lead">${masVista.usuario}</p></div>
	  <div class="col-md-2">
	  	<h4  class="text-center"><spring:message code="anotacion.puntuacion" /></h4>
		<p class="lead text-center">${masVista.puntuacion}</p>
	  </div>
	  <div class="col-md-3">
	  	<h4 class="text-center"><spring:message code="anotacion.numComentarios" /></h4>
		<p class="lead text-center"> ${masVista.numComentarios}</p>
	  </div>
	  <div class="col-md-2">
	  	<h4 class="text-center"><spring:message code="anotacion.numVistas" /></h4>
		<p class="lead text-center"> ${masVista.numVistas}</p>
	</div>
  </div>

	<spring:url value="/anotaciones/ver/{indice}-{id}" var="url">
		<spring:param name="id" value="${masVista.anotacionId}"/>
		<spring:param name="indice" value="${masVista.indexador}"/>
	</spring:url>
	<a href="${url}" class="btn btn-default" type="submit">
		<span class="glyphicon glyphicon-search"></span>
		<spring:message code="anotacion.ver"/>
	</a>
	
		
  
</div>
</div>

<div class="panel panel-default">
  <div class="panel-heading"><h4><spring:message code="index.ultimosComentarios"/></h4></div>
  	<div class="panel-body">
	<table id="comentarios" class="table table-hover" onclick="verAnotacion()">
	<thead class="thead-inverse">
		<tr>
			 <th class="text-center"><spring:message code="usuario.nombre" /></th>
			 <th class="text-justify"><spring:message code="anotacion.comentario" /></th>
			 <th class="text-center"><spring:message code="anotacion.nombre" /></th>
			 <th class="text-center"><spring:message code="anotacion.fecha" /></th>
			 <th> </th>
		</tr>
	</thead>
	<tbody>
		<c:choose>
			<c:when test="${empty ultimosComentarios}">
				<tr>
					<td>--</td>
					<td class="text-justify"> <spring:message code="comentario.noHay"/> </td>
					<td>--</td>
					<td>--</td>
					<td></td>
				</tr>
			</c:when>
		<c:otherwise>
			<c:forEach items="${ultimosComentarios}" var="comentario">
			<tr>
				<td class="text-center">${comentario.usuario}</td>
				<td class="text-justify">${comentario.contenido}</td>
				<td class="text-justify">${comentario.anotacionNombre}</td>
				<td class="text-center">${comentario.fecha}</td>
				<td class="text-center">
					<spring:url value="/anotaciones/ver/{indice}-{id}" var="url">
						<spring:param name="id" value="${comentario.anotacionId}"/>
						<spring:param name="indice" value="${comentario.indexador}"/>
					</spring:url>
					<a href="${url}" class="btn btn-default" type="submit">
						<span class="glyphicon glyphicon-search"></span>
						<spring:message code="anotacion.ver"/>
					</a>
				</td>
			</tr>
			</c:forEach>
		</c:otherwise>
		</c:choose>
	</tbody>
	</table>
	</div>
</div>


<!-- Panel de Cookies -->

<div id="popup-cookies" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-body">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		
			<h3>
				<strong><spring:message code="index.usoCookies" /></strong>
			</h3>
			<p class="text-justify">
				<spring:message code="index.usoCookiesContenido1" />
				<spring:message code="index.usoCookiesContenido2" />
				<spring:message code="index.usoCookiesContenido3" />
			</p>
			<p class="text-justify separator">
				<spring:message code="index.usoCookiesContenido4" />
				<a href="/"> <spring:message code='index.politicaCookies' /></a>
				<spring:message code="index.usoCookiesContenido5" />
		
			</p>
		
			<p class="text-justify separator">
				<spring:message code="index.usoCookiesContenido6" />
			</p>
		
			<button type="button" class="btn btn-primary center-block separator"
				 data-dismiss="modal">
				<spring:message code="index.acepto" />
			</button>
    	
    	</div>
    </div>
</div>

</div>

<!-- Acceso a la politica de cookies -->
<script type="text/javascript">

	$(function () {
		console.log(localStorage.controlcookie);
		if (!localStorage.controlcookie) {
			$('#popup-cookies').modal('show');
			$('#popup-cookies button').click(function () {
				localStorage.controlcookie = (localStorage.controlcookie || 0);
				localStorage.controlcookie++; // incrementamos cuenta de la cookie
			});
		}
	});

</script>

<!-- Puntuacion -->
<script type="text/javascript"
		src='<c:url value="/jquery/jQuery.score.js" />'></script>
		
<script>
$('.score-star').score({

	// score range
	number      : 5,

	// symbol size
	size        : 26,           

	// symbole color
	color       : 'red', 

	// initila score
	score       : ${masVista.puntuacion}, 

	// vertical layout
	vertical    : false,         

	// replace default hints
	// e.g. ['bad', 'poor', 'regular', 'good', 'gorgeous']
	hints       : undefined,  

	// read only mode
	readOnly    : false,

	// uses font awesome icons
	fontAwesome : false, 

	// debug mode
	debug       : false, 

	});

</script>

<script>
function verAnotacion(){
	
	$(function() {
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");
		$(document).ajaxSend(function(e, xhr, options) {
			xhr.setRequestHeader(header, token);
		});
	});
	
	$("#comentarios").each(function(index) {
		var id = $(this).find('input[name="anotacionId"]').val();
		
		$.ajax({
			url : '/anotaciones/ver/'+id,
			contentType: "application/json",
			processData : false,
			type : 'GET'
		});
	})
}
</script>