<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="contenedor-fluid">

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h3 class="text-uppercase text-center font-white">
				<spring:message code="index.politicaPrivacidad" />
			</h3>
		</div>
		<div class="panel-body">

			<h4 class="text-uppercase">
				<spring:message code="index.politicaPrivacidadTitulo1" />
			</h4>

			<p class="text-justify separator">
				<spring:message code="index.politicaPrivacidadContenido1" />
			</p>
			
			<p class="text-justify separator">
				<spring:message code="index.politicaPrivacidadContenido2" />
			</p>

			<h4 class="text-uppercase">
				<spring:message code="index.politicaPrivacidadTitulo2" />
			</h4>

			<p class="text-justify separator">
				<spring:message code="index.politicaPrivacidadContenido3" />
			</p>

			<h4 class="text-uppercase text-left">
				<spring:message code="index.politicaPrivacidadTitulo4" />
			</h4>

			<p class="text-justify separator">
				<spring:message code="index.politicaPrivacidadContenido4" />
			</p>

			<h4 class="text-uppercase text-left">
				<spring:message code="index.politicaPrivacidadTitulo5" />
			</h4>

			<ul>
				<li>
					<p class="text-justify separator">
					<spring:message code="index.politicaPrivacidadContenido5" />
					</p>
				</li>
				<li>
					<p class="text-justify separator">
					<spring:message code="index.politicaPrivacidadContenido6" />
					</p>
				</li>
			</ul>



		</div>
	</div>

</div>